<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Enquiry Details Report</h3>
  	</div>        
    <div class="col-sm-12">

      <div id="filterArea" class="form-row">
        <div class="row">
          <div class="col-md-1">
            <label for="name" class="control-label">Year</label>
            <select id="selectYear" class="form-control"></select>
          </div>
          <div class="col-md-1">
            <label for="name" class="control-label">Month</label>
            <select id="selectMonth" class="form-control"><option value="">All</option></select>
          </div>
          <div class="col-md-1">
            <label for="name" class="control-label">Status</label>
            <select id="selectStatus" class="form-control"><option value="">All</option><option value="Open" selected>Open</option><option value="Closed">Closed</option></select>
          </div>
          <div class="pull-right" style='padding-bottom:10px; padding-top: 5px;'>
            <button type="button" class="btn btn-xs btn-primary" id="btn-pdf" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> PDF</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-xls" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> Excel</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-print" data-row-id="0">
            <span class="glyphicon glyphicon-print"></span> Print</button>
          </div>

          <div class="form-group col-md-4">
          </div>

          <div class="form-group col-md-4">
          </div>
        </div>            
      </div>
      
      <div id="printArea" class="row">

       
      </div>
     

    </div>
  </div>
</div>


<?php include_once("footer.php"); ?>
</div>

<script>
$(document).ready(function() 
{ 
// Year data populate
var max = new Date().getFullYear(),
    min = max - 20,
    select = document.getElementById('selectYear');

for (var i = max; i>=min; i--){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


for (var i = 0; i<monthShortNames.length; i++){
    select = document.getElementById('selectMonth');
    var opt = document.createElement('option');
    opt.value = i+1;
    opt.innerHTML = monthShortNames[i];
    select.appendChild(opt);
}


var db_table_name = [] , db_table_columns = [], table_header = [];

db_table_name = ["enquiry"]; // db table names 
db_table_where_colummn = ["enquiry_date"]; // db table names
db_table_columns = ["id", "name", "phone", "whatsapp", "area", "city", "status", "followup_comments"]; // db table column
table_header = ["Sl.No.","Name", "Phone", "Whatsapp", "Area", "City", "Status", "Followup"]; // db table column

loadAjaxCall();

$("#selectYear, #selectMonth, #selectStatus").change(function () {
  $("#printArea").html("");
  loadAjaxCall();
});

function loadAjaxCall() {
  var year = $("#selectYear option:selected").val();
  var month = $("#selectMonth option:selected").val();
  var status = $("#selectStatus option:selected").val();
   if(year != '') {
      var sqlQuery = "select * from " + db_table_name[0] + 
                      " where year(" + db_table_where_colummn[0] + ") = '" +  year + "'";
        if(month != '') {
          sqlQuery += " and month(" + db_table_where_colummn[0] + ") = '" +  month + "'"
        }
        if(status != '') {
          sqlQuery += " and status = '" +  status + "'"
        }
      
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                
                  populateTable(response, month, year);
               
                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });

      
   }
}

function populateTable(response, month, year) 
{ 
  console.log(response);
  var monthname = '';
  if(month!='') {
    monthname = monthFullNames[month-1]
  }
var htmlTableData = '';                  
          
        htmlTableData += '<table class="reportHeader"><tbody><tr><td><b>Enquiry Details - ' + monthname + ' ' + year + '</b></td></tr></tbody></table>' + 
                          '<div class="table-responsive"><table id="tableData" class="table table-bordered table-responsive">' + 
                          '<thead>' +
                          '<tr>';
        var tableHeaderLength = table_header.length;
        for(loopColumns=0;loopColumns<tableHeaderLength;loopColumns++) 
        {
          
              htmlTableData += '<th width=100>' + table_header[loopColumns] + '</th>'              
           
        }
        htmlTableData += '</tr>' +
                          '</thead>';

        htmlTableData += '<tbody>' 
                          
        var responseLength = response.length;
        for(loopColumns=0;loopColumns<responseLength;loopColumns++) 
        {          
              htmlTableData += '<tr>'
                  var db_table_columns_Length = db_table_columns.length;
                  for(loopHeaders=0; loopHeaders<db_table_columns_Length; loopHeaders++)  {
                    if(loopHeaders==0) {
                      htmlTableData += '<td>' + parseInt(loopColumns+1) + '</td>';
                    }
                    else {
                      var rowContent = response[loopColumns][db_table_columns[loopHeaders]];
                      if(rowContent != null)
                      {
                        htmlTableData += '<td>' + response[loopColumns][db_table_columns[loopHeaders]] + '</td>';
                      }
                      else {
                        htmlTableData += '<td>&#160;</td>';
                      }
                    }
                  }

               htmlTableData += '</tr>'
        }
        htmlTableData += '</tbody></table></div>';
        $("#printArea").html(htmlTableData);
}

});



 $( "#btn-xls" ).click(function() {
      $('#printArea').tableExport({type:'excel'}); 
 }); 

 $( "#btn-print" ).click(function() {
      window.print(); 
 });

 $( "#btn-pdf" ).click(function() {
      pdfExport("printArea");
 });

</script>
</body>
</html>

<?php ob_end_flush(); ?>