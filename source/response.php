
<?php
	//include connection file 
	include_once("connection.php");
	session_start();

	$db = new dbObj();
	$connString =  $db->getConnstring();

	$params = $_REQUEST;	

	$posts = $_GET;

	$action = isset($params['action']) != '' ? $params['action'] : '';
	$empCls = new User($connString);

	switch($action) {
	 case 'add':
		$empCls->insertAndUpdateRecords($params);
	 break;
	 case 'edit':
		$empCls->insertAndUpdateRecords($params);
	 break;
	 case 'both':
		$empCls->insertAndUpdateRecords($params);
	 break;
	 case 'delete':
		$empCls->deleteRecord($params);
	 break;
	 case 'save':
		$empCls->updateUserProfile($params);
	 break;
	 case 'fetch':
		$empCls->fetchRecord($params);
	 break;
	 case 'export':
		$empCls->exportExcel($params);
	 break;
	 default:
	 	$empCls->getAllRecords($params);
	 return;
	}
	
	class User 
	{
		protected $conn;
		protected $data = array();
		function __construct($connString) {
			$this->conn = $connString;
	}
	
	public function getAllRecords($params) {		
		$this->data = $this->getRecords($params);		
		echo json_encode($this->data);
	}

	public function exportExcel($params) {		
		$this->data = $this->exportToExcel($params);		
		echo json_encode($this->data);
	}	
	
	function insertAndUpdateRecords($params) {

			if($params["action"] == "add") 
			{
				if(isset($params["sqlInsertQueryArray"])) {
				$sqlInsertQueryArray = $params["sqlInsertQueryArray"];
					for($sqlQueryCount = 0; $sqlQueryCount < count($sqlInsertQueryArray); $sqlQueryCount++)
					{	
						$sql = $sqlInsertQueryArray[$sqlQueryCount];
						
						echo $result = mysqli_query($this->conn, $sql) or die("error to update data");
					}
				}
			}
			if($params["action"] == "edit") 
			{
				if(isset($params["sqlUpdateQueryArray"])) {
				$sqlUpdateQueryArray = $params["sqlUpdateQueryArray"];
					for($sqlQueryCount = 0; $sqlQueryCount < count($sqlUpdateQueryArray); $sqlQueryCount++)
					{	
						$sql = $sqlUpdateQueryArray[$sqlQueryCount];
						echo $result = mysqli_query($this->conn, $sql) or die("error to update data");
					}
				}
			}
			if($params["action"] == "both") 
			{	
				if(isset($params["sqlInsertQueryArray"])) {
				$sqlInsertQueryArray = $params["sqlInsertQueryArray"];
					for($sqlQueryCount = 0; $sqlQueryCount < count($sqlInsertQueryArray); $sqlQueryCount++)
					{	
						$sql = $sqlInsertQueryArray[$sqlQueryCount];
						
						echo $result = mysqli_query($this->conn, $sql) or die("error to update data");
					}
				}

				if(isset($params["sqlUpdateQueryArray"])) {
				$sqlUpdateQueryArray = $params["sqlUpdateQueryArray"];
					for($sqlQueryCount = 0; $sqlQueryCount < count($sqlUpdateQueryArray); $sqlQueryCount++)
					{	
						$sql = $sqlUpdateQueryArray[$sqlQueryCount];
						echo $result = mysqli_query($this->conn, $sql) or die("error to update data");
					}
				}
			}			
	}

	function updateUserProfile($params) {
		
		$data = array();

		$sql = "UPDATE `users` SET `fullname`='".$params["fullname"]."',`phone`='".$params["phone"]."',`email`='".$params["email"]."',`password`='".$params["password"]."' WHERE id='".$_POST["save_id"]."'";	
		echo $sql;
		echo $result = mysqli_query($this->conn, $sql) or die("error to update data");
	}
	
	function deleteRecord($params) {
		$data = array();
		$sql = "delete from ".$params['table_name']." WHERE ".$params['where_field_name']."=".$params["delete_id"];
		echo $result = mysqli_query($this->conn, $sql) or die("error to delete data");
	}

	function fetchRecord($params) {
		$data = array();		
		$sqlRec = '';		

	   // getting the records 
		if (isset($params['sqlQuery'])) { 
			$sqlRec = $params['sqlQuery']; 
		}				

		$queryRecords = mysqli_query($this->conn, $sqlRec) or die("error to fetch data");
		
		while( $row = mysqli_fetch_assoc($queryRecords) ) { 
			$data[] = $row;
			
		}
		echo json_encode($data);
	}

	function getRecords($params) {
		$data = array();
		$rp = isset($params['rowCount']) ? $params['rowCount'] : 10;
		
		if (isset($params['current'])) { $page  = $params['current']; } else { $page=1; };  
        
        $start_from = ($page-1) * $rp;
		
		$sql = $sqlRec = $sqlTot = $where = '';		

		if( !empty($params['searchPhrase']) ) {  
			// All column names retrieval from the given table
			$sqlTableColumn = "describe ".$params['table_name'];
			$queryColumnNames = mysqli_query($this->conn, $sqlTableColumn) or die("error to fetch data");
			
			while( $rowColumnNames = mysqli_fetch_assoc($queryColumnNames) ) { 
				$columnNames[] = $rowColumnNames;
			}
			
			$where .=" WHERE ";

			foreach($columnNames as $item) 
			{
			    $where .=$item['Field']." LIKE '%".$params['searchPhrase']."%' ";
			    if ($item != end($columnNames))
			    $where .=" OR ";
			} 
	   }
	   if( !empty($params['sort']) ) {  
			$where .=" ORDER By ".key($params['sort']) .' '.current($params['sort'])." ";
		}

	   // getting total number records without any search
		if( !empty($params['sql_query']) ) {
			$sql = $params['sql_query'];
		}
		else {
			$sql = "SELECT * FROM ".$params['table_name'];
		}

		$sqlTot .= $sql;
		$sqlRec .= $sql;		
		
		//concatenate search sql if value exist
		if(isset($where) && $where != '') {

			$sqlTot .= $where;
			$sqlRec .= $where;
		}
		
		if ($rp!=-1)
		$sqlRec .= " LIMIT ". $start_from .",".$rp;

		$qtot = mysqli_query($this->conn, $sqlTot) or die("error to fetch tot data");
		$queryRecords = mysqli_query($this->conn, $sqlRec) or die("error to fetch data");
		
		while( $row = mysqli_fetch_assoc($queryRecords) ) { 
			$data[] = $row;
		}

		$json_data = array(
			"current"            => intval($params['current']), 
			"rowCount"            => 10, 			
			"total"    => intval($qtot->num_rows),
			"rows"            => $data   // total data array
			);
		return $json_data;
	}	
}

?>