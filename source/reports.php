<?php
  ob_start();
  session_start();
   include_once("connection.php");
  
  $db = new dbObj();
  $connString =  $db->getConnstring();
  
  // if session is not set this will redirect to login page
  if ( isset($_SESSION['user'])=="") {
    header("Location: index.php");
    exit;
  }
  if ( isset($_SESSION['user'])!="" && isset($_SESSION['isAdmin'])!="No") {
    header("Location: users.php");
    exit;
  }
  if ( isset($_SESSION['user'])!="" && isset($_SESSION['isAdmin'])!="Yes" && isset($_SESSION['Active'])!="off") {
    header("Location: home.php");
    exit;
  }
  // select loggedin users detail
  $res=mysqli_query($connString, "SELECT * FROM users WHERE UserId=".$_SESSION['user']);
  $userRow=mysqli_fetch_array($res);

  $username = ""; $usernameError = "";
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Welcome - <?php echo $userRow['FullName']; ?></title>
<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css" media="all">
<link href="assets/css/jquery.bootgrid.css" rel="stylesheet" />
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.bootgrid.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js
"></script>
<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>


<link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" type="text/css" />

<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>

  <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">COMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="users.php">Users</a></li>
            <li><a href="transactions.php">Transactions</a></li>
            <li class="active"><a href="reports.php">Reports</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <span class="glyphicon glyphicon-user"></span>&nbsp; <?php echo $userRow['FullName']; ?>&nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav> 

  <div id="wrapper">

  <div class="container">
    
      <div class="page-header">
      <h3>Reports - Pending Loan</h3>
      </div>
        
      <div class="">        
        <div class="col-sm-12">
   
    <table id="User_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
      <thead>
        <tr>
          <th data-column-id="UserId" data-type="numeric" data-identifier="true">UserId</th>
          <th data-column-id="FullName">Name</th>
          <th data-column-id="Phone">Phone</th>
          <th data-column-id="Email">Email</th>
          <th data-column-id="Savings">Savings</th>
          <th data-column-id="Loan">Loan</th>
          <th data-column-id="UserName">UserName</th>
          <th data-column-id="Active">Active</th>
          <th data-column-id="commands" data-formatter="commands" data-sortable="false">Transactions</th>
        </tr>
      </thead>
    </table>
    </div>
      </div>
    </div>
  

 
</div>   
    </div>
    
</body>
</html>
<script type="text/javascript">
$( document ).ready(function() {

  var grid = $("#User_grid").bootgrid({
    ajax: true,
    rowSelect: true,
    post: function ()
    {
      /* To accumulate custom parameter with the request object */
      return {
        id: "b0df282a-0d67-40e5-8558-c9e93b7befed",
        pendingLoan: "Yes"
      };
    },
    
    url: "response.php?pendingLoan=Yes",
    formatters: {
            "commands": function(column, row)
            {
                return "<button type=\"button\" class=\"btn btn-xs btn-default command-transact\" data-row-id=\"" + row.UserId + "\"><span class=\"glyphicon glyphicon-transfer\"></span></button>";
            }
        }
   }).on("loaded.rs.jquery.bootgrid", function()
{
    /* Executes after data is loaded and rendered */
    grid.find(".command-transact").on("click", function(e)
    {
      var ele =$(this).parent();
      window.location="transactions.php?uid="+$(this).data("row-id"); 
    });

});                    
});

</script>

<?php ob_end_flush(); ?>