<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Income/Expense Report</h3>
  	</div>        
    <div class="col-sm-12">

      <div id="filterArea" class="form-row"> 
        <div class="row">
          <div class="col-md-2">
            <label for="selectType" class="control-label">Type</label>
            <select id="selectType" class="form-control"><option value="">All</option><option value="Income">Income</option><option value="Expense">Expense</option></select>
          </div>
          <div class="col-md-2">
            <label for="name" class="control-label">Status</label>
            <select id="selectStatus" class="form-control"><option value="">All</option><option value="Full payment">Full payment</option><option value="Partial payment">Partial payment</option></select>       
          </div>
          <div class="col-md-0">
            <label for="fromDate" class="control-label">From date</label>
            <input elementtype="text" class="form-control" id="fromDate" name="fromDate" value="" placeholder="YYYY-MM-DD" type="text">
          </div>
          <div class="col-md-0">
            <label for="toDate" class="control-label">To date</label>
            <input elementtype="text" class="form-control" id="toDate" name="toDate" value="" placeholder="YYYY-MM-DD" type="text">
          </div>          
          <div class="pull-right" style='padding-bottom:10px; padding-top: 5px'>
            <button type="button" class="btn btn-xs btn-primary" id="btn-pdf" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> PDF</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-xls" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> Excel</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-print" data-row-id="0">
            <span class="glyphicon glyphicon-print"></span> Print</button>
          </div>

          <div class="form-group col-md-4">
            <span class="text-danger"></span>
          </div>

          <div class="row">
          <div class="form-group col-md-4">
          </div>
        </div>            
      </div>
      
      <div id="printArea" class="row">

       
      </div>
     

    </div>
  </div>
</div>


<?php include_once("footer.php"); ?>
</div>

<script>
$(document).ready(function() 
{ 


var db_table_name = [] , db_table_columns = [], table_header = [];

db_table_name = ["incomeexpense"]; // db table names 
db_table_where_colummn = []; // db table names
db_table_columns = ["id","category","description","total_bill_amount","invoice_number","invoice_date","payment_dates","amounts_received","amounts_paid","payment_modes","payments_comments","status","balance"]; // db table column
table_header = ["Sl.No.","Category","Description","Bill amount","Invoice No.","Invoice date","Payment date","Amount received","Amount paid","Payment mode","Comments","Status","Due",]; // db table column

$("#selectType").prop("selectedIndex", 0);
$("#selectStatus").prop("selectedIndex", 2);
$('#fromDate, #toDate').val('');

// date popup

   $('#fromDate, #toDate').parent().addClass('date')
    $('#fromDate, #toDate').datetimepicker({
    format: 'YYYY-MM-DD'
   });

loadAjaxCall();

$("#selectType, #selectStatus").change(function () {
  $("#printArea").html("");
  loadAjaxCall();
});

$("#fromDate, #toDate").blur(function () {
  $("#printArea").html("");
  loadAjaxCall();
});

function loadAjaxCall() {
  var status = $("#selectStatus option:selected").val();
  var acctype = $("#selectType option:selected").val();

  var fromDate = $("#fromDate").val();
  var toDate = $("#toDate").val();


      var sqlQuery = "SELECT GROUP_CONCAT(incomeexpensepayment.payment_mode SEPARATOR ', ') as payment_modes, GROUP_CONCAT(incomeexpensepayment.payment_comments SEPARATOR ', ') as payments_comments, GROUP_CONCAT(incomeexpensepayment.payment_date SEPARATOR ', ') as payment_dates, IF(incomeexpensepayment.amount_received <> 0, GROUP_CONCAT(incomeexpensepayment.amount_received SEPARATOR ', '),0) as amounts_received, IF(incomeexpensepayment.amount_paid <> 0, GROUP_CONCAT(incomeexpensepayment.amount_paid SEPARATOR ', '), 0) as amounts_paid, incomeexpense.* from incomeexpensepayment left join incomeexpense on incomeexpense.id = incomeexpensepayment.incomeexpense_id where 1"
        if(acctype != ''  && acctype != undefined) {
          sqlQuery += " and incomeexpense.type = '" +  acctype + "'"
        }
        else {
          acctype = '';
        }
        if(status != ''  && status != undefined) {
          sqlQuery += " and incomeexpense.status = '" +  status + "'"
        }
        else {
          status = '';
        }

        if(fromDate != '' && toDate != '' && fromDate != undefined && toDate != undefined) {
            sqlQuery += " and incomeexpensepayment.payment_date BETWEEN '" + fromDate + "' AND '" + toDate + "'"
        }

        sqlQuery += " group by incomeexpensepayment.incomeexpense_id order by incomeexpensepayment.payment_date desc"

      console.log(sqlQuery)
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                
                  populateTable(response, status, acctype, fromDate, toDate);               
                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}

function populateTable(response, status, acctype, fromDate, toDate) 
{ 

var htmlTableData = '', dates = '', headingText = '';
  if(acctype != '' && acctype != undefined) {
    headingText = acctype;
  }
  else {
    headingText = 'Income/Expense';
  }

  $(".text-danger").html("");
  if(fromDate != '' && toDate != '' && fromDate != undefined && toDate != undefined) {
        if(moment(toDate).isAfter(fromDate) == false && fromDate != toDate)
          {
            $(".text-danger").html("To date should be greater than or equal to From date");            
          }
          else {
             var fromDateObject = new Date(fromDate);
            var fromDateFormatted = fromDateObject.getDate() + ' ' + monthShortNames[fromDateObject.getMonth()] + ' ' + fromDateObject.getFullYear();

            var toDateObject = new Date(toDate);
            var toDateFormatted = toDateObject.getDate() + ' ' + monthShortNames[toDateObject.getMonth()] + ' ' + toDateObject.getFullYear();

            $(".text-danger").html("");
            // console.log(fromDate, toDate)
            if(fromDate == toDate) {
              dates = '<br>' + fromDateFormatted;
            }
            else
            { 
              dates = '<br>' + fromDateFormatted + ' - ' + toDateFormatted;
            }
          }

 
  }
  else {
    dates = '';
  }


        htmlTableData += '<table class="reportHeader"><tbody><tr><td><b>' + headingText + ' Details ' + dates + '</b></td></tr></tbody></table>' + 
                          '<div class="table-responsive"><table id="tableData" class="table table-bordered table-responsive">' + 
                          '<thead>' +
                          '<tr>';
        var tableHeaderLength = table_header.length;
        for(loopColumns=0;loopColumns<tableHeaderLength;loopColumns++) 
        {
              if($("#selectType option:selected").val() == "Income" && table_header[loopColumns] == "Amount paid")
              {
              }
              else if($("#selectType option:selected").val() == "Expense" && table_header[loopColumns] == "Amount received")
              {
              }
              else {
                htmlTableData += '<th width=100>' + table_header[loopColumns] + '</th>';
              }
                         
           
        }
        htmlTableData += '</tr>' +
                          '</thead>';

        htmlTableData += '<tbody>' 
                          
        var responseLength = response.length;
        for(loopColumns=0;loopColumns<responseLength;loopColumns++) 
        {          
              htmlTableData += '<tr>'
                  var db_table_columns_Length = db_table_columns.length;
                  for(loopHeaders=0; loopHeaders<db_table_columns_Length; loopHeaders++)  {
                    if(loopHeaders==0) {
                      htmlTableData += '<td>' + parseInt(loopColumns+1) + '</td>';
                    }
                    else {
                      // console.log(db_table_columns[loopHeaders]);
                      if($("#selectType option:selected").val() == "Income" && db_table_columns[loopHeaders] == "amounts_paid")
                      {
                      }
                      else if($("#selectType option:selected").val() == "Expense" && db_table_columns[loopHeaders] == "amounts_received")
                      {
                      }
                      else {
                          var rowContent = response[loopColumns][db_table_columns[loopHeaders]];
                          if(rowContent != null)
                          {
                            htmlTableData += '<td>' + response[loopColumns][db_table_columns[loopHeaders]] + '</td>';
                          }
                          else {
                            htmlTableData += '<td>&#160;</td>';
                          }
                      }


                     
                    }
                  }

               htmlTableData += '</tr>'
        }
        htmlTableData += '</tbody></table></div><br><br>';
        // console.log(htmlTableData)
        $("#printArea").html(htmlTableData);
}

});



 $( "#btn-xls" ).click(function() {
      $('#printArea').tableExport({type:'excel'}); 
 }); 

 $( "#btn-print" ).click(function() {
      window.print(); 
 });

 $( "#btn-pdf" ).click(function() {
      pdfExport("printArea");
 });

</script>
</body>
</html>

<?php ob_end_flush(); ?>