  <footer id="footer">
	  <div class="copyright">
	    <div>Copyright © 2018 Understand the Quran Academy</div>
	  </div>
  </footer> 

  <?php
  if(isset($_SESSION['is_admin']) == 1) {
  	echo "<div id='isadmin' style='display:none'>" . $_SESSION['is_admin'] . "</div>";
   }
  ?>

  <script>
  	$(document).ready(function() 
	{ 
		if($("#isadmin").html() == "No") {
			$("#command-add").css("display","none");
		}
	});
  </script>