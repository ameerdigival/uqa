<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Enquiry Details</h3>
  	</div>        
    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","","");


              renderGridHeaderColumn("name","Name",true,"","","","","","");
              renderGridHeaderColumn("phone","Phone",false,"","","","","","");
              renderGridHeaderColumn("whatsapp","Whatsapp",false,"","","","","","");
              renderGridHeaderColumn("address","Address",false,"","","","","false","");
              renderGridHeaderColumn("area","Area",false,"","","","","","");
              renderGridHeaderColumn("city","City",false,"","","","","","");
              renderGridHeaderColumn("country","Country",false,"","","","","false","");
              renderGridHeaderColumn("pincode","Pincode",false,"","","","","false","");
              renderGridHeaderColumn("email","Email",false,"","","","","false","");
              renderGridHeaderColumn("enquiry_category","Category",false,"","","","","","");
              renderGridHeaderColumn("enquiry_date","Enq. Date",false,"","","","","","");
              renderGridHeaderColumn("book_language","Book language",false,"","","","","false","");
              renderGridHeaderColumn("book_names","Book names",false,"","","","","false","");
              renderGridHeaderColumn("job_category","Job category",false,"","","","","false","");
              renderGridHeaderColumn("comments","Comments",false,"","","","","false","");
              renderGridHeaderColumn("status","Status",true,"","","","","","");
              renderGridHeaderColumn("followup_comments","Followup",false,"review","","","","","");


              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>

<!---- Add/Edit Form -->

<div id="edit_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo "<div class='form-sub-header'>Personal / Contact Information</div>";
            echo '<div class="row">';         
            renderFormInput("name","Name",'data-validation="alphanumeric" data-validation-allowing=". "',"","col-md-4");
            renderFormInput("phone","Phone",'data-validation="required"',"","col-md-4");
            renderFormInput("whatsapp","Whatsapp",'',"","col-md-4");
            echo '</div>';

            echo "<br><div class='form-sub-header'>Address Information</div>";
            echo '<div class="row">';
            renderFormInput("email","Email",'data-validation="email" data-validation-error-msg="Enter a valid e-mail" data-validation-optional="true" placeholder="example@domain.com"',"","col-md-4");
            renderFormInput("address","Address",'',"","col-md-4");            
            renderFormInput("area","Area",'',"","col-md-4");
            echo '</div>';

            echo '<div class="row">';
            renderFormInput("city","City",'',"","col-md-4");           
            renderFormInput("country","Country",'',"","col-md-4");
            renderFormInput("pincode","Post / Zip code",'',"","col-md-4");
            echo '</div>';
            
            echo "<br><div class='form-sub-header'>Additional Information</div>";
            echo '<div class="row">';            
            renderFormRadio("enquiry_category","Enquiry category",'data-validation="required"',['Books','Course','eLearning Card','Job','Others'],'',"");
            renderFormInput("enquiry_date","Enquiry date",'data-validation="required" placeholder="YYYY-MM-DD"',"","col-md-3");
            echo '</div>';

            echo '<div class="row">';
            renderFormSelect("book_language","Book language",' multiple',['Tamil','English','Urdu'],['Tamil','English','Urdu'],[''],[],"col-md-4");

            $optionTexts=[]; $optionValues=[]; $optionSubTexts=[];
              $sqlResult = fetchRecordForDropdown("select id, name, unit_price from inventory where category = 'Books' order by name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['id'] | $json[$loop]['unit_price']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;unit price: ' .  $json[$loop]['unit_price']);
              } 

              renderFormSelect("book_names","Book name(s)",' multiple',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-8");

            echo '</div>';

            echo '<div class="row">';
            renderFormRadio("job_category","Job category",'',['Staff','Teaching'],"","");
            renderFormTextarea("comments","Comments",'',"","");
            echo '</div>';

            echo "<br><div class='form-sub-header'>Follow-up Information</div>";
            echo '<div class="row">';
            renderFormRadio("status","Follow-up Status",'data-validation="required"',['Open','Closed'],'Open',"");
            renderFormTextarea("followup_comments","Follow-up comments",'',"","");
            echo '</div>';

            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>

<script>
  $(document).ready(function() 
  { 
      
    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["enquiry"]]; // db table names for insert
    db_table_insert_columns = [["name","phone","whatsapp","address","area","city","country","pincode","email","enquiry_category","book_language","book_names","job_category","enquiry_date","comments","status","followup_comments"]]; // db table column names for insert

    db_table_names_update = [["enquiry"]]; // db table names for update
    db_table_update_columns = [["name","phone","whatsapp","address","area","city","country","pincode","email","enquiry_category","book_language","book_names","job_category","enquiry_date","comments","status","followup_comments"]]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   


    
    // load data grid
    loadBootgrid(db_table_names_insert[0].toString());   

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal
            formReset('frm_edit');
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data              
              $('#' + formId+' #id').val($(this).data("row-id")); // in case we're changing the key
              // ajax call
              sqlQuery = 'select * from ' + db_table_names_update + ' where id=' + $(this).data("row-id");
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                          populateEditFormData(response);
                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });
              } else 
              {  //alert('No row selected! First select row, then click edit button');
              }
          }); 
    });




function populateEditFormData(dataArray) {
  // populate the data to the form
  formReset('frm_edit');
  var dataArrayLength = dataArray[0].length, formElementValue = '';

  if(dataArray.length > 0) 
  {
    
      loopColumnsArrayLength = db_table_update_columns[0].length;

        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = dataArray[0][formElementId];
          

          
            if($("#"+formElementId).attr('elementType') === "text") {
              $('#' + formElementId).val(formElementValue);
            }

            if($("#" + formElementId).attr('elementType') === "radio") {

              formInteractions(formElementId);
              
              if(formElementValue!="") {
                $("input[id="+formElementId+"][value='" + formElementValue + "']").prop('checked', true); 
              }
              
            }

            if($("#"+formElementId).attr('elementType') === "select") {
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
              formElementValueSplit = formElementValue.split(",");
              
              if(formElementValueSplit.length>0) {
                for (loop = 0; loop<formElementValueSplit.length; loop++)
                {
                  $("#" + formElementId + " option[value='" + formElementValueSplit[loop] + "']").prop("selected",true);
                }
              }
              else {
                $("#"+formElementId+"[value=" + formElementValue + "]").prop("selected",true);
              }
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
            }

            if($("#" + formElementId).attr("elementType") === "textarea") {
              $('#' + formElementId).val(formElementValue);
            }
        }
  }

}

   /************** form interactions *************/

   $('#div_book_language').hide();
   $('#div_book_names').hide();
   $('#div_job_category').hide();

   var eventElementId = "enquiry_category";
   $('input:radio[id='+eventElementId+']').change(function () {
        formInteractions(eventElementId);
    });


   // date popup

   $('#enquiry_date').parent().addClass('date')
    $('#enquiry_date').datetimepicker({
    format: 'YYYY-MM-DD'
   });

   function formInteractions(elementId)
   {
        if ($("input[id="+eventElementId+"]:checked").val() == 'Books') {
          $('#div_book_language').show();
          $('#div_book_language .formOptionalText').hide();
          $('#div_book_names').show();
          $('#div_book_names .formOptionalText').hide();
          $('#book_language').attr("data-validation", "required");
          $('#book_names').attr("data-validation", "required");
        }
        else {
          $('#div_book_language').hide();
          $('#div_book_names').hide();
          $('#book_language').attr("data-validation", "");
          $('input[id="book_language"]').prop('checked', false);
          $('#book_names').attr("data-validation", "");
          $('#book_names').val('default').selectpicker("refresh");
        }

        if ($("input[id="+eventElementId+"]:checked").val() == 'Job') {
          $('#div_job_category').show();
          $('#job_category').attr("data-validation", "required");
        }
        else {
          $('#div_job_category').hide();
          $('#div_job_category .formOptionalText').hide();
          $('#job_category').attr("data-validation", "");
          $('input[id="job_category"]').prop('checked', false);
        }
   }

   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
      sqlUpdateQueryArray.length=0;
      sqlInsertQueryArray.length=0;
      db_table_update_values.length=0;
      db_table_insert_values.length=0;

      if(db_table_update_columns.length!=0) {
        var tempArray = [], 
            formValue = '', 
            formElementId = '', 
            formElementValue = '', 
            loopColumnsArrayLength =0;


        /************* get edit form values and push to array ***************/

        loopColumnsArrayLength = db_table_update_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
            tempArray.push(formElementValue); 
        }
        db_table_update_values.push(tempArray);



         /************* get add form values and push to array ***************/

        loopColumnsArrayLength = db_table_insert_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_insert_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
          tempArray.push(formElementValue);                    
        }
        db_table_insert_values.push(tempArray);
      }

      create_db_table_sql_update_query($('#' + formId + ' #id').val()); // For update query - db table row unique id as parameter
      create_db_table_sql_insert_query(); // For insert query 
      ajaxAction($('#action').val());  // parameter "both" executes both insert and update sql
      return false; // Will stop the submission of the form
    }
  });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>