// Database table for insert values - all in arrays for multi tables and its columns, if any
    var db_table_names_insert = []; // db table names for insert
    var db_table_insert_columns = []; // db table column names for insert
    var db_table_insert_values = [[],[]]; // db table column values for insert

    var db_table_names_update = []; // db table names for update
    var db_table_update_columns = []; // db table column names for update
    var db_table_update_values = [[]]; // db table column values for insert

    var sqlUpdateQueryArray = [];  // query array for sql update
    var sqlInsertQueryArray = [];  // query array for sql insert

    var dayFullNames = [];  // array for days
    var weekDayFullNames = [];  //  days full names
    var weekDayShortNames = [];  //  days short names
    var monthFullNames = [];  //  month full names
    var monthShortNames = [];  //  month full names 

    var validateOnBlur, errorMessagePosition, scrollToTopOnError, validateErrorMessage;

    dayFullNames = ["Monday" , "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    weekDayFullNames = ["Sunday", "Monday" , "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    weekDayShortNames = ["Sun", "Mon" , "Tue", "Wed", "Thu", "Fri", "Sat"];
    monthFullNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    monthShortNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    _validateOnBlur = true; // disable validation when input looses focus
    _errorMessagePosition = 'inline'; // Instead of 'inline' which is default, you can set to 'top'
    _scrollToTopOnError = true; // Set this property to true on longer forms 
    _validateErrorMessage = "Fill up the required data..." // Set this property to display alert validation error 

    var compare_dates = (function(){
    "user strict";

    var output;

    return function(date1, date2){
    var date1 = new Date(date1), date2 = new Date(date2); 
    var time1 = date1.getTime(), time2 = date2.getTime(), diff = time1-time2;
    if (diff > 0){
    output = 'd1 > d2';
    }else if (diff < 0){
    output = 'd1 < d2';
    }else{
    output = 'd1 = d2';
    }
    return output;
    };

    })();

function arrUnique(a){
  var t = [];
  for(var x = 0; x < a.length; x++){
    if(t.indexOf(a[x]) == -1)t.push(a[x]);
  }
  return t;
}

function formatDate(dateValue) {
  var dateObject = new Date(dateValue);
  return dateObject.getDate() + ' ' + monthShortNames[dateObject.getMonth()] + ' ' + dateObject.getFullYear();
}

function pdfExport(htmlElement) {
var pdf = new jsPDF('p', 'pt', 'letter'),
                            margins = {
                              top: 30,
                              bottom: 30,
                              left: 30,
                              width: 800
                          };
      pdf.canvas.height = 72 * 11;
      pdf.canvas.width = 72 * 8.5;
      pdf.setFont("arial");
      pdf.setFontSize("9");      

      pdf.fromHTML(
        document.getElementById(htmlElement),
        margins.left, // x coord
            margins.top, { // y coord
                'width': margins.width
              });
      pdf.save('uqatamil_download.pdf'); 
}



function formReset(formId) {
  $('#' + formId).trigger("reset");
  $('.modal-body').animate({ scrollTop: 0 }, 'fast')
  $('.selectpicker').selectpicker('refresh');
} 

$(window).on('shown.bs.modal', function (e) {
    $(".modal-body").scrollTop();

    $("#frm_edit input").each(function() {
      // console.log(this.id + '=' + this.value);
       if (this.value == '0000-00-00') {
          this.value = '';
       }
       });
});

$( document ).ready(function() {

 $.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';

 $('form').attr('autocomplete', 'off');

  var alerModalDialogHtml = '<div id="alert_modal" class="modal fade">' +
                            '<div class="modal-dialog alert-box">' +
                              '<div class="modal-content">' + 
                                '<div class="modal-body alert-modal-content">' +                                  
                                '</div>' +
                              '</div>' +
                            '</div>' +
                          '</div>';
  $(alerModalDialogHtml).insertAfter('#edit_modal');

  $( "#command-add" ).click(function() {
    formReset('frm_edit');
    $('.modal-title').html('Add - '+$('.page-header h3').text());
    $('#action').val('add');
    $('#edit_modal').modal('show'); 
    $('.modal-body').animate({ scrollTop: 0 }, 'fast')  
  });


  $("#frm_delete").submit(function(){
      return false;
  });

  $("#frm_edit").submit(function(){
      return false;
  });


  $( "#btn_delete" ).click(function() 
  {
    data = {
          action:'delete',
          table_name: db_table_names_insert[0].toString(),
          where_field_name: 'id',
          delete_id: $('#frm_delete #delete_id').val()
         };

    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json", 
             success: function(response)  
              { 
                if(response.status != -1) 
                {
                  alertm("Details are deleted...");
                  $('#delete_modal').modal('hide');
                  $("#data_grid").bootgrid('reload'); 
                }
                else 
                {
                  alert("ERROR: " + response);
                }               
              },
              error: function(req, status, error)  
              {
                alert("Error: \n"+status+"\n"+error);
              }
            });              
});

  return false;                
});

function showError(elementId, errorText) {console.log($("#span"+elementId))
  if($("#span"+elementId).html() == 'undefined' || $("#span"+elementId).html() == '') {
    $("#"+elementId).after('<span id="span' + elementId + '" class="help-block form-error">' + errorText + '</span>');
  }
  else {
    $("#span"+elementId).html(errorText);
  }
}

function removeError(elementId) {
  $("#span"+elementId).remove();
}

function create_db_table_sql_update_query(rowid)
    {      
        if(db_table_names_insert.length!=0) 
        {            
          for(var indexTable=0; indexTable<db_table_names_update.length; indexTable++) 
          {
          var sqlQueryText = "";
          sqlQueryText += "UPDATE " + db_table_names_update[indexTable] + " SET ";

            if(db_table_update_columns.length!=0) 
            {              
              for(var indexColumnsArr=0; indexColumnsArr<db_table_update_columns[indexTable].length; indexColumnsArr++) 
              { 

                    if(db_table_update_values[indexTable][indexColumnsArr] == '') {
                    sqlQueryText += db_table_update_columns[indexTable][indexColumnsArr] + "=NULL";
                    }
                    else 
                    {
                      // console.log(db_table_update_columns[indexTable][indexColumnsArr] +" " + db_table_update_values[indexTable][indexColumnsArr])
                    sqlQueryText += db_table_update_columns[indexTable][indexColumnsArr] + "='"+db_table_update_values[indexTable][indexColumnsArr].toString().replace(/'/g, "\\'")+"'"; 
                    }                  

                    if(indexColumnsArr!=(db_table_update_columns[indexTable].length-1) && db_table_update_columns[indexTable].length!=1) 
                    {
                      sqlQueryText += ", ";                   
                    }
              }
            }
            sqlQueryText += " WHERE id='"+rowid+"'";
            sqlUpdateQueryArray.push(sqlQueryText);
            sqlQueryText="";
            console.log("sqlUpdateQuery "+(indexTable+1)+": "+sqlUpdateQueryArray[indexTable]);            
          }
        }
    }

    function create_db_table_sql_insert_query(sqlCmd, rowid)
    {    
        if(db_table_names_insert.length!=0) 
        {            
          for(var indexTable=0; indexTable<db_table_names_insert.length; indexTable++) 
          {
          var sqlQueryText = "";
          sqlQueryText += "INSERT INTO " + db_table_names_insert[indexTable] + " (";

            if(db_table_insert_columns.length!=0) 
            {              
              for(var indexColumnsArr=0; indexColumnsArr<db_table_insert_columns[indexTable].length; indexColumnsArr++) 
              {                
                    sqlQueryText += db_table_insert_columns[indexTable][indexColumnsArr];

                  if(indexColumnsArr!=(db_table_insert_columns[indexTable].length-1) && db_table_insert_columns[indexTable].length!=1) 
                  {
                    sqlQueryText += ", ";                   
                  }
              }
              sqlQueryText += ") VALUES(";
              for(var indexColumnsArr=0; indexColumnsArr<db_table_insert_columns[indexTable].length; indexColumnsArr++) 
              {     
                    if(db_table_insert_values[indexTable][indexColumnsArr] == '') {
                    sqlQueryText += "NULL";
                    }
                    else 
                    {
                    sqlQueryText += "'"+db_table_insert_values[indexTable][indexColumnsArr].toString().replace(/'/g, "\\'")+"'"; 
                    }

                  if(indexColumnsArr!=(db_table_insert_columns[indexTable].length-1) && db_table_insert_columns[indexTable].length!=1) 
                  {
                    sqlQueryText += ", ";                  
                  }
              }
            }
            sqlQueryText += ")";
            sqlInsertQueryArray.push(sqlQueryText);
            sqlQueryText="";
            console.log("sqlInsertQuery "+(indexTable+1)+": "+sqlInsertQueryArray[indexTable]);            
          }
        }
    }


// function to get record using ajajx
function fetchRecord(sqlQuery) 
{
    data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                return response;
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}

// function to load data grid
function loadBootgrid(tableName, sqlQuery) {

   var grid = $("#data_grid").bootgrid
   ({
      ajax: true,
      rowSelect: true,
      "review": function(column, row) {
      return '<span title="'+row.review+'">'+row.review+'</span>';
      },
      post: function ()
            {
                  /* To accumulate custom parameter with the request object */
                  return {
                    id: "b0df282a-0d67-40e5-8558-c9e93b7befee",
                    table_name: tableName,
                    sql_query: sqlQuery          
                  };
            },    
      url: "response.php",
      formatters: 
            {
              "commands": function(column, row)
              {
                  return '<button type=\"button\" data-toggle="tooltip" title="Edit" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"' + row.id + '\"><i class="fa fa-edit" aria-hidden="true"></i></button> ' + 
                      '<button type=\"button\"  data-toggle="tooltip" title="Delete" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"' + row.id + '\"><i class="fa fa-trash" aria-hidden="true"></i></span></button>';
              }
            }
  }).on("loaded.rs.jquery.bootgrid", function()
  {        

          grid.find(".command-delete").on("click", function(e)
          {
              // show delete modal
              $('#delete_modal').modal('show');
              $('.modal-title').html('Delete - '+$('.page-header h3').text());

              // collect the data  
              if($(this).data("row-id") >0) 
              {              
                // collect the data
                var ele =$(this).parent();
                $('#frm_delete #delete_id').val($(this).data("row-id"));
                var ele =$(this).parent();
                strInfo = "Are you sure to delete this info from the record?<br><br><div class='deleteInfoText'>Name: " +
                ele.siblings(':nth-of-type(2)').text().trim() + "<br>ID: " + $(this).data("row-id") +
                "<br><br>"+"</div>";
                $('#frm_delete #deleteConfirmInfo').html(strInfo); // in case we're changing the key                                                        
              }                    
            });
      $('[data-toggle="tooltip"]').tooltip(); 
  });
}


function alertm(alertContent) {
  $('.alert-modal-content').html(alertContent);
  $('#alert_modal').modal('show');
  setTimeout(function () {
      $('#alert_modal').modal('hide');
  }, 1500);
}

function alert_presave(alertContent) {
  $('.alert-modal-content').html(alertContent);
  $('#alert_modal').modal('show');
}

function alert_postsave(alertContent) {
  $('.alert-modal-content').html(alertContent);
  setTimeout(function () {
      $('#alert_modal').modal('hide');
  }, 1500);
}


function ajaxAction(action) 
{
alert_presave("Processing...");

   // ajax call
  data = {
          action:action,
          sqlInsertQueryArray: sqlInsertQueryArray,
          sqlUpdateQueryArray: sqlUpdateQueryArray
         };
  $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            {        
              if(response.status!=-1) 
              {                
                $('#edit_modal').modal('hide');
                alert_postsave("Details are saved...");         
                $("#data_grid").bootgrid('reload');
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
        });
}

function isValidField(formID, elementId, elementType) 
{
  // form validation
  if(elementType="text") 
  {
    if($("#"+formID + " #" + elementId).val()=="") 
    { 
      $("#"+formID + " #" + elementId).next(".text-danger").html("This cannot be empty"); 
      $("#"+formID + " #" + elementId).focus(); 
      return false; 
    } 
    else 
    { 
      $("#"+formID + " #" + elementId).next(".text-danger").html("");
      return true;
    }
  }
}