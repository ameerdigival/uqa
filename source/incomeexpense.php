<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Income / Expense Details</h3>
  	</div>     

<!---- Add/Edit Form -->

<div id="edit_modal" class="modal fade">-->
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo "<div class='form-sub-header'>Basic Information</div>";
              echo '<div class="row">';
                renderFormRadio("type","Accounts type",'data-validation="required"',['Income','Expense'],"","");
              echo "</div>";

                echo '<div class="row">';
                renderFormSelect("category","Category",'data-validation="required"',[],[],[],[],"col-md-4");
                 echo "</div>";

                 echo '<div class="row">';
                  
                  $optionTexts=[]; $optionValues=[]; $optionSubTexts=[];
                  $sqlResult = fetchRecordForDropdown("select id, name, area, city from school order by name");              
                  $json = json_decode($sqlResult, true);
                  $sqlRecordCount = count($json);

                  for($loop=0; $loop < $sqlRecordCount; $loop++) {
                    array_push($optionValues, $json[$loop]['id']);
                    array_push($optionTexts, $json[$loop]['name']);
                    array_push($optionSubTexts, '<br>&#160;&#160;City: ' .  $json[$loop]['city'] . '&#160;&#160;Area: ' .  $json[$loop]['area']);
                  } 

                  renderFormSelect("school_id","School",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-5");


                  $optionTexts=[]; $optionValues=[]; $optionSubTexts=[];
                  $sqlResult = fetchRecordForDropdown('select studentenrolment.id as enrolmentid, studentenrolment.student_id, batchdetails.name as batchname, students.name as studentname, courses.fullname as coursename, studentenrolment.enrolment_date, (studentenrolment.course_fees+studentenrolment.books_fees-studentenrolment.discount)-(SELECT IFNULL(sum(amount_received),0) FROM incomeexpense where incomeexpense.student_enrolment_id = studentenrolment.id) as dueamount, (studentenrolment.course_fees+studentenrolment.books_fees-studentenrolment.discount) as payableamount from studentenrolment left join students on students.id = studentenrolment.student_id left join courses on courses.id = studentenrolment.course_id left join batchdetails on batchdetails.batch_id = studentenrolment.batch_id where studentenrolment.status <> "Certified" order by students.name');
                  $json = json_decode($sqlResult, true);
                  $sqlRecordCount = count($json);

                  for($loop=0; $loop < $sqlRecordCount; $loop++) {
                    array_push($optionValues, $json[$loop]['enrolmentid'] . '|' . $json[$loop]['payableamount'] . '##$$' . $json[$loop]['student_id']);
                    array_push($optionTexts, $json[$loop]['studentname']);
                    array_push($optionSubTexts, '<br>&#160;&#160;Enroll Date: ' .  $json[$loop]['enrolment_date'] . '&#160;&#160;Course: ' .  $json[$loop]['coursename'] . ' ' . $json[$loop]['batchname'] . '&#160;&#160;Payableamount: ' .  $json[$loop]['payableamount'] . '&#160;&#160;Due: ' .  $json[$loop]['dueamount']);
                  } 

                  renderFormSelect("student_enrolment_id","Student",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-5");


                  $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

                  $sqlResult = fetchRecordForDropdown("select id, batch_id, name, start_date, end_date, fees, status from batchdetails order by name");              
                  $json = json_decode($sqlResult, true);
                  $sqlRecordCount = count($json);

                  for($loop=0; $loop < $sqlRecordCount; $loop++) {
                    array_push($optionValues, $json[$loop]['batch_id']);
                    array_push($optionTexts, $json[$loop]['name']);
                    array_push($optionSubTexts, '<br>&#160;&#160;ID: ' .  $json[$loop]['id'] . '  &#160;&#160;Fees: ' . $json[$loop]['fees'] . '  &#160;&#160;Batch Date: ' . $json[$loop]['start_date'] . ' to ' . $json[$loop]['end_date'] . '  &#160;&#160;status: ' . $json[$loop]['status']);
                  }              

                  renderFormSelect("batch_id","Batch",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-4"); 

                  
            echo '</div>';

            echo "<br><div class='form-sub-header'>Bill Information</div>";
            
            echo '<div class="row">';
              renderFormInput("description","Description",'data-validation="alphanumeric" data-validation-allowing="#(),-. "',"","col-md-6");
            echo '</div>';


              echo '<div class="row">';
              renderFormInput("total_bill_amount","Total bill amount",'data-validation="number" data-validation-allowing="float"',"","col-md-2");
              renderFormInput("invoice_date","Invoice date",'placeholder="YYYY-MM-DD"',"","col-md-3");
              renderFormInput("invoice_number","Invoice number",'data-validation="alphanumeric" data-validation-optional="true" data-validation-allowing="#(),-. "',"","col-md-3");              
            echo '</div>';


            echo "<br><div class='form-sub-header'>Payment Information</div>";
            echo '<div class="row"><div id="paymentHistory" style="max-width: 650px;"></div>';
                renderFormInput("payableamount","Payable amount",'disabled data-validation="number" data-validation-allowing="float"',"","col-md-3");                 
                echo '</div>';

                echo '<div class="row">';  
              renderFormInput("amount_received","Amount received",'data-validation="number" data-validation-allowing="float"',"","col-md-2");
              renderFormInput("amount_paid","Amount paid",'data-validation="number" data-validation-allowing="float"',"","col-md-2");
              renderFormInput("payment_date","Payment date",'data-validation="required" placeholder="YYYY-MM-DD"',"","col-md-3");
              renderFormInput("balance","Amount due",'disabled data-validation="number" data-validation-allowing="float"',"","col-md-2");
              renderFormInput("payment_comments","Payment comments",'data-validation="alphanumeric" data-validation-allowing="#(),-. "',"","col-md-5");
              echo '</div>';

              echo '<div class="row">';
              renderFormRadio("payment_mode","Payment mode",'data-validation="required"',['Cash', 'Bank'],"","col-md-3");            
              renderFormRadio("status","Payment status",'data-validation="required"',['Partial payment','Full payment'],"","");
              echo "</div>";


            echo "<br><div class='form-sub-header'>Additional Information</div>";
            echo '<div class="row">';
            renderFormTextarea("comments","Comments",'',"","");
            echo '</div>';

            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","","");


              renderGridHeaderColumn("type","Type",true,"","","","","true","");
              renderGridHeaderColumn("category","Category",true,"","","","","true","");
              renderGridHeaderColumn("schoolname","School",false,"","","","","false","");
              renderGridHeaderColumn("school_id","School id",false,"","","","","false","");
              renderGridHeaderColumn("batchname","Batch",false,"","","","","false","");
              renderGridHeaderColumn("batch_id","Batch id",false,"","","","","false","");
              renderGridHeaderColumn("description","Description",false,"","","","","false","");
              renderGridHeaderColumn("total_bill_amount","Total bill amount",false,"","","","","true","");
              renderGridHeaderColumn("amount_received","Amount received",false,"","","","","true","");
              renderGridHeaderColumn("amount_paid","Amount paid",false,"","","","","true","");
              renderGridHeaderColumn("balance","Amount due",true,"","","","","true","");
              renderGridHeaderColumn("invoice_number","Invoice number",false,"","","","","false","");
              renderGridHeaderColumn("invoice_date","Invoice date",false,"","","","","false","");
              renderGridHeaderColumn("payment_date","Payment date",false,"","","","","false","");
              renderGridHeaderColumn("status","Status",false,"","","","","true","");
              renderGridHeaderColumn("comments","Comments",false,"","","","","false","");


              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>



<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>

<script>
  $(document).ready(function() 
  { 

    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["incomeexpense"]]; // db table names for insert
    db_table_insert_columns = [["type","category","student_enrolment_id", "school_id","batch_id","description","total_bill_amount","amount_received","amount_paid","balance","invoice_number","invoice_date","payment_date","payment_mode", "payment_comments", "status","comments"]]; // db table column names for insert

    db_table_names_update = [["incomeexpense"]]; // db table names for update
    db_table_update_columns = [["type","category","student_enrolment_id", "school_id","batch_id","description","total_bill_amount","amount_received","amount_paid","balance","invoice_number","invoice_date","payment_date","payment_date","payment_mode", "payment_comments", "status","comments"]]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   


    
    // load data grid
    loadBootgrid('incomeexpense_view');   

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal
            formReset('frm_edit');
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data              
              $('#' + formId+' #id').val($(this).data("row-id")); // in case we're changing the key
              // ajax call
              sqlQuery = 'select * from ' + db_table_names_update + ' where id=' + $(this).data("row-id");
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                          populateEditFormData(response);
                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });
              } else 
              {  //alert('No row selected! First select row, then click edit button');
              }
          }); 
    });



var paymentHistory = '', amount_paid = 0, amount_received = 0, previous_amount_paid = 0, previous_amount_received = 0, payment_date = '', payment_mode = '', payment_comments = '', incomeexpense_id = 0;
function populateEditFormData(dataArray) {
  // populate the data to the form
  formReset('frm_edit');
  var dataArrayLength = dataArray[0].length, formElementValue = '';

  if(dataArray.length > 0) 
  {
    
      loopColumnsArrayLength = db_table_update_columns[0].length;

        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = dataArray[0][formElementId];                    

            if($("#"+formElementId).attr('elementType') === "text") {
              $('#' + formElementId).val(formElementValue);
            }

            if($("#" + formElementId).attr('elementType') === "radio") {
                if(dataArray[0][formElementId] == 'Income') {
                  populateCategory('Income');
                  $("#div_amount_received").show();
                  $("#div_amount_paid").hide();
                }
                if(dataArray[0][formElementId] == 'Expense') {
                  populateCategory('Expense');
                  $("#div_amount_paid").show();
                  $("#div_amount_received").hide();
                }
              
              formInteractions(formElementId);
              
              if(formElementValue!="") {
                $("input[id="+formElementId+"][value='" + formElementValue + "']").prop('checked', true); 
              }
              
            }

            if($("#"+formElementId).attr('elementType') === "select") {
              if(dataArray[0][formElementId] == 'School') {
                  $("#div_school_id").show();
                   $("#div_batch_id").hide();
                   $("#div_student_enrolment_id").hide();
                }
                if(dataArray[0][formElementId] == 'Batch') {
                  $("#div_batch_id").show();
                  $("#div_school_id").hide();
                  $("#div_student_enrolment_id").hide();
                }
                if(dataArray[0][formElementId] == 'Student') {
                  $("#div_student_enrolment_id").show();
                  $("#div_batch_id").hide();
                  $("#div_school_id").hide();                  
                }
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
              if(formElementValue != null || formElementValue != undefined)
                {
                    formElementValueSplit = formElementValue.split(",");
                    
                    if(formElementValueSplit.length>0) {
                      for (loop = 0; loop<formElementValueSplit.length; loop++)
                      {
                        $("#" + formElementId + " option[value='" + formElementValueSplit[loop] + "']").prop("selected",true);
                      }
                }
              }
              else {
                $("#"+formElementId+"[value=" + formElementValue + "]").prop("selected",true);
              }
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
            }

            if($("#" + formElementId).attr("elementType") === "textarea") {
              $('#' + formElementId).val(formElementValue);
            }
        }
  }


$("#total_bill_amount").attr('disabled','true');
$('#payableamount').val(dataArray[0]["total_bill_amount"]);
$("#paymentHistory").html('');
incomeexpense_id = dataArray[0]["id"];
sqlQuery = 'select * from incomeexpensepayment where incomeexpense_id=' + incomeexpense_id + ' order by payment_date desc';
console.log(sqlQuery)
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                          if(response.length > 0) {
                          previous_amount_received = 0, previous_amount_paid = 0;                          
                            paymentHistory = '<label class="control-label">Payment History</label><table class="table table-bordered table-responsive"">'
                            for(loopColumns=0;loopColumns<response.length;loopColumns++) 
                            {
                                  amount_paid = response[loopColumns]["amount_paid"];
                                   amount_received = response[loopColumns]["amount_received"];
                                   payment_date = response[loopColumns]["payment_date"]; 
                                   payment_mode = response[loopColumns]["payment_mode"]; 
                                   payment_comments = response[loopColumns]["payment_comments"]; 

                                  if(loopColumns==0) 
                                  {

                                      paymentHistory += '<tr bgcolor="#f1f1f1">'
                                      paymentHistory += '<th>Payment date</th>'
                                      if(amount_paid > 0) {
                                        paymentHistory += '<th>Amount paid</th>'
                                        
                                      }
                                      if(amount_received > 0) {
                                        paymentHistory += '<th>Amount received</th>'
                                        
                                      }                                                 
                                        
                                        paymentHistory += '<th>Payment mode</th>'
                                        paymentHistory += '<th>Comments</th>'
                                        paymentHistory += '</tr>'
                                  }

                                      paymentHistory += '<tr>'
                                      paymentHistory += '<td>' + payment_date + '</td>'
                                      if(amount_paid > 0) {
                                        paymentHistory += '<td>' + amount_paid + '</td>'
                                        previous_amount_paid+=parseInt(amount_paid);
                                      }
                                      if(amount_received > 0) {
                                        paymentHistory += '<td>' + amount_received + '</td>'
                                        previous_amount_received+=parseInt(amount_received);
                                      }                                                      
                                        paymentHistory += '<td>' + payment_mode + '</td>'
                                        paymentHistory += '<td>' + payment_comments + '</td>'
                                        paymentHistory += '</tr>'
                              
                            }
                            paymentHistory += '</table>';

                            $("#paymentHistory").html(paymentHistory);
                            $('#payableamount').val(parseInt(dataArray[0]["total_bill_amount"])-parseInt(previous_amount_paid)-parseInt(previous_amount_received));
                            $("#amount_received").val('');
                            $("#amount_paid").val('');
                            $("#payment_date").val('');
                          }
                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });

}

   /************** form interactions *************/

   var income_category = ["Student","School","Batch","Sales","General"];
   var expense_category = ["School","Batch","Purchase","General"];

   $('#div_school_id').hide();
   $('#div_batch_id').hide();
   $('#div_student_enrolment_id').hide();
   $("#div_amount_paid").hide();
   $("#div_amount_received").hide();

   $('#category').change(function () {
    //console.log("selected:"+$("#" + eventElementId).val());
     if ($("#category option:selected").val() != '' && $("#category option:selected").val() != 'undefined') 
     {
      $('#school_id').val('default').selectpicker("refresh");
      $('#batch_id').val('default').selectpicker("refresh");
      $('#student_enrolment_id').val('default').selectpicker("refresh");

         if ($("#category option:selected").val() == 'School') {          
          $('#div_school_id').show();
          $('#div_school_id').prop("data-validation='required'");
          $('#div_batch_id').hide();
          $('#batch_id').prop("data-validation=''");  
          $('#div_student_enrolment_id').hide();
          $('#student_enrolment_id').prop("data-validation=''"); 
         }
         else {
          $('#div_school_id').hide();
          $('#div_school_id').prop("data-validation=''");
         }

         if ($("#category option:selected").val() == 'Batch') {
          $('#div_batch_id').show();
          $('#batch_id').prop("data-validation='required'"); 
          $('#div_student_enrolment_id').hide();
          $('#student_enrolment_id').prop("data-validation=''");
          $('#div_school_id').hide();
          $('#school_id').prop("data-validation=''");        
         }
         else {
          $('#div_batch_id').hide();
          $('#batch_id').prop("data-validation=''");  
         }

         if ($("#category option:selected").val() == 'Student') {
          $('#div_student_enrolment_id').show();
          $('#student_enrolment_id').prop("data-validation='required'");
          $('#div_batch_id').hide();
          $('#batch_id').prop("data-validation=''");  
          $('#div_school_id').hide();
          $('#school_id').prop("data-validation=''");       
         }
         else {
          $('#div_student_enrolment_id').hide();
          $('#student_enrolment_id').prop("data-validation=''");
         }

      }
   }); 

   $(window).on('shown.bs.modal', function (e) {
    if($("#action").val() == "edit") {
      $("#batch_id").attr('disabled','disabled');
      $('#batch_id').selectpicker("refresh");
      $("#student_enrolment_id").attr('disabled','disabled');
      $('#student_enrolment_id').selectpicker("refresh");
      $("#category").attr('disabled','disabled');
      $('#category').selectpicker("refresh");
      $('#type').attr('disabled','disabled');  
    }
    else {
      $("#batch_id").removeAttr('disabled');
      $('#batch_id').selectpicker("refresh");
      $("#student_enrolment_id").removeAttr('disabled');
      $('#student_enrolment_id').selectpicker("refresh");
      $("#category").removeAttr('disabled');
      $('#category').selectpicker("refresh");
      $('#type').removeAttr('disabled'); 
    } 
  });

   $('#student_enrolment_id').change(function() {
    var studentData = $("#student_enrolment_id option:selected").attr("data-value"); 
    var studentDataSplitted = studentData.split("##$$"); 
       $('#total_bill_amount, #payableamount').val(studentDataSplitted[0]);
    });


$( "#command-add" ).click(function() {
     $("#paymentHistory").html(''); 
     $("#total_bill_amount").removeAttr('disabled');
     incomeexpense_id = 0;
     $('#amount_paid').val('');
     $('#amount_received').val('');
  });

$( "#command-edit" ).click(function() {
     $("#paymentHistory").html(''); 
     incomeexpense_id = 0;
     $('#amount_paid').val('');
     $('#amount_received').val('');
  });

$('#edit_modal').on('shown', function () {
    $("#paymentHistory").html('');
    $("#total_bill_amount").removeAttr('disabled');
});


  function populateCategory(accountsType) 
  {
      $('#div_school_id').hide();
      $('#div_batch_id').hide();
      $('#div_student_enrolment_id').hide();
      $("#category").empty();
      if(accountsType == 'Income') 
      {
            $("#category").append("<option value=''></option>");
            for(loopCategory = 0; loopCategory < income_category.length; loopCategory++ ) {
              $("#category").append("<option value='" + income_category[loopCategory] + "'>" + income_category[loopCategory] + "</option>");
            }
      }

      if(accountsType == 'Expense') 
      {       
              $("#category").append("<option value=''></option>");
              for(loopCategory = 0; loopCategory < expense_category.length; loopCategory++ ) {
              $("#category").append("<option value='" + expense_category[loopCategory] + "'>" + expense_category[loopCategory] + "</option>");
              }
      }

      $('#category').val('default').selectpicker("refresh");
      $('#school_id').val('default').selectpicker("refresh");
      $('#batch_id').val('default').selectpicker("refresh");
      $('#student_enrolment_id').val('default').selectpicker("refresh");
  }

   var eventElementId = "type";
   $('input:radio[id='+eventElementId+']').change(function () {
        if ($("input[id="+eventElementId+"]:checked").val() == 'Income') 
        {
          populateCategory('Income');
          $("#div_amount_received").show();
          $("#div_amount_paid").hide();
          $("#amount_paid").val('');
        }        

        if ($("input[id="+eventElementId+"]:checked").val() == 'Expense') 
        {
          populateCategory('Expense');
          $("#div_amount_paid").show();
          $("#div_amount_received").hide();
          $("#amount_received").val('');  
        }
    });


     // date popup
     $('#payment_date, #invoice_date').parent().addClass('date')
     $('#payment_date, #invoice_date').datetimepicker({
      format: 'YYYY-MM-DD'
     });

     $('#total_bill_amount').change(function() {
       $('#payableamount').val($('#total_bill_amount').val());
    });

     $('#amount_received, #amount_paid').change(function() {
      balanceAmountCalculations();  
   });

     $('#frm_edit').click(function() {
      balanceAmountCalculations();  
   });

     function balanceAmountCalculations() {      
      // console.log($('#payableamount').val());
      var payableamount = parseFloat($('#payableamount').val() == "" ? 0 : $('#payableamount').val());
      var amount_received = parseFloat($('#amount_received').val() == "" ? 0 : $('#amount_received').val());
      var amount_paid = parseFloat($('#amount_paid').val() == "" ? 0 : $('#amount_paid').val());


      if ($("#type:checked").val() == 'Income')  {
        if(amount_received > payableamount) {
        $('#div_amount_received .text-danger').html('Amount received should not be more than payable amount');
        }
        else {
        $('#div_amount_received .text-danger').html('');
        $("#balance").val(payableamount-amount_received); 
        }
      }

      if ($("#type:checked").val() == 'Expense') {
        if(amount_paid > payableamount) {
        $('#div_amount_paid .text-danger').html('Amount paid should not be more than payable amount');
        }
        else {
        $('#div_amount_paid .text-danger').html('');
        $("#balance").val(parseFloat(payableamount) - parseFloat(amount_paid)); 
        }
      }  

      if($("#balance").val() == 0)  {
        $("input[id='status'][value='Full payment']").prop('checked', true); 
      } 

      if($("#balance").val() > 0)  {
        $("input[id='status'][value='Partial payment']").prop('checked', true); 
      }
      
   }



   function formInteractions(elementId)
   {      
   }

   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
      sqlUpdateQueryArray.length=0;
      sqlInsertQueryArray.length=0;
      db_table_update_values.length=0;
      db_table_insert_values.length=0;

      if(db_table_update_columns.length!=0) {
        var tempArray = [], 
            formValue = '', 
            formElementId = '', 
            formElementValue = '', 
            loopColumnsArrayLength =0;


        /************* get edit form values and push to array ***************/

        loopColumnsArrayLength = db_table_update_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
            tempArray.push(formElementValue); 
        }
        db_table_update_values.push(tempArray);



         /************* get add form values and push to array ***************/

        loopColumnsArrayLength = db_table_insert_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_insert_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              //console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
          tempArray.push(formElementValue);                    
        }
        db_table_insert_values.push(tempArray);
      }

      create_db_table_sql_update_query($('#' + formId + ' #id').val()); // For update query - db table row unique id as parameter
      create_db_table_sql_insert_query(); // For insert query 

      var amount_received = parseFloat($('#amount_received').val() == "" ? 0 : $('#amount_received').val());
      
      var amount_paid = parseFloat($('#amount_paid').val() == "" ? 0 : $('#amount_paid').val());
      
      var payment_date = $('#payment_date').val();
      var payment_mode = $('input[id=payment_mode]:checked').val();
      var payment_comments = $('#payment_comments').val();

      var paymentQuery = "INSERT INTO incomeexpensepayment (incomeexpense_id, amount_received, amount_paid, payment_date, payment_mode, payment_comments) VALUES ( " + incomeexpense_id + ", " + amount_received + ", " + amount_paid + ", '" + payment_date + "', '" + payment_mode  + "', '" + payment_comments + "' )";

      amount_received = parseInt(previous_amount_received)+parseInt(amount_received);
      amount_paid = parseInt(previous_amount_paid)+parseInt(amount_paid);

      var paymentUpdatesQuery = "UPDATE incomeexpense SET amount_received = " + amount_received + ", amount_paid = " +  amount_paid + ", payment_date = '" + payment_date + "' WHERE id = " + incomeexpense_id;

      sqlUpdateQueryArray.push(paymentQuery);
      sqlUpdateQueryArray.push(paymentUpdatesQuery);
      ajaxAction($('#action').val());  // parameter "both" executes both insert and update sql
      return false; // Will stop the submission of the form
    }
  });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>