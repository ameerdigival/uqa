<?php
  ob_start();
  session_start();
   include_once("connection.php");
   include_once("helper.php"); 

  $db = new dbObj();
  $connString =  $db->getConnstring();
  

  // if session is not set this will redirect to login page
  if ( isset($_SESSION['user'])=="") {
      echo "<script type='text/javascript'>  window.location='index.php'; </script>";
      exit;
  }

  if ( isset($_SESSION['user'])!="" && isset($_SESSION['is_admin'])!="No") {
    header("Location: dashboard.php");
    exit;
  }

  if ( isset($_SESSION['user'])!="" && isset($_SESSION['is_admin'])!="Yes" && isset($_SESSION['active'])!="Yes") {
    header("Location: dashboard.php");
    exit;
  }
?>

<?php
  // select loggedin users detail
  $res=mysqli_query($connString, "SELECT * FROM users WHERE id='" . $_SESSION['user'] . "'");
  $userRow=mysqli_fetch_array($res);

  $username = ""; $usernameError = "";

  // common variables
  $bootgridTableTHclass = 'data-css-class="hidden-xs hidden-sm" data-header-css-class="hidden-xs hidden-sm';
?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php include_once("includes.php"); ?>
