<?php

	session_start();
	
	if (!isset($_SESSION['user'])) {
		header("Location: index.php");
	} else if(isset($_SESSION['user'])!=""  && isset($_SESSION['is_admin'])!="No") {
		header("Location: users.php");
	}
	 else if(isset($_SESSION['user'])!=""  && isset($_SESSION['is_admin'])!="Yes") {
		header("Location: mytransactions.php");
	}
	
	if (isset($_GET['logout'])) {
		unset($_SESSION['user']);
		unset($_SESSION['is_admin']);
		unset($_SESSION['active']);
		session_unset();
		session_destroy();
		header("Location: index.php");
		exit;
	}