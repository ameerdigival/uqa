<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Batches Yearwise Report</h3>
  	</div>        
    <div class="col-sm-12">

      <div id="filterArea" class="form-row">
        <div class="row">
          <div class="col-md-1">
            <label for="name" class="control-label">Year</label>
            <select id="selectYear" class="form-control"></select>
          </div>
          
          <div class="pull-right" style='padding-bottom:10px; padding-top: 5px;'>
            <button type="button" class="btn btn-xs btn-primary" id="btn-pdf" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> PDF</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-xls" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> Excel</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-print" data-row-id="0">
            <span class="glyphicon glyphicon-print"></span> Print</button>
          </div>

          <div class="form-group col-md-4">
          </div>

          <div class="form-group col-md-4">
          </div>
        </div>            
      </div>
      
      <div id="printArea" class="row">

       
      </div>
     

    </div>
  </div>
</div>


<?php include_once("footer.php"); ?>
</div>

<script>
$(document).ready(function() 
{ 
// Year data populate
var max = new Date().getFullYear(),
    min = max - 20,
    select = document.getElementById('selectYear');

for (var i = max; i>=min; i--){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


var db_table_name = [] , db_table_columns = [], table_header = [];

db_table_name = ["batchdetails"]; // db table names 
db_table_where_colummn = []; // db table names
db_table_columns = ["id", "batch_name", "no_of_students", "start_date", "end_date", "comments", "income", "expense"]; // db table column
table_header = ["Sl.No.","Batch Name", "No. of Students", "Start Date", "End Date", "Comments", "Income", "Expense"]; // db table column

loadAjaxCall();

$("#selectYear").change(function () {
  $("#printArea").html("");
  loadAjaxCall();
});


function loadAjaxCall() {
  var year = $("#selectYear option:selected").val();
   if(year != '') {
      var sqlQuery = "SELECT batchdetails.name as batch_name, count(studentenrolment.batch_id) as no_of_students, batchdetails.start_date, batchdetails.end_date, batchdetails.comments, (studentenrolment.course_fees + studentenrolment.books_fees - studentenrolment.discount) as income, (select IFNULL(sum(amount_paid),0) from incomeexpense where batch_id = studentenrolment.batch_id) as expense FROM studentenrolment left join batchdetails on batchdetails.batch_id = studentenrolment.batch_id where studentenrolment.category = 'Classroom' and year(batchdetails.start_date) = '"+year+"' GROUP by studentenrolment.batch_id ORDER by studentenrolment.batch_id ASC ";
      
      
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                  populateTable(response, year);

              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });

      
   }
}

function populateTable(response, year) 
{ 
var htmlTableData = '', netIncome = 0, netExpense = 0;
          
        htmlTableData += '<table class="reportHeader"><tbody><tr><td><b>Batches - ' + year + '</b></td></tr></tbody></table>' + 
                          '<div class="table-responsive"><table id="tableData" class="table table-bordered table-responsive">' + 
                          '<thead>' +
                          '<tr>';
        var tableHeaderLength = table_header.length;
        for(loopColumns=0;loopColumns<tableHeaderLength;loopColumns++) 
        {
          
              htmlTableData += '<th>' + table_header[loopColumns] + '</th>'              
           
        }
        htmlTableData += '</tr>' +
                          '</thead>';

        htmlTableData += '<tbody>' 
                          
        var responseLength = response.length;
        for(loopColumns=0;loopColumns<responseLength;loopColumns++) 
        {          
              htmlTableData += '<tr>'
                  var db_table_columns_Length = db_table_columns.length;
                  for(loopHeaders=0; loopHeaders<db_table_columns_Length; loopHeaders++)  {
                    if(loopHeaders==0) {
                      htmlTableData += '<td>' + parseInt(loopColumns+1) + '</td>';
                    }
                    else {
                      var rowContent = response[loopColumns][db_table_columns[loopHeaders]];
                      if(rowContent != null)
                      {
                        htmlTableData += '<td>' + response[loopColumns][db_table_columns[loopHeaders]] + '</td>';
                      }
                      else {
                        htmlTableData += '<td>&#160;</td>';
                      }
                    }
                    if(db_table_columns[loopHeaders] == "income") {
                      netIncome += parseInt(response[loopColumns][db_table_columns[loopHeaders]]);
                    }
                    if(db_table_columns[loopHeaders] == "expense") {
                      netExpense += parseInt(response[loopColumns][db_table_columns[loopHeaders]]);
                    }
                    
                  }

               htmlTableData += '</tr>'
        }
        htmlTableData += '</tbody></table></div>' +
                      '<div><b>Net Income: </b>' + netIncome + '</div>' +
                      '<div><b>Net Expense: </b>' + netExpense + '</div><br><br>';
        console.log(htmlTableData)
        $("#printArea").html(htmlTableData);
}

});

 $( "#btn-xls" ).click(function() {
      $('#printArea').tableExport({type:'excel'}); 
 }); 

 $( "#btn-print" ).click(function() {
      window.print(); 
 });

 $( "#btn-pdf" ).click(function() {
      pdfExport("printArea");
 });

</script>
</body>
</html>

<?php ob_end_flush(); ?>