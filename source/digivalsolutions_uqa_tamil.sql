-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 14, 2018 at 08:54 AM
-- Server version: 10.2.15-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digivalsolutions_uqa_tamil`
--

-- --------------------------------------------------------

--
-- Table structure for table `batchdetails` 
--

CREATE TABLE `batchdetails` (
  `id` int(11) NOT NULL,
  `batch_id` varchar(100) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `course_id` varchar(60) DEFAULT NULL,
  `fees` decimal(60,0) DEFAULT NULL,
  `location_id` varchar(600) DEFAULT NULL,
  `days` varchar(60) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` varchar(60) DEFAULT NULL,
  `end_time` varchar(60) DEFAULT NULL,
  `trainer_employee_id` varchar(60) DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  `comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batchdetails`
--

INSERT INTO `batchdetails` (`id`, `batch_id`, `name`, `course_id`, `fees`, `location_id`, `days`, `start_date`, `end_date`, `start_time`, `end_time`, `trainer_employee_id`, `status`, `comments`, `modified`, `created`) VALUES
(1, 'SEPBATCH', 'JULY BATCH', '66', '15000', '61', 'Tue,Thu,Fri,Sun', '2018-07-25', '2018-07-31', '2:00am', '3:00am', '', 'Not started', 'TEST BATCH INFO', '2018-08-02 17:30:33', '2018-07-27 15:49:19'),
(2, 'Batch2', 'Batch 2', '67', '45000', '62', 'Mon,Wed,Fri', '2018-07-02', '2018-07-27', '12:00am', '1:30am', '3', 'Not started', '', '2018-08-02 17:30:57', '2018-07-28 10:53:19'),
(3, 'RQT-1-18', 'RQT-1-18', '68', '2000', '64', 'Sun', '2018-02-18', '2018-04-22', '7:00am', '10:00am', '4', 'Not started', '', '2018-08-04 05:06:27', '2018-08-04 04:59:32');

-- --------------------------------------------------------

--
-- Stand-in structure for view `batchdetails_view`
-- (See below for the actual view)
--
CREATE TABLE `batchdetails_view` (
`id` int(11)
,`batch_id` varchar(100)
,`name` varchar(200)
,`course_id` varchar(60)
,`fees` decimal(60,0)
,`location_id` varchar(600)
,`days` varchar(60)
,`start_date` date
,`end_date` date
,`start_time` varchar(60)
,`end_time` varchar(60)
,`trainer_employee_id` varchar(60)
,`status` varchar(60)
,`comments` varchar(600)
,`modified` timestamp
,`created` timestamp
,`locationname` varchar(200)
,`coursename` varchar(200)
,`employeename` varchar(60)
);

-- --------------------------------------------------------

--
-- Table structure for table `batchportions`
--

CREATE TABLE `batchportions` (
  `id` int(10) NOT NULL,
  `batch_id` varchar(200) NOT NULL,
  `batch_date` date DEFAULT NULL,
  `portions_targeted` varchar(600) DEFAULT NULL,
  `portions_completed` varchar(600) DEFAULT NULL,
  `comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batchportions`
--

INSERT INTO `batchportions` (`id`, `batch_id`, `batch_date`, `portions_targeted`, `portions_completed`, `comments`, `modified`, `created`) VALUES
(1, 'SEPBATCH', '2018-07-26', '5', '13', 'test', '2018-08-02 01:43:54', '2018-07-27 15:49:19'),
(2, 'SEPBATCH', '2018-07-29', '5', '4', 'test', '2018-08-02 01:43:54', '2018-07-27 15:49:19'),
(3, 'SEPBATCH', '2018-07-31', '5', '6', 'test', '2018-08-02 01:43:54', '2018-07-27 15:49:19'),
(4, 'Batch2', '2018-07-02', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(5, 'Batch2', '2018-07-04', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(6, 'Batch2', '2018-07-06', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(7, 'Batch2', '2018-07-09', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(8, 'Batch2', '2018-07-11', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(9, 'Batch2', '2018-07-13', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(10, 'Batch2', '2018-07-16', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(11, 'Batch2', '2018-07-18', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(12, 'Batch2', '2018-07-20', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(13, 'Batch2', '2018-07-23', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(14, 'Batch2', '2018-07-25', '', '', '', '0000-00-00 00:00:00', '2018-07-28 10:53:19'),
(15, 'Batch2', '2018-07-27', '5', '13', 'test', '2018-08-02 01:43:36', '2018-07-28 10:53:19'),
(16, 'RQT-1-18', '2018-02-18', '', '', '', '0000-00-00 00:00:00', '2018-08-04 04:59:32'),
(17, 'RQT-1-18', '2018-02-25', '', '', '', '0000-00-00 00:00:00', '2018-08-04 04:59:32'),
(18, 'RQT-1-18', '2018-03-04', '', '', '', '0000-00-00 00:00:00', '2018-08-04 04:59:32'),
(19, 'RQT-1-18', '2018-03-11', '', '', '', '0000-00-00 00:00:00', '2018-08-04 04:59:32'),
(20, 'RQT-1-18', '2018-03-18', '', '', '', '0000-00-00 00:00:00', '2018-08-04 04:59:32'),
(21, 'RQT-1-18', '2018-03-25', '', '', '', '0000-00-00 00:00:00', '2018-08-04 04:59:32'),
(22, 'RQT-1-18', '2018-04-01', '', '', '', '0000-00-00 00:00:00', '2018-08-04 04:59:32'),
(23, 'RQT-1-18', '2018-04-08', '', '', '', '0000-00-00 00:00:00', '2018-08-04 04:59:32'),
(24, 'RQT-1-18', '2018-04-15', '', '', '', '0000-00-00 00:00:00', '2018-08-04 04:59:32'),
(25, 'RQT-1-18', '2018-04-22', '', '', '', '0000-00-00 00:00:00', '2018-08-04 04:59:32');

-- --------------------------------------------------------

--
-- Stand-in structure for view `batchportions_view`
-- (See below for the actual view)
--
CREATE TABLE `batchportions_view` (
`id` int(10)
,`batch_id` varchar(200)
,`batch_date` date
,`portions_targeted` varchar(600)
,`portions_completed` varchar(600)
,`comments` varchar(600)
,`modified` timestamp
,`created` timestamp
,`batchname` varchar(200)
);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `shortname` varchar(60) DEFAULT NULL,
  `coursefees` decimal(60,0) DEFAULT NULL,
  `summary` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `fullname`, `shortname`, `coursefees`, `summary`, `modified`, `created`) VALUES
(63, 'undestanding Quran - II', 'UQ2', '10000', '1.Copyright center align\n2.SCROLL BAR REQUITED FOR POPUP BOX\n3.SUBMIT BUTTON COLOR IS CHANGIN TO BLUE\n4.STUDENT ENROLEMENT COURSE FEE SHOULD BE POPULTED AND VALIDATION NEED TO CHECK ACTUAL VS PAID\n5.STUDENT NAME,COURSE NAME,BATCHNAME  SHOULD BE DISPLAY IN STUDENT ENROLMENT SCREEN.\n6.BATCH DETAILS SCREEN COURSE NAME SHOULD BE DISPLAYED.\n7.ADD STUDENT ENROLMENT SCREEN COURSE PAID SHOULD NOT BE ACTUAL COURSE FEE(VALIDATION REQUIRED) & TOTAL PAID AMOUNT SHOULD NOT EXCEED ACTUAL AMOUNT\n8.BATCH PORTION SCREEN -BATCH PORTION IS NOT REFLECRED IF I UPDATE BATCH DETAILS\n9.Batch details screen trainer,co', '0000-00-00 00:00:00', '2018-08-02 01:44:49'),
(64, 'READING QURAN ', 'RQN', '5000', 'test the summay update', '2018-08-02 01:47:03', '2018-08-02 01:45:53'),
(65, 'READING QURAN ', 'RQN', '9000', 'sjldj\n*&((((((((((((((((((((((((((\n46546456464\nkljlsjlkadjsaldkd\n\njslajdlkas', '2018-08-02 01:46:38', '2018-08-02 01:45:53'),
(66, 'Undestanding Quran New', 'UQN', '15000', 'sadasdas\n\n\nasdas\n\n\n\nasd\na\n\nsdad\n\nd\nas\nd\naS\ndass', '0000-00-00 00:00:00', '2018-08-02 01:48:56'),
(67, 'Integrated quran', 'IQ', '45000', 'asdasd\n\n\nsa\nds\n\ngf\nfg\n\nhg\ng\nhg\ng\n\nhfg\n\nhfgggggggggg\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nfgdddddddd', '0000-00-00 00:00:00', '2018-08-02 01:50:17'),
(68, 'Read Quran with Tajweed in Tamil', 'RQT', '2000', '', '0000-00-00 00:00:00', '2018-08-04 04:52:38'),
(69, 'Read Quran with Tajweed in English', 'RQE', '2000', '', '0000-00-00 00:00:00', '2018-08-04 04:53:05'),
(70, 'Understand Quran and Salah in Tamil (Course-1)', 'UQT-1', '2000', '', '2018-08-04 04:55:23', '2018-08-04 04:54:30'),
(71, 'Understand Quran and Salah in English(Course-1)', 'UQE-1', '2000', '', '0000-00-00 00:00:00', '2018-08-04 04:55:08');

-- --------------------------------------------------------

--
-- Table structure for table `employeeattendance`
--

CREATE TABLE `employeeattendance` (
  `id` int(10) NOT NULL,
  `employee_id` varchar(60) NOT NULL,
  `attendance_date` date DEFAULT NULL,
  `attended` varchar(100) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employeeattendance`
--

INSERT INTO `employeeattendance` (`id`, `employee_id`, `attendance_date`, `attended`, `modified`, `created`) VALUES
(1, '1', '2018-07-03', 'Present', '2018-08-01 18:31:19', '2018-07-27 16:30:19'),
(2, '2', '2018-07-03', 'Halfday Co', '2018-08-01 18:31:09', '2018-07-27 16:30:19'),
(3, '1', '2018-07-04', 'Halfday Leave', '2018-08-02 10:20:04', '2018-07-27 16:30:54'),
(4, '2', '2018-07-04', 'Present', '2018-08-02 02:08:56', '2018-07-27 16:30:54'),
(5, '1', '2018-07-18', 'Compensatory Leave', '2018-08-02 10:20:16', '2018-07-28 04:51:36'),
(6, '2', '2018-07-18', 'Absent', '2018-08-02 02:09:20', '2018-07-28 04:51:36'),
(7, '1', '2018-08-24', 'Present', '0000-00-00 00:00:00', '2018-08-01 18:30:06'),
(8, '2', '2018-08-24', 'Present', '0000-00-00 00:00:00', '2018-08-01 18:30:06'),
(9, '1', '2018-08-01', 'Halfday Compensatory', '2018-08-02 10:19:56', '2018-08-02 02:08:30'),
(10, '3', '2018-08-01', 'Compensatory Leave', '2018-08-02 10:20:11', '2018-08-02 02:08:30'),
(11, '1', '2018-08-02', 'Present', '0000-00-00 00:00:00', '2018-08-02 02:09:57'),
(12, '3', '2018-08-02', 'Present', '0000-00-00 00:00:00', '2018-08-02 02:09:57');

-- --------------------------------------------------------

--
-- Stand-in structure for view `employeeattendance_view`
-- (See below for the actual view)
--
CREATE TABLE `employeeattendance_view` (
`id` int(10)
,`employee_id` varchar(60)
,`attendance_date` date
,`attended` varchar(100)
,`modified` timestamp
,`created` timestamp
,`name` varchar(60)
);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) NOT NULL,
  `name` varchar(60) NOT NULL,
  `dateofbirth` date DEFAULT NULL,
  `gender` varchar(60) DEFAULT NULL,
  `married` varchar(60) DEFAULT NULL,
  `qualification` varchar(60) DEFAULT NULL,
  `experience` varchar(600) DEFAULT NULL,
  `job_category` varchar(60) DEFAULT NULL,
  `job_type` varchar(60) DEFAULT NULL,
  `job_status` varchar(60) DEFAULT NULL,
  `joined_date` date DEFAULT NULL,
  `resigned_date` date DEFAULT NULL,
  `salary` decimal(60,0) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `area` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `pincode` varchar(60) DEFAULT 'off',
  `email` varchar(60) DEFAULT 'No',
  `comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `dateofbirth`, `gender`, `married`, `qualification`, `experience`, `job_category`, `job_type`, `job_status`, `joined_date`, `resigned_date`, `salary`, `phone`, `address`, `area`, `city`, `country`, `pincode`, `email`, `comments`, `modified`, `created`) VALUES
(1, 'Shahul', '1973-02-02', 'Male', 'Yes', 'B.Com.', '', 'Staff', 'Permanent', 'Employed', '2017-01-18', '0000-00-00', '20000', '9176129152', '', 'Triplicane', 'Chennai', 'India', '600005', 'chakoshah@gmail.com', '', '2018-08-04 04:26:10', '2018-07-27 16:22:35'),
(2, 'sheik', '1992-01-29', 'Male', 'Yes', 'B.E', '', 'Staff', 'Part-Time', 'Resigned', '2018-07-26', '0000-00-00', '15000', '9898923912', '', 'triplicane', 'CHENNAI', '', '', '', '', '2018-08-02 02:04:11', '2018-07-27 16:24:48'),
(3, 'HAJIRA', '1992-01-29', 'Female', 'No', 'B.A ARABIC', '1.Copyright center align\n2.SCROLL BAR REQUITED FOR POPUP BOX\n3.SUBMIT BUTTON COLOR IS CHANGIN TO BLUE\n4.STUDENT ENROLEMENT COURSE FEE SHOULD BE POPULTED AND VALIDATION NEED TO CHECK ACTUAL VS PAID\n5.STUDENT NAME,COURSE NAME,BATCHNAME  SHOULD BE DISPLAY IN STUDENT ENROLMENT SCREEN.\n6.BATCH DETAILS SCREEN COURSE NAME SHOULD BE DISPLAYED.\n7.ADD STUDENT ENROLMENT SCREEN COURSE PAID SHOULD NOT BE ACTUAL COURSE FEE(VALIDATION REQUIRED) & TOTAL PAID AMOUNT SHOULD NOT EXCEED ACTUAL AMOUNT\n8.BATCH PORTION SCREEN -BATCH PORTION IS NOT REFLECRED IF I UPDATE BATCH DETAILS\n9.Batch details screen trainer,co', 'Trainer', 'Part-Time', 'Employed', '2018-08-01', '0000-00-00', '9000', '+919941969786', '', '', 'Hyderabad', '', '', '', '', '2018-08-02 02:07:57', '2018-08-02 02:06:19'),
(4, 'Asrarul Haq', '1993-01-31', 'Male', 'No', 'B.Com.', '', 'Trainer', 'Permanent', 'Employed', '2017-09-25', '0000-00-00', '12000', ' 9087540133', '385 / 11, 1st Cross, Shah Nagar, Pallapatti, Karur Dist.', 'Pallapatti', 'Karur', 'India', '639205', 'haseenaamiri@gmail.com', '', '0000-00-00 00:00:00', '2018-08-04 04:18:29'),
(5, 'Katheeja', '0000-00-00', 'Female', 'Yes', '', '', 'Staff', 'Permanent', 'Employed', '2017-01-27', '0000-00-00', '10000', '9884012489', '', '', 'Chennai', '', '', '', '', '2018-08-06 06:56:59', '2018-08-04 04:20:49');

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE `enquiry` (
  `id` int(10) NOT NULL,
  `name` varchar(60) NOT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `whatsapp` varchar(60) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `area` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `pincode` varchar(60) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `enquiry_category` varchar(60) DEFAULT NULL,
  `enquiry_date` date NOT NULL,
  `book_language` varchar(60) DEFAULT NULL,
  `book_names` varchar(200) DEFAULT NULL,
  `job_category` varchar(60) DEFAULT NULL,
  `comments` varchar(600) DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  `followup_comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiry`
--

INSERT INTO `enquiry` (`id`, `name`, `phone`, `whatsapp`, `address`, `area`, `city`, `country`, `pincode`, `email`, `enquiry_category`, `enquiry_date`, `book_language`, `book_names`, `job_category`, `comments`, `status`, `followup_comments`, `modified`, `created`) VALUES
(1, 'SULAIMAN', '7304023984230', '2230840923840', '', '', '', '', '', '', 'Course', '2018-07-02', '', '', '', 'TRTEST ADD ENQUIRY', 'Closed', 'Not reachable', '2018-08-02 15:33:42', '2018-07-27 15:39:50'),
(3, 'abbas', '9893442323', '9898989823', '16 post office road', 'guindy', 'chennai', 'india', '60032', 'ameer@vz.com', 'Job', '2018-07-05', '', '', 'Teaching', 'RQ', 'Open', '', '2018-08-02 15:33:20', '2018-08-01 09:42:12'),
(4, 'JAFFAR', '+919941969786', '098102398012', '10,High Road', '', 'Hyderabad', 'india', '', 'abuthahir11@gmail.com', 'Job', '2018-08-02', '', '', 'Staff', 'I have 3 yrs experience', 'Open', '', '2018-08-02 15:33:01', '2018-08-02 01:58:17'),
(5, 'Ayisha', '9025777501 ', '', '', '', '', '', '', '', 'Books', '2018-08-01', 'Tamil', '300', '', 'Ayisha called and asking for SC2 Tamil. But told her that is not available and recommended for eLC. To call her son Faiyaz (9626303576) and to provide eLC.', 'Open', '', '0000-00-00 00:00:00', '2018-08-04 04:37:49'),
(6, 'Abdullah', '8148785864', '', '', '', '', '', '', '', 'Books', '2018-07-31', 'Tamil', '300', '', 'Would like to have books RQT & SC1 Tamil.', 'Closed', 'Sent books on 2-Aug-2018.', '2018-08-04 04:42:51', '2018-08-04 04:42:26');

-- --------------------------------------------------------

--
-- Table structure for table `incomeexpense`
--

CREATE TABLE `incomeexpense` (
  `id` int(10) NOT NULL,
  `type` varchar(200) DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `school_id` varchar(200) DEFAULT NULL,
  `batch_id` varchar(200) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `total_bill_amount` decimal(60,0) NOT NULL,
  `amount_received` decimal(60,0) DEFAULT NULL,
  `amount_paid` decimal(60,0) DEFAULT NULL,
  `balance` decimal(60,0) DEFAULT NULL,
  `invoice_number` varchar(200) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `multi_payment_date` date NOT NULL,
  `multi_amount_received` varchar(60) NOT NULL,
  `multi_amount_paid` varchar(60) NOT NULL,
  `status` varchar(60) NOT NULL,
  `comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incomeexpense`
--

INSERT INTO `incomeexpense` (`id`, `type`, `category`, `school_id`, `batch_id`, `description`, `total_bill_amount`, `amount_received`, `amount_paid`, `balance`, `invoice_number`, `invoice_date`, `payment_date`, `multi_payment_date`, `multi_amount_received`, `multi_amount_paid`, `status`, `comments`, `modified`, `created`) VALUES
(1, 'Income', 'School', '1', '', 'reading books', '10000', '5000', '0', '5000', '111', '2018-08-02', '2018-08-10', '0000-00-00', '', '', 'Partial payment', '', '2018-08-06 06:18:16', '2018-08-03 04:43:33'),
(2, 'Expense', 'Purchase', '', '', 'office Files', '5000', '0', '5000', '0', '222', '2018-08-01', '2018-08-09', '0000-00-00', '', '', 'Full payment', '', '2018-08-06 06:18:33', '2018-08-03 04:45:13'),
(3, 'Expense', 'Purchase', '', '', 'office Files', '1000', '0', '500', '500', '222', '2018-08-09', '2018-08-02', '0000-00-00', '', '', 'Partial payment', '', '2018-08-06 06:18:47', '2018-08-03 04:55:56'),
(4, 'Income', 'School', '3', '', 'reading books', '4000', '1200', '0', '2800', '222', '2018-08-03', '2018-08-03', '0000-00-00', '', '', 'Partial payment', '', '2018-08-06 06:19:04', '2018-08-03 05:19:43'),
(5, 'Expense', 'General', '', '', 'Tutor fee', '10000', '0', '5000', '5000', '122', '2018-08-02', '2018-08-10', '0000-00-00', '', '', 'Partial payment', '', '2018-08-06 06:16:24', '2018-08-03 05:20:28'),
(6, 'Income', 'School', '2', '', 'Sale of book-4', '1000', '1000', '0', '0', '001', '2018-08-04', '2018-08-04', '0000-00-00', '', '', 'Full payment', '', '0000-00-00 00:00:00', '2018-08-04 05:22:31');

-- --------------------------------------------------------

--
-- Stand-in structure for view `incomeexpense_view`
-- (See below for the actual view)
--
CREATE TABLE `incomeexpense_view` (
`id` int(10)
,`type` varchar(200)
,`category` varchar(200)
,`school_id` varchar(200)
,`batch_id` varchar(200)
,`description` varchar(500)
,`total_bill_amount` decimal(60,0)
,`amount_received` decimal(60,0)
,`amount_paid` decimal(60,0)
,`balance` decimal(60,0)
,`invoice_number` varchar(200)
,`invoice_date` date
,`payment_date` date
,`multi_payment_date` date
,`multi_amount_received` varchar(60)
,`multi_amount_paid` varchar(60)
,`status` varchar(60)
,`comments` varchar(600)
,`modified` timestamp
,`created` timestamp
,`batchname` varchar(200)
,`schoolname` varchar(60)
);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(10) NOT NULL,
  `category` varchar(60) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `quantity` int(60) DEFAULT NULL,
  `unit_price` decimal(60,0) DEFAULT NULL,
  `discount` decimal(60,0) DEFAULT NULL,
  `extra_charges` decimal(60,0) DEFAULT NULL,
  `total_amount` decimal(60,0) NOT NULL,
  `comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `category`, `name`, `quantity`, `unit_price`, `discount`, `extra_charges`, `total_amount`, `comments`, `modified`, `created`) VALUES
(1, 'Office', 'Marker', 20, '12', '0', '0', '0', 'tarif', '2018-08-03 04:41:55', '2018-08-01 11:29:26'),
(2, 'Books', 'Tajweed', 50, '100', '1000', '4000', '0', 'this is for RQ course', '0000-00-00 00:00:00', '2018-08-03 04:40:55'),
(3, 'Office', 'board', 5, '500', '100', '4000', '0', '', '0000-00-00 00:00:00', '2018-08-03 04:42:31');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `area` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `pincode` varchar(60) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `name`, `address`, `area`, `city`, `country`, `pincode`, `modified`, `created`) VALUES
(61, 'IMAX', NULL, 'Avadi', 'Chennai', 'India', '560001', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 'GMAX', '', 'Mylapore', 'Chennai', 'India', '560001', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 'AIM SCHOOL', '42,RAILWAY STATIONROAD', 'ALANDUR', 'CHENNAI', 'INDIA', '60039', '0000-00-00 00:00:00', '2018-07-31 14:20:30'),
(64, 'Understand Al-Quran Academy - Chennai', '108, Quaid-E-Millath (Triplicane) High Road, Triplicane', 'Triplicane', 'Chennai', 'India', '600005', '0000-00-00 00:00:00', '2018-08-04 05:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `meetingaction`
--

CREATE TABLE `meetingaction` (
  `id` int(10) NOT NULL,
  `meeting_id` varchar(200) DEFAULT NULL,
  `task_name` varchar(200) DEFAULT NULL,
  `task_start_date` date DEFAULT NULL,
  `task_end_date` date DEFAULT NULL,
  `task_assigned_to` varchar(200) DEFAULT NULL,
  `action_tobe_taken` varchar(600) DEFAULT NULL,
  `action_taken` varchar(600) DEFAULT NULL,
  `task_status` varchar(100) DEFAULT NULL,
  `comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meetingaction`
--

INSERT INTO `meetingaction` (`id`, `meeting_id`, `task_name`, `task_start_date`, `task_end_date`, `task_assigned_to`, `action_tobe_taken`, `action_taken`, `task_status`, `comments`, `modified`, `created`) VALUES
(1, '1', 'visiting AIM school', '2018-08-01', '2018-08-06', '2', 'Meeting principal', '', 'Inprogress', 'Kindly check  availability', '2018-08-01 12:53:17', '2018-07-31 14:16:41'),
(2, '2', 'visiting imac school', '2018-08-03', '2018-08-05', '3', 'vistign imac', 'sadsada', 'Not started', 'asddasd', '0000-00-00 00:00:00', '2018-08-02 02:13:03');

-- --------------------------------------------------------

--
-- Stand-in structure for view `meetingaction_view`
-- (See below for the actual view)
--
CREATE TABLE `meetingaction_view` (
`id` int(10)
,`meeting_id` varchar(200)
,`task_name` varchar(200)
,`task_start_date` date
,`task_end_date` date
,`task_assigned_to` varchar(200)
,`action_tobe_taken` varchar(600)
,`action_taken` varchar(600)
,`task_status` varchar(100)
,`comments` varchar(600)
,`modified` timestamp
,`created` timestamp
,`meetingname` varchar(200)
,`employeename` varchar(60)
);

-- --------------------------------------------------------

--
-- Table structure for table `meetingdetails`
--

CREATE TABLE `meetingdetails` (
  `id` int(10) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `time` varchar(60) DEFAULT NULL,
  `attendees` varchar(600) DEFAULT NULL,
  `agenda` varchar(600) DEFAULT NULL,
  `comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meetingdetails`
--

INSERT INTO `meetingdetails` (`id`, `name`, `date`, `location`, `time`, `attendees`, `agenda`, `comments`, `modified`, `created`) VALUES
(1, 'AIM SCHOOL', '2018-07-30', 'chennai', '9', 'testt', 'TEST', 'Test command', '0000-00-00 00:00:00', '2018-07-31 14:12:08'),
(2, 'JULY BATCH', '2018-08-01', 'UQCENTER', '12 noon', 'All', 'teachers', 'techers', '2018-08-02 02:11:47', '2018-08-02 02:11:18');

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `id` int(10) NOT NULL,
  `name` varchar(60) NOT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `whatsapp` varchar(60) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `area` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `pincode` varchar(60) DEFAULT NULL,
  `contact_name` varchar(60) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`id`, `name`, `phone`, `whatsapp`, `address`, `area`, `city`, `country`, `pincode`, `contact_name`, `email`, `comments`, `modified`, `created`) VALUES
(1, 'AIM SCHOOL', '9898923912', '098102398012', '42,RAILWAY STATIONROAD', 'ALANDUR', 'CHENNAI', 'INDIA', '60039', 'MANSOOR', 'admin@example.com', 'TEST', '0000-00-00 00:00:00', '2018-07-27 16:08:20'),
(2, 'IMAX', '+445555522', '098102398012', '10,High Road', 'Royapettah', 'CHENNAI', 'INDIA', '60039', 'NASAR', 'admin@example.com', 'test', '0000-00-00 00:00:00', '2018-08-02 01:55:31'),
(3, 'GOVT.MUSLIM SCHOOL', '+919941969786', '', '', '', '', '', '', 'saleem', '', '', '0000-00-00 00:00:00', '2018-08-02 01:56:09'),
(4, 'Sana Model School', '9841234353', '', '', 'Poonamallee', 'Chennai', 'India', '', 'Nazia', 'principal@sanamodelschool.com', 'Sana Model School introduced UQA Book-4 to their students from Class 4-10 starting Academic year 2018-2019.', '2018-08-04 04:48:13', '2018-08-04 04:47:47'),
(5, 'IMAX Nursery and Primary School', '9884435189', '', '', 'Washermenpet', 'Chennai', 'India', '', 'Najmunnissa', 'princimaxwspt@gmail.com', '', '0000-00-00 00:00:00', '2018-08-04 04:51:26');

-- --------------------------------------------------------

--
-- Table structure for table `studentattendance`
--

CREATE TABLE `studentattendance` (
  `id` int(10) NOT NULL,
  `student_id` varchar(60) NOT NULL,
  `batch_id` varchar(600) DEFAULT NULL,
  `batch_date_id` varchar(600) DEFAULT NULL,
  `attendance_date` date DEFAULT NULL,
  `attended` varchar(10) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studentattendance`
--

INSERT INTO `studentattendance` (`id`, `student_id`, `batch_id`, `batch_date_id`, `attendance_date`, `attended`, `modified`, `created`) VALUES
(1, '3', 'Batch2', '4', '2018-07-02', 'Yes', '0000-00-00 00:00:00', '2018-08-01 12:54:54'),
(2, '3', 'Batch2', '6', '2018-07-06', 'No', '0000-00-00 00:00:00', '2018-08-01 12:55:25'),
(3, '4', 'SEPBATCH', '3', '2018-07-31', 'Yes', '0000-00-00 00:00:00', '2018-08-02 01:37:39'),
(4, '4', 'SEPBATCH', '3', '2018-07-31', 'Yes', '0000-00-00 00:00:00', '2018-08-02 01:37:39'),
(5, '8', 'RQT-1-18', '16', '2018-02-18', 'Yes', '2018-08-08 17:45:25', '2018-08-04 05:17:29');

-- --------------------------------------------------------

--
-- Stand-in structure for view `studentattendance_batchportions_join_view`
-- (See below for the actual view)
--
CREATE TABLE `studentattendance_batchportions_join_view` (
`rowid` int(10)
,`id` varchar(60)
,`batch_id` varchar(200)
,`attendance_date` date
,`attended` varchar(10)
,`batch_date` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `studentattendance_students_join_view`
-- (See below for the actual view)
--
CREATE TABLE `studentattendance_students_join_view` (
`rowid` int(10)
,`id` varchar(60)
,`batch_id` varchar(600)
,`attendance_date` date
,`attended` varchar(10)
,`name` varchar(60)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `studentattendance_view`
-- (See below for the actual view)
--
CREATE TABLE `studentattendance_view` (
`id` int(10)
,`student_id` varchar(60)
,`batch_id` varchar(600)
,`batch_date_id` varchar(600)
,`attendance_date` date
,`attended` varchar(10)
,`modified` timestamp
,`created` timestamp
,`name` varchar(60)
,`batchname` varchar(200)
);

-- --------------------------------------------------------

--
-- Table structure for table `studentenrolment`
--

CREATE TABLE `studentenrolment` (
  `id` int(11) NOT NULL,
  `student_id` varchar(60) NOT NULL,
  `batch_id` varchar(60) DEFAULT NULL,
  `course_id` varchar(60) DEFAULT NULL,
  `category` varchar(60) DEFAULT NULL,
  `enrolment_date` date NOT NULL,
  `books_id` varchar(600) DEFAULT NULL,
  `course_fees` decimal(60,0) DEFAULT NULL,
  `books_fees` decimal(60,0) DEFAULT NULL,
  `course_fees_paid` decimal(60,0) DEFAULT NULL,
  `books_fees_paid` decimal(60,0) DEFAULT NULL,
  `discount` decimal(60,0) DEFAULT NULL,
  `dueamount` decimal(60,0) DEFAULT NULL,
  `status` varchar(60) DEFAULT NULL,
  `comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studentenrolment`
--

INSERT INTO `studentenrolment` (`id`, `student_id`, `batch_id`, `course_id`, `category`, `enrolment_date`, `books_id`, `course_fees`, `books_fees`, `course_fees_paid`, `books_fees_paid`, `discount`, `dueamount`, `status`, `comments`, `modified`, `created`) VALUES
(2, '2', 'Batch2', '67', 'Classroom', '0000-00-00', '', '45000', '300', '5000', '100', '100', '40100', 'Not started', '', '2018-08-03 17:20:00', '2018-07-27 16:04:51'),
(3, '4', 'SEPBATCH', '66', 'Classroom', '2018-06-06', '', '15000', '500', '2500', '400', '0', '12600', 'Not started', '', '2018-08-03 17:19:48', '2018-07-28 05:02:29'),
(4, '3', 'SEPBATCH', '66', 'Classroom', '2018-07-11', '', '15000', '400', '6000', '400', '0', '9000', 'Not started', '', '2018-08-03 17:19:30', '2018-07-28 05:04:04'),
(5, '4', '', '67', 'Online', '0000-00-00', '1', '45000', '1000', '45000', '500', '200', '300', 'Not started', '', '2018-08-02 05:28:02', '2018-07-28 10:41:37'),
(6, '1', 'Batch2', '67', 'Classroom', '2018-08-01', '', '45000', '500', '10000', '400', '0', '35100', 'Not started', 'SPecial rate', '2018-08-03 17:18:18', '2018-08-02 01:11:35'),
(9, '4', 'Batch2', '69', 'Classroom', '2018-08-06', '2', '2000', '200', '2000', '200', '0', '0', 'Inprogress', '', '2018-08-08 17:42:39', '2018-08-08 06:20:30'),
(7, '8', 'RQT-1-18', '68', 'Classroom', '2018-08-04', '2', '2000', '100', '1000', '100', '1000', '0', 'Not started', '', '2018-08-08 17:44:44', '2018-08-04 05:15:56'),
(8, '9', 'SEPBATCH', '63', 'Classroom', '2018-08-08', '2', '10000', '400', '8000', '700', '700', '1000', 'Not started', '', '2018-08-08 17:44:51', '2018-08-08 06:13:09');

-- --------------------------------------------------------

--
-- Stand-in structure for view `studentenrolment_students_join_view`
-- (See below for the actual view)
--
CREATE TABLE `studentenrolment_students_join_view` (
`id` varchar(60)
,`batch_id` varchar(60)
,`name` varchar(60)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `studentenrolment_view`
-- (See below for the actual view)
--
CREATE TABLE `studentenrolment_view` (
`id` int(11)
,`student_id` varchar(60)
,`batch_id` varchar(60)
,`course_id` varchar(60)
,`category` varchar(60)
,`enrolment_date` date
,`books_id` varchar(600)
,`course_fees` decimal(60,0)
,`books_fees` decimal(60,0)
,`course_fees_paid` decimal(60,0)
,`books_fees_paid` decimal(60,0)
,`discount` decimal(60,0)
,`dueamount` decimal(60,0)
,`status` varchar(60)
,`comments` varchar(600)
,`modified` timestamp
,`created` timestamp
,`name` varchar(60)
,`coursename` varchar(200)
,`bookname` varchar(200)
,`batchname` varchar(200)
);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) NOT NULL,
  `name` varchar(60) NOT NULL,
  `dateofbirth` date DEFAULT NULL,
  `gender` varchar(60) DEFAULT NULL,
  `married` varchar(60) DEFAULT NULL,
  `qualification` varchar(60) DEFAULT NULL,
  `elc_cardnumber` varchar(60) DEFAULT NULL,
  `enrolment_key` varchar(60) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `whatsapp` varchar(60) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `area` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `pincode` varchar(60) DEFAULT 'off',
  `email` varchar(60) DEFAULT 'No',
  `comments` varchar(600) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `dateofbirth`, `gender`, `married`, `qualification`, `elc_cardnumber`, `enrolment_key`, `phone`, `whatsapp`, `address`, `area`, `city`, `country`, `pincode`, `email`, `comments`, `modified`, `created`) VALUES
(1, 'sulaiman', '2018-07-27', 'Male', '', 'B.E', '21312312312', '', '0908090909809', '+919941969786', '42,RAILWAY STATIONROAD', 'ALANDUR', 'CHENNAI', '', '60039', 'SU@VZ.COM', 'testing', '2018-07-28 03:59:57', '2018-07-27 15:37:46'),
(2, 'sakthi', '2008-07-28', 'Female', 'Yes', 'B.E', '213123232213', '543543', '5678901234', '5678901234', '', 'ALANDUR', 'CHENNAI', 'INDIA', '60042', 'admin@example.com', '', '0000-00-00 00:00:00', '2018-07-28 04:57:28'),
(3, 'younus', '1992-01-29', 'Male', 'Yes', 'Bsc', '2131231231232423', '', '1111111111', '2222222222', '16 post office', 'triplicane', 'CHENNAI', 'INDIA', '600086', '', 'skskks\n\nss\n\ns\ns\n\ns\n', '2018-08-02 05:30:29', '2018-07-28 05:00:16'),
(4, 'sara', '1992-01-06', 'Female', 'No', 'mca', '45434534', 'trichy', '8980880900', '8900044422', 'asddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddassaaaaaa', '', 'trichy', '', '700100', 'abuthahir11@gmail.com', 'testing', '2018-08-08 06:26:00', '2018-07-28 05:01:14'),
(5, 'Abu', '2012-01-02', 'Male', 'Yes', 'B.E', '', '', '+919941969786', '+919941969786', '54#,SUbam apt,chennai-85', 'ALANDUR', 'CHENNAI', 'INDIA', '', 'abuthahir@digivalsolutions.com', '', '0000-00-00 00:00:00', '2018-08-02 01:21:25'),
(6, 'ishaaq', '1992-01-30', 'Male', 'No', 'MBBS', '', '', '9032840923', '9030948203984', '', 'guindy', 'CHENNAI', 'INDIA', '', 'SU@VZ.COM', '', '0000-00-00 00:00:00', '2018-08-02 01:26:25'),
(7, 'eeas.K', '2010-07-27', 'Male', 'No', '+2', '', '', 'Nil', 'Nil', '42,RAILWAY STATIONROAD,pondisadaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 'towlichowki', 'Hyderabad', 'india', '760022', '123@digi.com', 'popup should display name instead of id1.Copyright center align\n2.SCROLL BAR REQUITED FOR POPUP BOX\n3.SUBMIT BUTTON COLOR IS CHANGIN TO BLUE\n4.STUDENT ENROLEMENT COURSE FEE SHOULD BE POPULTED AND VALIDATION NEED TO CHECK ACTUAL VS PAID\n5.STUDENT NAME,COURSE NAME,BATCHNAME  SHOULD BE DISPLAY IN STUDENT ENROLMENT SCREEN.\n6.BATCH DETAILS SCREEN COURSE NAME SHOULD BE DISPLAYED.\n7.ADD STUDENT ENROLMENT SCREEN COURSE PAID SHOULD NOT BE ACTUAL COURSE FEE(VALIDATION REQUIRED) & TOTAL PAID AMOUNT SHOULD NOT EXCEED ACTUAL AMOUNT\n8.BATCH PORTION SCREEN -BATCH PORTION IS NOT REFLECRED IF I UPDATE BATCH DE', '0000-00-00 00:00:00', '2018-08-02 01:32:04'),
(8, 'G. Farhan', '2018-08-04', 'Male', 'Yes', '', '', '', '9176060334', '', '', '', 'Chennai', '', '', '', '', '0000-00-00 00:00:00', '2018-08-04 05:13:55'),
(9, 'Ishaaq', '2013-02-06', 'Male', 'Yes', '7th standard', '', '', '9999977777', '7777777777', '', 'alandur', 'chennai', 'india', '', 'isha@gmail.com', '', '0000-00-00 00:00:00', '2018-08-08 06:10:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullname` varchar(60) NOT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `username` varchar(60) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `active` varchar(3) NOT NULL DEFAULT 'No',
  `is_admin` varchar(3) NOT NULL DEFAULT 'No',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `phone`, `email`, `username`, `password`, `active`, `is_admin`, `modified`, `created`) VALUES
(61, 'ADMIN', '9840498345', 'admin@gmail.com', 'admin', 'admin', 'Yes', 'Yes', '2018-07-27 15:10:20', '2018-07-27 14:55:11'),
(62, 'abu', '9941969786', 'abuthahir11@gmail.com', 'abu', 'abu', 'Yes', 'Yes', '2018-08-01 09:19:50', '2018-07-27 15:31:36'),
(64, 'Shahul', '9176129152', 'admin@example.com', 'shahul', 'shahul', 'Yes', 'Yes', '2018-08-01 09:20:04', '2018-07-28 10:00:57'),
(67, 'Younus', '9898923912', 'admin@example.com', 'younus', '123', 'Yes', 'No', '0000-00-00 00:00:00', '2018-08-01 09:18:57'),
(68, 'siraj', '+919941969786', 'SU@VZ.COM', 'Siraj', '1234', 'Yes', 'Yes', '0000-00-00 00:00:00', '2018-08-01 09:20:44'),
(69, 'gazali', '1111111111', 'rah@dv.com', 'gazali', '1234', 'Yes', 'Yes', '0000-00-00 00:00:00', '2018-08-01 09:21:11'),
(70, 'ibrahim', '9879890809', 'sulaiman@digivalsolution.com', 'ibrahim', '1234', 'No', 'No', '0000-00-00 00:00:00', '2018-08-01 09:22:00'),
(71, 'sheik', '9176129152', 'admin@example.com', 'sheik', '1234', 'Yes', 'No', '0000-00-00 00:00:00', '2018-08-01 09:22:41'),
(72, 'Abu', '+919941969786', 'abuthahir@digivalsolutions.com', 'Abu', '1234', 'Yes', 'Yes', '0000-00-00 00:00:00', '2018-08-01 09:23:26'),
(73, 'raja mohamed', '9879890809', 'abuthahir@digivalsolutions.com', 'Raja', '1234', 'Yes', 'No', '0000-00-00 00:00:00', '2018-08-01 09:24:19'),
(74, 'ayesha', '+919941969786', 'abuthahir@digivalsolutions.com', 'Ayesha', '1234', 'Yes', 'No', '0000-00-00 00:00:00', '2018-08-01 09:25:13');

-- --------------------------------------------------------

--
-- Structure for view `batchdetails_view`
--
DROP TABLE IF EXISTS `batchdetails_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digivalsolutions`@`localhost` SQL SECURITY DEFINER VIEW `batchdetails_view`  AS  select `batchdetails`.`id` AS `id`,`batchdetails`.`batch_id` AS `batch_id`,`batchdetails`.`name` AS `name`,`batchdetails`.`course_id` AS `course_id`,`batchdetails`.`fees` AS `fees`,`batchdetails`.`location_id` AS `location_id`,`batchdetails`.`days` AS `days`,`batchdetails`.`start_date` AS `start_date`,`batchdetails`.`end_date` AS `end_date`,`batchdetails`.`start_time` AS `start_time`,`batchdetails`.`end_time` AS `end_time`,`batchdetails`.`trainer_employee_id` AS `trainer_employee_id`,`batchdetails`.`status` AS `status`,`batchdetails`.`comments` AS `comments`,`batchdetails`.`modified` AS `modified`,`batchdetails`.`created` AS `created`,`location`.`name` AS `locationname`,`courses`.`fullname` AS `coursename`,`employees`.`name` AS `employeename` from (((`batchdetails` left join `courses` on(`courses`.`id` = `batchdetails`.`course_id`)) left join `location` on(`location`.`id` = `batchdetails`.`location_id`)) left join `employees` on(`employees`.`id` = `batchdetails`.`trainer_employee_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `batchportions_view`
--
DROP TABLE IF EXISTS `batchportions_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digivalsolutions`@`localhost` SQL SECURITY DEFINER VIEW `batchportions_view`  AS  select `batchportions`.`id` AS `id`,`batchportions`.`batch_id` AS `batch_id`,`batchportions`.`batch_date` AS `batch_date`,`batchportions`.`portions_targeted` AS `portions_targeted`,`batchportions`.`portions_completed` AS `portions_completed`,`batchportions`.`comments` AS `comments`,`batchportions`.`modified` AS `modified`,`batchportions`.`created` AS `created`,`batchdetails`.`name` AS `batchname` from (`batchportions` left join `batchdetails` on(`batchdetails`.`batch_id` = `batchportions`.`batch_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `employeeattendance_view`
--
DROP TABLE IF EXISTS `employeeattendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digivalsolutions`@`localhost` SQL SECURITY DEFINER VIEW `employeeattendance_view`  AS  select `employeeattendance`.`id` AS `id`,`employeeattendance`.`employee_id` AS `employee_id`,`employeeattendance`.`attendance_date` AS `attendance_date`,`employeeattendance`.`attended` AS `attended`,`employeeattendance`.`modified` AS `modified`,`employeeattendance`.`created` AS `created`,`employees`.`name` AS `name` from (`employeeattendance` join `employees` on(`employeeattendance`.`employee_id` = `employees`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `incomeexpense_view`
--
DROP TABLE IF EXISTS `incomeexpense_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digivalsolutions`@`localhost` SQL SECURITY DEFINER VIEW `incomeexpense_view`  AS  select distinct `incomeexpense`.`id` AS `id`,`incomeexpense`.`type` AS `type`,`incomeexpense`.`category` AS `category`,`incomeexpense`.`school_id` AS `school_id`,`incomeexpense`.`batch_id` AS `batch_id`,`incomeexpense`.`description` AS `description`,`incomeexpense`.`total_bill_amount` AS `total_bill_amount`,`incomeexpense`.`amount_received` AS `amount_received`,`incomeexpense`.`amount_paid` AS `amount_paid`,`incomeexpense`.`balance` AS `balance`,`incomeexpense`.`invoice_number` AS `invoice_number`,`incomeexpense`.`invoice_date` AS `invoice_date`,`incomeexpense`.`payment_date` AS `payment_date`,`incomeexpense`.`multi_payment_date` AS `multi_payment_date`,`incomeexpense`.`multi_amount_received` AS `multi_amount_received`,`incomeexpense`.`multi_amount_paid` AS `multi_amount_paid`,`incomeexpense`.`status` AS `status`,`incomeexpense`.`comments` AS `comments`,`incomeexpense`.`modified` AS `modified`,`incomeexpense`.`created` AS `created`,`batchdetails`.`name` AS `batchname`,`school`.`name` AS `schoolname` from ((`incomeexpense` left join `batchdetails` on(`batchdetails`.`batch_id` = `incomeexpense`.`batch_id`)) left join `school` on(`school`.`id` = `incomeexpense`.`school_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `meetingaction_view`
--
DROP TABLE IF EXISTS `meetingaction_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digivalsolutions`@`localhost` SQL SECURITY DEFINER VIEW `meetingaction_view`  AS  select `meetingaction`.`id` AS `id`,`meetingaction`.`meeting_id` AS `meeting_id`,`meetingaction`.`task_name` AS `task_name`,`meetingaction`.`task_start_date` AS `task_start_date`,`meetingaction`.`task_end_date` AS `task_end_date`,`meetingaction`.`task_assigned_to` AS `task_assigned_to`,`meetingaction`.`action_tobe_taken` AS `action_tobe_taken`,`meetingaction`.`action_taken` AS `action_taken`,`meetingaction`.`task_status` AS `task_status`,`meetingaction`.`comments` AS `comments`,`meetingaction`.`modified` AS `modified`,`meetingaction`.`created` AS `created`,`meetingdetails`.`name` AS `meetingname`,`employees`.`name` AS `employeename` from ((`meetingaction` left join `meetingdetails` on(`meetingdetails`.`id` = `meetingaction`.`meeting_id`)) left join `employees` on(`employees`.`id` = `meetingaction`.`task_assigned_to`)) ;

-- --------------------------------------------------------

--
-- Structure for view `studentattendance_batchportions_join_view`
--
DROP TABLE IF EXISTS `studentattendance_batchportions_join_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digivalsolutions`@`localhost` SQL SECURITY DEFINER VIEW `studentattendance_batchportions_join_view`  AS  select distinct `studentattendance`.`id` AS `rowid`,`studentattendance`.`student_id` AS `id`,`batchportions`.`batch_id` AS `batch_id`,`studentattendance`.`attendance_date` AS `attendance_date`,`studentattendance`.`attended` AS `attended`,`batchportions`.`batch_date` AS `batch_date` from (`batchportions` left join `studentattendance` on(`batchportions`.`batch_date` = `studentattendance`.`attendance_date`)) ;

-- --------------------------------------------------------

--
-- Structure for view `studentattendance_students_join_view`
--
DROP TABLE IF EXISTS `studentattendance_students_join_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digivalsolutions`@`localhost` SQL SECURITY DEFINER VIEW `studentattendance_students_join_view`  AS  select `studentattendance`.`id` AS `rowid`,`studentattendance`.`student_id` AS `id`,`studentattendance`.`batch_id` AS `batch_id`,`studentattendance`.`attendance_date` AS `attendance_date`,`studentattendance`.`attended` AS `attended`,`students`.`name` AS `name` from (`studentattendance` join `students` on(`students`.`id` = `studentattendance`.`student_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `studentattendance_view`
--
DROP TABLE IF EXISTS `studentattendance_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digivalsolutions`@`localhost` SQL SECURITY DEFINER VIEW `studentattendance_view`  AS  select `studentattendance`.`id` AS `id`,`studentattendance`.`student_id` AS `student_id`,`studentattendance`.`batch_id` AS `batch_id`,`studentattendance`.`batch_date_id` AS `batch_date_id`,`studentattendance`.`attendance_date` AS `attendance_date`,`studentattendance`.`attended` AS `attended`,`studentattendance`.`modified` AS `modified`,`studentattendance`.`created` AS `created`,`students`.`name` AS `name`,`batchdetails`.`name` AS `batchname` from ((`studentattendance` left join `students` on(`studentattendance`.`student_id` = `students`.`id`)) left join `batchdetails` on(`batchdetails`.`batch_id` = `studentattendance`.`batch_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `studentenrolment_students_join_view`
--
DROP TABLE IF EXISTS `studentenrolment_students_join_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digivalsolutions`@`localhost` SQL SECURITY DEFINER VIEW `studentenrolment_students_join_view`  AS  select `studentenrolment`.`student_id` AS `id`,`studentenrolment`.`batch_id` AS `batch_id`,`students`.`name` AS `name` from (`studentenrolment` join `students` on(`students`.`id` = `studentenrolment`.`student_id`)) where `studentenrolment`.`category` = 'Classroom' ;

-- --------------------------------------------------------

--
-- Structure for view `studentenrolment_view`
--
DROP TABLE IF EXISTS `studentenrolment_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`digivalsolutions`@`localhost` SQL SECURITY DEFINER VIEW `studentenrolment_view`  AS  select distinct `studentenrolment`.`id` AS `id`,`studentenrolment`.`student_id` AS `student_id`,`studentenrolment`.`batch_id` AS `batch_id`,`studentenrolment`.`course_id` AS `course_id`,`studentenrolment`.`category` AS `category`,`studentenrolment`.`enrolment_date` AS `enrolment_date`,`studentenrolment`.`books_id` AS `books_id`,`studentenrolment`.`course_fees` AS `course_fees`,`studentenrolment`.`books_fees` AS `books_fees`,`studentenrolment`.`course_fees_paid` AS `course_fees_paid`,`studentenrolment`.`books_fees_paid` AS `books_fees_paid`,`studentenrolment`.`discount` AS `discount`,`studentenrolment`.`dueamount` AS `dueamount`,`studentenrolment`.`status` AS `status`,`studentenrolment`.`comments` AS `comments`,`studentenrolment`.`modified` AS `modified`,`studentenrolment`.`created` AS `created`,`students`.`name` AS `name`,`courses`.`fullname` AS `coursename`,`inventory`.`name` AS `bookname`,`batchdetails`.`name` AS `batchname` from ((((`studentenrolment` left join `students` on(`students`.`id` = `studentenrolment`.`student_id`)) left join `courses` on(`courses`.`id` = `studentenrolment`.`course_id`)) left join `inventory` on(`studentenrolment`.`books_id` = `inventory`.`id`)) left join `batchdetails` on(`batchdetails`.`batch_id` = `studentenrolment`.`batch_id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batchdetails`
--
ALTER TABLE `batchdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batchportions`
--
ALTER TABLE `batchportions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employeeattendance`
--
ALTER TABLE `employeeattendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomeexpense`
--
ALTER TABLE `incomeexpense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetingaction`
--
ALTER TABLE `meetingaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetingdetails`
--
ALTER TABLE `meetingdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studentattendance`
--
ALTER TABLE `studentattendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studentenrolment`
--
ALTER TABLE `studentenrolment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batchdetails`
--
ALTER TABLE `batchdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `batchportions`
--
ALTER TABLE `batchportions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `employeeattendance`
--
ALTER TABLE `employeeattendance`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `enquiry`
--
ALTER TABLE `enquiry`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `incomeexpense`
--
ALTER TABLE `incomeexpense`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `meetingaction`
--
ALTER TABLE `meetingaction`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `meetingdetails`
--
ALTER TABLE `meetingdetails`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `studentattendance`
--
ALTER TABLE `studentattendance`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `studentenrolment`
--
ALTER TABLE `studentenrolment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
