<?php
	ob_start();
	session_start();
	 include_once("connection.php");
  
  $db = new dbObj();
  $connString =  $db->getConnstring();
	
	// if session is not set this will redirect to login page
  if ( isset($_SESSION['user'])=="") {
    header("Location: index.php");
    exit;
  }
	if ( isset($_SESSION['user'])!="" && isset($_SESSION['isAdmin'])!="No") {
    header("Location: users.php");
    exit;
  }
  if ( isset($_SESSION['user'])!="" && isset($_SESSION['isAdmin'])!="Yes" && isset($_SESSION['Active'])!="off") {
    header("Location: home.php");
    exit;
  }
	// select loggedin users detail
	$res=mysqli_query($connString, "SELECT * FROM users WHERE UserId=".$_SESSION['user']);
	$userRow=mysqli_fetch_array($res);

  $username = ""; $usernameError = "";
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Welcome - <?php echo $userRow['FullName']; ?></title>

<script type="text/javascript"  src="assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"  src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript"  src="assets/js/jquery.bootgrid.js"></script>
<script type="text/javascript"  src="assets/js/moment-with-locales.js"></script>
<script type="text/javascript"  src="assets/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/FileSaver.js"></script>

<script type="text/javascript" src="assets/js/tableExport.js"></script>
<script type="text/javascript" src="assets/js/jquery.base64.js"></script>


<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css" media="all">
<link href="assets/css/jquery.bootgrid.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.css" type="text/css" />
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">COMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="users.php">Users</a></li>
            <li class="active"><a href="transactions.php">Transactions</a></li>
            <li><a href="reports.php">Reports</a> </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			  <span class="glyphicon glyphicon-user"></span>&nbsp; <?php echo $userRow['FullName']; ?>&nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav> 

	<div id="wrapper">

	<div class="container">
    
    	<div class="page-header">
    	<h3>Transactions</h3>
    	</div>
      Filter:
          <?php
              $sql = "SELECT SchemeID, SchemeName FROM schemes order by SchemeName";

              $res=mysqli_query($connString, $sql) or die("error to read User data");

              echo "<select id='selectScheme' name='selectScheme'>";
              echo "<option value=''>-Scheme-</option>";
              while( $option = mysqli_fetch_array($res)) {
                echo "<option value='".htmlspecialchars($option['SchemeName'])."'>".htmlspecialchars($option['SchemeName'])."</option>";
              }

              echo "</select>";
              ?>
              <select id='selectSubscription' name='selectSubscription'>
            <option value=''>-Subscription-</option>
            <option value='One-time'>One-time</option>
            <option value='Monthly'>Monthly</option>
            
            </select> 
              <?php
              $sql = "SELECT DISTINCT YEAR(PayDate) as  PayYear FROM UserTransactions order by PayDate desc";

              $res=mysqli_query($connString, $sql) or die("error to read User data");

              echo "<select id='selectYear' name='selectYear'>";
              echo "<option value=''>-Year-</option>";
              while( $option = mysqli_fetch_array($res)) {
                echo "<option value='".htmlspecialchars($option['PayYear'])."'>".htmlspecialchars($option['PayYear'])."</option>";
              }

              echo "</select>";
          ?>
            <select id='selectMonth' name='selectMonth'>
            <option value=''>-Month-</option>
            <option value='1'>Jan</option>
            <option value='2'>Feb</option>
            <option value='3'>Mar</option>
            <option value='4'>Apr</option>
            <option value='5'>May</option>
            <option value='6'>Jun</option>
            <option value='7'>Jul</option>
            <option value='8'>Aug</option>
            <option value='9'>Sep</option>
            <option value='10'>Oct</option>
            <option value='11'>Nov</option>
            <option value='12'>Dec</option>
            </select> 
                    <br />
      <div>
      <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="btn_export" data-row-id="0">
      <span class="glyphicon glyphicon-export"></span> Export to Excel</button></div></div>

      <div class="">        
        <div class="col-sm-12">
    
    <table id="User_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
      <thead>
        <tr>
          <th data-column-id="TransID" data-type="numeric" data-identifier="true">TransID</th>
          <th data-column-id="UserID">UserID</th> 
          <th data-column-id="FullName">Name</th> 
          <th data-column-id="Amount">Amount</th>   
          <th data-column-id="PayType">PayType</th>   
          <th data-column-id="SchemeID">Scheme</th>  
          <th data-column-id="Subscription">Subscription</th>
          <th data-column-id="PayDate">PayDate</th>   
          <th data-column-id="Comments">Comments</th>   
        </tr>
      </thead>
    </table>
    </div>
      </div>
    </div>
  

 <div id="pay_model" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Payment</h4>
            </div>
            <div class="modal-body">
                
        </div>
    </div>
</div>   
    </div>
    
</body>
</html>
<script type="text/javascript">
$( document ).ready(function() {

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}

var uid=$.urlParam('uid');
var userid="";

if(uid!=null){userid=uid;}

  var grid = $("#User_grid").bootgrid({
    ajax: true,
    rowSelect: true,
    post: function ()
    {
      /* To accumulate custom parameter with the request object */
      return {
        id: "b0df282a-0d67-40e5-8558-c9e93b7befel",
        scheme: $("#selectScheme").val(),
        year: $("#selectYear").val(),
        month: $("#selectMonth").val(),
        subscription: $("#selectSubscription").val()
      };
    },
    
    url: "response.php?action=transactReports&userid="+userid+"&scheme="+$("#selectScheme").val()+"&year="+$("#selectYear").val()+"&month="+$("#selectMonth").val()+"&subscription="+$("#selectSubscription").val()
  });    
      
   


function ajaxAction(action) {
        data = $("#frm_"+action).serializeArray();
        $.ajax({
          type: "POST",  
          url: "response.php?action=transactReports&searchPhrase=&current=1&rowCount=10&&userid="+userid+"&scheme="+$("#selectScheme").val()+"&year="+$("#selectYear").val()+"&month="+$("#selectMonth").val()+"&subscription="+$("#selectSubscription").val(),  
          data: data,
          dataType: "json",       
          success: function(response)  
          {
            
            if(response.status!=-1) {
              $("#User_grid").bootgrid('reload');
            }
            else 
            {
              alert(response.message);
            }
          },
          error: function(error)  
          {
            alert("error");
          } 
        });
      }      
   

$('#selectScheme').change(function(){
  $(".search-field").val("");
   ajaxAction('transactReports');
  });

$('#selectYear').change(function(){
     $(".search-field").val("");
   ajaxAction('transactReports');
  });

$('#selectMonth').change(function(){
     $(".search-field").val("");
   ajaxAction('transactReports');
  });

$('#selectSubscription').change(function(){
     $(".search-field").val("");
   ajaxAction('transactReports');
  });

 $( "#btn_export" ).click(function() {
      $("#User_grid").bootgrid();
      $('#User_grid').tableExport({type:'excel'}); 
    });  

});


</script>

  <?php ob_end_flush(); ?>