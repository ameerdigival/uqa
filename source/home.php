<?php
	ob_start();
	session_start();
	 include_once("connection.php");
  
  $db = new dbObj();
  $connString =  $db->getConnstring();
	
	// if session is not set this will redirect to login page
	if( !isset($_SESSION['user']) && isset($_SESSION['Active'])!="on") {
		header("Location: index.php");
		exit;
	}
	// select loggedin users detail
	$res=mysqli_query($connString, "SELECT * FROM users WHERE UserId=".$_SESSION['user']);
	$userRow=mysqli_fetch_array($res);

  $username = ""; $usernameError = "";
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Welcome - <?php echo $userRow['FullName']; ?></title>
<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css" media="all">
<link href="assets/css/jquery.bootgrid.css" rel="stylesheet" />
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.bootgrid.min.js"></script>
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">COMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Transactions</a></li>
            <li><a href="#">Profile</a></li>      
          </ul>
          <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			  <span class="glyphicon glyphicon-user"></span>&nbsp; <?php echo $userRow['FullName']; ?>&nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav> 

  <div id="wrapper">

    <div class="container">

      <div class="page-header">
      <h3>Transactions</h3>
      </div>       

    </div>

  </div>
    
</body>
</html>



<?php ob_end_flush(); ?>