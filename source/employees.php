<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Employee Details</h3>
  	</div>        
    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","","");

              renderGridHeaderColumn("name","Name",true,"","","","","","");
              renderGridHeaderColumn("dateofbirth","Date of birth",false,"","","","","false","");
              renderGridHeaderColumn("gender","Gender",false,"","","","","true","");
              renderGridHeaderColumn("married","Married",false,"","","","","false","");
              renderGridHeaderColumn("qualification","Qualification",false,"","","","","false","");
              renderGridHeaderColumn("experience","Experience",false,"","","","","false","");
              renderGridHeaderColumn("job_category","Job category",true,"","","","","","");
              renderGridHeaderColumn("job_type","Job type",true,"","","","","false","");
              renderGridHeaderColumn("job_status","Job Status",false,"","","","","","");
              renderGridHeaderColumn("joined_date","Joined date",false,"","","","","","");
              renderGridHeaderColumn("resigned_date","Resigned date",false,"","","","","","");
              renderGridHeaderColumn("salary","Salary",false,"","","","","false","");
              renderGridHeaderColumn("phone","Phone",false,"","","","","","");
              renderGridHeaderColumn("address","Address",false,"","","","","false","");
              renderGridHeaderColumn("area","Area",false,"","","","","false","");
              renderGridHeaderColumn("city","City",false,"","","","","false","");
              renderGridHeaderColumn("country","Country",false,"","","","","false","");
              renderGridHeaderColumn("pincode","Pincode",false,"","","","","false","");
              renderGridHeaderColumn("email","Email",false,"","","","","false","");
              renderGridHeaderColumn("comments","Comments",false,"","","","","false","");


              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>

<!---- Add/Edit Form -->

<div id="edit_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo "<div class='form-sub-header'>Personal Information</div>";
            echo '<div class="row">';

             $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

              $sqlResult = fetchRecordForDropdown("select id, name, city, phone from enquiry order by name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['id']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;ID: ' .  $json[$loop]['id'] . '  &#160;&#160;Phone: ' . $json[$loop]['phone'] . ' &#160;&#160;City: ' . $json[$loop]['city']);
              }              

             // renderFormSelect("name","Name",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-4");    
              renderFormInput("name","Name",'data-validation="alphanumeric" data-validation-allowing=". "',"","col-md-4");  
              renderFormInput("dateofbirth","Date of birth",' placeholder="YYYY-MM-DD"',"","col-md-4");
              renderFormInput("qualification","Qualification",'',"","col-md-4");
            echo '</div>';
            echo '<div class="row">'; 
              renderFormRadio("gender","Gender",'',['Male', 'Female'],"","col-md-4");
              renderFormRadio("married","Married",'',['Yes', 'No'],"","col-md-4");  
            echo '</div>';

             echo "<br><div class='form-sub-header'>Contact Information</div>";
            echo '<div class="row">';
              renderFormInput("phone","Phone",'data-validation="required"',"","col-md-4");
              renderFormInput("email","Email",'data-validation="email" data-validation-error-msg="Enter a valid e-mail" data-validation-optional="true" placeholder="example@domain.com"',"","col-md-4");
            echo '</div>';

            echo "<br><div class='form-sub-header'>Address Information</div>";

            echo '<div class="row">';
              renderFormInput("city","City",'data-validation="required"',"","col-md-3");
              renderFormInput("area","Area",'',"","col-md-3"); 
              renderFormInput("country","Country",'',"","col-md-3");
              renderFormInput("pincode","Post / Zip code",'',"","col-md-3");                           
            echo '</div>';

            echo '<div class="row">';
              renderFormInput("address","Address",'',"","col-md-12");
            echo '</div>';


            echo "<br><div class='form-sub-header'>Experience Information</div>";
            echo '<div class="row">';
            renderFormTextarea("experience","Experience Summary",'',"","col-md-12");
            echo '</div>';

            echo "<br><div class='form-sub-header'>Employment Information</div>";
            echo '<div class="row">';            
            renderFormRadio("job_category","Job category",'data-validation="required"',['Staff','Trainer','Others'],'',"col-md-4");
            renderFormRadio("job_type","Job type",'data-validation="required"',['Permanent','Part-Time'],'',"col-md-4");
            renderFormRadio("job_status","Job status",'data-validation="required"',['Employed','Resigned'],'',"col-md-4");
            echo '</div>';

            echo '<div class="row">';              
              renderFormInput("joined_date","Join date",'data-validation="required" placeholder="YYYY-MM-DD"',"","col-md-3");

              renderFormInput("salary","Salary",'data-validation="number" data-validation-optional="true"',"","col-md-2");
              renderFormInput("resigned_date","Resign date",' placeholder="YYYY-MM-DD"',"","col-md-3");            
            echo '</div>';           

            echo "<br><div class='form-sub-header'>Additional Information</div>";
            echo '<div class="row">';
              renderFormTextarea("comments","Comments",'',"","");
            echo '</div>';


            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>

<script>
  $(document).ready(function() 
  { 
      
    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["employees"]]; // db table names for insert
    db_table_insert_columns = [["name","dateofbirth","gender","married","qualification","experience","job_category","job_type","job_status","joined_date","resigned_date","salary","phone","address","area","city","country","pincode","email","comments"]]; // db table column names for insert

    db_table_names_update = [["employees"]]; // db table names for update
    db_table_update_columns = [["name","dateofbirth","gender","married","qualification","experience","job_category","job_type","job_status","joined_date","resigned_date","salary","phone","address","area","city","country","pincode","email","comments"]]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   


    // load data grid
    loadBootgrid(db_table_names_insert[0].toString());   

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal
            formReset('frm_edit');
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data              
              $('#' + formId+' #id').val($(this).data("row-id")); // in case we're changing the key
              // ajax call
              sqlQuery = 'select * from ' + db_table_names_update + ' where id=' + $(this).data("row-id");
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                          populateEditFormData(response);
                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });
              } else 
              {  //alert('No row selected! First select row, then click edit button');
              }
          }); 
    });




function populateEditFormData(dataArray) {
  // populate the data to the form
  formReset('frm_edit');
  var dataArrayLength = dataArray[0].length, formElementValue = '';

  if(dataArray.length > 0) 
  {
    
      loopColumnsArrayLength = db_table_update_columns[0].length;

        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = dataArray[0][formElementId];
          

          
            if($("#"+formElementId).attr('elementType') === "text") {
              $('#' + formElementId).val(formElementValue);
            }

            if($("#" + formElementId).attr('elementType') === "radio") {
              
              if(formElementValue!="") {
                $("input[name="+formElementId+"][value='" + formElementValue + "']").prop('checked', true); 
              }
              
            }

            if($("#"+formElementId).attr('elementType') === "select") {
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
              formElementValueSplit = formElementValue.split(","); 
              
              if(formElementValueSplit.length>0) {
                for (loop = 0; loop<formElementValueSplit.length; loop++)
                {
                  $("#" + formElementId + " option[value='" + formElementValueSplit[loop] + "']").prop("selected",true);
                }
              }
              else {
                $("#"+formElementId+"[value=" + formElementValue + "]").prop("selected",true);
              }
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
            }

            if($("#" + formElementId).attr("elementType") === "textarea") {
              $('#' + formElementId).val(formElementValue);
            }
        }
  }
  $("#frm_edit input").each(function() {
       if (this.value == '0000-00-00') {
          this.value = '';
       }
       });
}

   /************** form interactions *************/


   // date popup

   $('#dateofbirth, #joined_date, #resigned_date').parent().addClass('date')
    $('#dateofbirth, #joined_date, #resigned_date').datetimepicker({
      format: 'YYYY-MM-DD'
   });

    $("#resigned_date").attr('disabled','disabled');

    $('#resigned_date').blur(function () {
        forminteractions () 
    });  

    $('#job_status').change(function () {
        forminteractions () 
    }); 

    $('#frm_edit').click(function () {
        forminteractions () 
    });

    function forminteractions () {
      var dateValue_2 = $('#resigned_date').val();
      if ($('input[id=job_status]:checked').val() == 'Resigned') 
        {
          $("#resigned_date").removeAttr('disabled');
          if(dateValue_2 == '' || dateValue_2 == '0000-00-00' ||dateValue_2 == undefined)
          {
            $('#resigned_date').attr("data-validation='required'");
            $('#div_resigned_date .formOptionalText').hide();
            $('#div_resigned_date .text-danger').html('This is a required field');
          }
          else
          {
            $('#resigned_date').attr("data-validation=''");
            $('#div_resigned_date .formOptionalText').show();
            $('#div_resigned_date .text-danger').html('');
          }
        
        }
      else {
        $('#resigned_date').val('');
        $("#resigned_date").attr('disabled','disabled');
        $('#resigned_date').attr("data-validation=''");
        $('#div_resigned_date .formOptionalText').show();
         $('#div_resigned_date .text-danger').html('');
      }
    }

   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
        sqlUpdateQueryArray.length=0;
        sqlInsertQueryArray.length=0;
        db_table_update_values.length=0;
        db_table_insert_values.length=0;

        if(db_table_update_columns.length!=0) {
          var tempArray = [], 
              formValue = '', 
              formElementId = '', 
              formElementValue = '', 
              loopColumnsArrayLength =0;


          /************* get edit form values and push to array ***************/

          loopColumnsArrayLength = db_table_update_columns[0].length;
          tempArray = [];
          for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
          {
            formElementId = db_table_update_columns[0][loopColumns];
            formElementValue = $('#' + formId + ' #' + formElementId).val();
              if($("#" + formElementId).attr('type') === "radio") {
                
                formElementValue = $("input[id="+formElementId+"]:checked").val();
                //console.log(formElementId + " : " + formElementValue)
              }

              if(formElementValue === undefined || formElementValue === null) {
                   formElementValue = '';            
              }
              tempArray.push(formElementValue); 
          }
          db_table_update_values.push(tempArray);



           /************* get add form values and push to array ***************/

          loopColumnsArrayLength = db_table_insert_columns[0].length;
          tempArray = [];
          for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
          {
            formElementId = db_table_insert_columns[0][loopColumns];
            formElementValue = $('#' + formId + ' #' + formElementId).val();
              if($("#" + formElementId).attr('type') === "radio") {
                
                formElementValue = $("input[id="+formElementId+"]:checked").val();
                //console.log(formElementId + " : " + formElementValue)
              }

              if(formElementValue === undefined || formElementValue === null) {
                   formElementValue = '';            
              }
            tempArray.push(formElementValue);                    
          }
          db_table_insert_values.push(tempArray);
        }



       var dateValue_1 = $('#joined_date').val();
       var dateValue_2 = $('#resigned_date').val();
       var dateComparison = compare_dates(dateValue_1, dateValue_2);
       if(dateComparison === "d1 > d2") {
        alert("Resign date should be greater than join date");
       }
       else {
        create_db_table_sql_update_query($('#' + formId + ' #id').val()); // For update query - db table row unique id as parameter
        create_db_table_sql_insert_query(); // For insert query 

        if ($('input[id=job_status]:checked').val() == 'Resigned') 
        {
          if(dateValue_2 == '' || dateValue_2 == '0000-00-00' ||dateValue_2 == undefined)
          {
            $('#resigned_date').attr("data-validation='required'");
            $('#div_resigned_date .formOptionalText').hide();
            $('#div_resigned_date .text-danger').html('This is a required field');
          }
          else
          {
            ajaxAction($('#action').val()); 
            $('#resigned_date').attr("data-validation=''");
            $('#div_resigned_date .formOptionalText').show();
            $('#div_resigned_date .text-danger').html('');
          }
        
        }
      else {
        ajaxAction($('#action').val()); 
        $('#resigned_date').attr("data-validation=''");
        $('#div_resigned_date .formOptionalText').show();
         $('#div_resigned_date .text-danger').html('');
      }
         // parameter "both" executes both insert and update sql
         }  

        
        return false; // Will stop the submission of the form
      }
    });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>