<?php
	ob_start();
	session_start();
	if( isset($_SESSION['user'])!="" ){
		header("Location: home.php");
	}
	include_once("connection.php");
	
	$db = new dbObj();
	$connString =  $db->getConnstring();
	
	class User {
	protected $conn;
	protected $data = array();
	function __construct($connString) {
		$this->conn = $connString;
	}
	}
	

	$error = false;
	$name = ""; $phone = ""; $email = ""; $username = ""; $pass = "";
	$nameError = ""; $phoneError = ""; $emailError = ""; $usernameError = ""; $passError = "";
	if ( isset($_POST['btn-signup']) ) {
		
		// clean user inputs to prevent sql injections
		$name = trim($_POST['name']);
		$name = strip_tags($name);
		$name = htmlspecialchars($name);
		
		$email = trim($_POST['email']);
		$email = strip_tags($email);
		$email = htmlspecialchars($email);

		$phone = trim($_POST['phone']);
		$phone = strip_tags($phone);
		$phone = htmlspecialchars($phone);

		$username = trim($_POST['username']);
		$username = strip_tags($username);
		$username = htmlspecialchars($username);
		
		$pass = trim($_POST['pass']);
		$pass = strip_tags($pass);
		$pass = htmlspecialchars($pass);
		
		// basic name validation
		if (empty($name)) {
			$error = true;
			$nameError = "Enter your name.";
		} else if (strlen($name) < 3) {
			$error = true;
			$nameError = "Name must have atleat 3 characters.";
		} else if (!preg_match("/^[a-zA-Z ]+$/",$name)) {
			$error = true;
			$nameError = "Name must contain alphabets and space.";
		}
		
		//basic email validation
		if (empty($email)!="1") {
			if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
				$error = true;
				$emailError = "Enter valid email address.";
			} else {
				// check email exist or not
				$query = "SELECT Email FROM users WHERE Email='$email'";
				$result = mysqli_query($connString, $query);
				$count = mysqli_num_rows($result);
				if($count!=0){
					$error = true;
					$emailError = "Provided Email is already in use.";
				}
			}
		}

		//basic phone validation
		if (empty($phone)) {
			$error = true;
			$phoneError = "Enter your phone number.";
		} 

		// user name validation
		if (empty($username)) {
			$error = true;
			$usernameError = "Enter username.";
		} else if (strlen($username) < 3) {
			$error = true;
			$usernameError = "Username must have atleat 3 characters.";
		} else {
				// check email exist or not
				$query = "SELECT UserName FROM users WHERE UserName='$username'";
				$result = mysqli_query($connString, $query);
				$count = mysqli_num_rows($result);
				if($count!=0){
					$error = true;
					$usernameError = "Provided Username is already in use.";
				}
			}

		// password validation
		if (empty($pass)){
			$error = true;
			$passError = "Enter password.";
		} else if(strlen($pass) < 5) {
			$error = true;
			$passError = "Password must have atleast 5 characters.";
		}
		
		// password encrypt using SHA256();
		//$pass = hash('sha256', $pass);

		// if there's no error, continue to signup
		if( !$error ) {			
			$query = "INSERT INTO users(UserName,PassWord,FullName,Phone,Email) VALUES('$username','$pass','$name','$phone','$email')";
			$res = mysqli_query($connString, $query);
				
			if ($res) {
				$errTyp = "success";
				$errMSG = "Successfully registered, you may <a href='index.php'>login</a> now";
				$name = ""; $phone = ""; $email = ""; $username = ""; $pass = "";
			} else {
				$errTyp = "danger";
				$errMSG = "Something went wrong, try again later...";	
			}	
				
		}
				
		
	}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Welcome Guest</title>
<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css" media="all">
<link href="assets/css/jquery.bootgrid.css" rel="stylesheet" />
<script src="assets/js/jquery-1.11.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.bootgrid.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js
"></script>
<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>


<link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" type="text/css" />

<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">COMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Login</a></li>
            <li class="active"><a href="register.php">Register</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			  <span class="glyphicon glyphicon-user"></span>&nbsp; Welcome Guest &nbsp;<span class="caret"></span></a>
             
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav> 

    	<div id="wrapper">
<div class="container">

	<div id="login-form">
    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">
    
    	<div class="col-md-12">
        
        	<div class="form-group">
            	<h2 class="">User Registration</h2>
            </div>
        
        	<div class="form-group">
            	<hr />
            </div>
            
            <?php
			if ( isset($errMSG) ) {
				
				?>
				<div class="form-group">
            	<div class="alert alert-<?php echo ($errTyp=="success") ? "success" : $errTyp; ?>">
				<span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                </div>
            	</div>
                <?php
			}
			?>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
            	<input type="text" name="name" class="form-control" placeholder="Full Name" maxlength="50" value="<?php echo $name ?>" />
                </div>
                <span class="text-danger"><?php echo $nameError; ?></span>
            </div>

            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span>
            	<input type="phone" name="phone" class="form-control" placeholder="Phone" maxlength="40" value="<?php echo $phone ?>" />
                </div>
                <span class="text-danger"><?php echo $phoneError; ?></span>
            </div>

            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
            	<input type="email" name="email" class="form-control" placeholder="Email" maxlength="50" value="<?php echo $email ?>" />
                </div>
                <span class="text-danger"><?php echo $emailError; ?></span>
            </div>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
            	<input type="username" name="username" class="form-control" placeholder="UserName" value="<?php echo $username; ?>" maxlength="60" />
                </div>
                <span class="text-danger"><?php echo $usernameError; ?></span>
            </div>            
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
            	<input type="password" name="pass" class="form-control" placeholder="Password" value="<?php echo $pass; ?>" maxlength="15" />
                </div>
                <span class="text-danger"><?php echo $passError; ?></span>
            </div>            
            
            <div class="form-group">
            	<hr />
            </div>
            
            <div class="form-group">
            	<button type="submit" class="btn btn-block btn-primary" name="btn-signup">Submit</button>
            </div>
            
            <div class="form-group">
            	<hr />
            </div>
            
            <div class="form-group">
            	<a href="index.php">Existing User Login Here...</a>
            </div>
        
        </div>
   
    </form>
    </div>	

</div>

</body>
</html>
<?php ob_end_flush(); ?>