<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Meeting Action</h3>
  	</div>       

<!---- Add/Edit Form -->

<div id="edit_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo "<div class='form-sub-header'>Basic Information</div>";
            echo '<div class="row">';
            $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

              $sqlResult = fetchRecordForDropdown("select id, name, date, location, time from meetingdetails order by date desc");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['id']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;Date: ' . $json[$loop]['date'] . '  &#160;&#160;Time: ' . $json[$loop]['time'] . ' &#160;&#160;Location: ' . $json[$loop]['location']);
              }              

              renderFormSelect("meeting_id","Meeting Name",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-6");
            echo '</div>';



            echo "<div id='div_task'>";
                echo "<br><div class='form-sub-header'>Task Information</div>";
                echo '<div class="row">';
                renderFormInput("task_name","Task name",'data-validation="alphanumeric" data-validation-allowing="#()-. "',"","col-md-4");

                $optionTexts=[]; $optionValues=[]; $optionSubTexts=[];
              $sqlResult = fetchRecordForDropdown("select id, name, job_category, job_type from employees where job_status = 'Employed' order by name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {  
                array_push($optionValues, $json[$loop]['id']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160; ID: ' .  $json[$loop]['id'] . ' &#160; Job category: ' . $json[$loop]['job_category'] . ' &#160; Job type: ' . $json[$loop]['job_type']);
              } 
              renderFormSelect("task_assigned_to","Task assigned to",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-4");

                renderFormInput("task_start_date","Start date",'data-validation="required" placeholder="YYYY-MM-DD"',"","col-md-2");
                  renderFormInput("task_end_date","End date",'placeholder="YYYY-MM-DD"',"","col-md-2");

               
                echo '</div>';


                echo '<div class="row">';
                renderFormRadio("task_status","Task status",'data-validation="required"',['Not started', 'Inprogress', 'Completed', 'Closed'],"","");
                echo '</div>';

                echo '<div class="row">';
                renderFormTextarea("action_tobe_taken","Action to be taken",'data-validation="required"',"","");
                echo '</div>';
                echo '<div class="row">';
                renderFormTextarea("action_taken","Action taken",'',"","");
                echo '</div>';
            echo '</div>';

            echo "<br><div class='form-sub-header'>Additional Information</div>";
            echo '<div class="row">';
              renderFormTextarea("comments","Comments",'',"","");
            echo '</div>';


            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","","");

              renderGridHeaderColumn("meetingname","Meeting",true,"","","","","true","");
              renderGridHeaderColumn("meeting_id","Meeting id",false,"","","","","false","");
              renderGridHeaderColumn("task_name","Task name",true,"","","","","true","");
              renderGridHeaderColumn("task_start_date","Task start date",false,"","","","","true","");
              renderGridHeaderColumn("task_end_date","Task end date",false,"","","","","true","");
              renderGridHeaderColumn("employeename","Action by",false,"","","","","true","");
              renderGridHeaderColumn("task_assigned_to","Emplolyee id",false,"","","","","false","");
              renderGridHeaderColumn("action_tobe_taken","Action to be taken",false,"","","","","false","");
              renderGridHeaderColumn("action_taken","Action taken",false,"","","","","false","");
              renderGridHeaderColumn("task_status","Task status",true,"","","","","true","");
              renderGridHeaderColumn("comments","Comments",false,"","","","","false","");

              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>



<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>

<script>
  $(document).ready(function() 
  { 
      
    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["meetingaction"]]; // db table names for insert
    db_table_insert_columns = [["meeting_id","task_name","task_start_date","task_end_date","task_assigned_to","action_tobe_taken","action_taken","task_status","comments"]]; // db table column names for insert

    db_table_names_update = [["meetingaction"]]; // db table names for update
    db_table_update_columns = [["meeting_id","task_name","task_start_date","task_end_date","task_assigned_to","action_tobe_taken","action_taken","task_status","comments"]]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   


    // load data grid
    loadBootgrid('meetingaction_view');   

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal
            formReset('frm_edit');
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data              
              $('#' + formId+' #id').val($(this).data("row-id")); // in case we're changing the key
              // ajax call
              sqlQuery = 'select * from ' + db_table_names_update + ' where id=' + $(this).data("row-id");
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                          populateEditFormData(response);
                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });
              } else 
              {  //alert('No row selected! First select row, then click edit button');
              }
          }); 
    });




function populateEditFormData(dataArray) {
  // populate the data to the form
  formReset('frm_edit');
  var dataArrayLength = dataArray[0].length, formElementValue = '';

  if(dataArray.length > 0) 
  {
    
      loopColumnsArrayLength = db_table_update_columns[0].length;

        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = dataArray[0][formElementId];
          
          console.log("#"+formElementId + " " + $('#' + formElementId).val())
          
            if($("#"+formElementId).attr('elementType') === "text") {
              $('#' + formElementId).val(formElementValue);
            }

            if($("#" + formElementId).attr('elementType') === "radio") {
              if(formElementValue!="") {
                $("input[name="+formElementId+"][value='" + formElementValue + "']").prop('checked', true); 
              }
              
            }

            if($("#"+formElementId).attr('elementType') === "select") {
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
              formElementValueSplit = formElementValue.split(",");
              
              if(formElementValueSplit.length>0) {
                for (loop = 0; loop<formElementValueSplit.length; loop++)
                {
                  $("#" + formElementId + " option[value='" + formElementValueSplit[loop] + "']").prop("selected",true);
                }
              }
              else {
                $("#"+formElementId+"[value=" + formElementValue + "]").prop("selected",true);
              }
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
            }

            if($("#" + formElementId).attr("elementType") === "textarea") {
              $('#' + formElementId).val(formElementValue);
            }
        }
  }

}

   /************** form interactions *************/

 var eventElementId = "category";
   $('#'+eventElementId).change(function () {
        formInteractions(eventElementId);
    });


     // date popup
    $('#task_start_date, #task_end_date').parent().addClass('date')
      $('#task_start_date, #task_end_date').datetimepicker({
      format: 'YYYY-MM-DD'
    });


$('#task_start_date, #task_end_date').blur(function () {
    formInteractions('');     
    });

$('#frm_edit').click(function () {
    formInteractions('');     
    });

   function formInteractions(elementId)
   {
    var startDateValue = $('#task_start_date').val();
    var endDateValue = $('#task_end_date').val();
    console.log(startDateValue + ''+endDateValue)
    if(startDateValue != "" && endDateValue != "" && endDateValue != startDateValue) {      

      if(moment(endDateValue).isAfter(startDateValue) == false)
      {
        $("#div_task_end_date .text-danger").html("End date should be greater than or equal to start date");
        $('#datesList').html('');
      }
      else {
        $("#div_task_end_date .text-danger").html("");
      }    
    }
     else {
        $("#div_task_end_date .text-danger").html("");
      }  
   }



     // date popup
     $('#date').parent().addClass('date')
    $('#date').datetimepicker({
      format: 'YYYY-MM-DD'
     });



   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
      sqlUpdateQueryArray.length=0;
      sqlInsertQueryArray.length=0;
      db_table_update_values.length=0;
      db_table_insert_values.length=0;

      if(db_table_update_columns.length!=0) {
        var tempArray = [], 
            formValue = '', 
            formElementId = '', 
            formElementValue = '', 
            loopColumnsArrayLength =0;


        /************* get edit form values and push to array ***************/

        loopColumnsArrayLength = db_table_update_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
            tempArray.push(formElementValue); 
        }
        db_table_update_values.push(tempArray);



         /************* get add form values and push to array ***************/

        loopColumnsArrayLength = db_table_insert_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_insert_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
          tempArray.push(formElementValue);                    
        }
        db_table_insert_values.push(tempArray);
      }

      create_db_table_sql_update_query($('#' + formId + ' #id').val()); // For update query - db table row unique id as parameter
      create_db_table_sql_insert_query(); // For insert query 
      ajaxAction($('#action').val());  // parameter "both" executes both insert and update sql
      return false; // Will stop the submission of the form
    }
  });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>