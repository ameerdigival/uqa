<?php
include_once("connection.php");

global $formRequiredFieldText, $formTextAtTop, $formOptionalFieldText;
$formRequiredFieldText = "";
$dayFullNames = [];  // array for days
$dayShortNames = [];  // array for days

$dayFullNames = ["Monday" , "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
$dayShortNames = ["Mon" , "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];


$formOptionalFieldText = "<span class='formOptionalText'>(optional)</span>";
$formTextAtTop = "Fields marked with * are mandatory";


function fetchRecordForDropdown($sqlQuery) 
{
	$db = new dbObj();
	$connString =  $db->getConnstring();
	$data = array();		
	$queryRecords = '';		

	$queryRecords = mysqli_query($connString, $sqlQuery) or die("error to fetch data");

	while( $row = mysqli_fetch_assoc($queryRecords) ) { 
		$data[] = $row;
		
	}
	return json_encode($data);
}

function renderFormTextAtTop($formTopText) {
	if($formTopText == "") 
	{
		$formTextAtTop = $GLOBALS['formTextAtTop'];
	}
	else {
		$formTextAtTop = $formTopText;
	}
	echo '<div class="formTextAtTop">' . $formTextAtTop . '</div>';
}	

function renderFormInput($elementID, $elementLabel, $additionalAttributes, $elementValue, $divClass) 
{	
//	echo($elementID);
// 	echo("validation=" . strpos($additionalAttributes, "validation="));
// 	echo("validation-optional=".strpos($additionalAttributes, "data-validation-optional=")."<br>");
	if (strpos($additionalAttributes, "validation=") < 1 || strpos($additionalAttributes, "data-validation-optional=") > 1) {
		$formRequiredFieldText = $GLOBALS['formOptionalFieldText'];
	}
	else {
		$formRequiredFieldText = $GLOBALS['formRequiredFieldText'];
	}
	echo '<div id="div_' . $elementID . '" class="form-group ' . $divClass . '">'
				.'<label for="' . $elementID . '" class="control-label">' . $elementLabel . ' ' . $formRequiredFieldText . '</label>'
				.'<input  type="text" elementType="text" class="form-control" id="'.$elementID . '" name="'.$elementID . '" value="'.$elementValue . '" ' . $additionalAttributes . ' />'
				.'<span class="text-danger"></span>'
			.'</div>';
}

function renderFormTextarea($elementID, $elementLabel, $additionalAttributes, $elementValue, $divClass) 
{	
	if (strpos($additionalAttributes, "validation=") < 1 || strpos($additionalAttributes, "data-validation-optional=") > 1) {
		$formRequiredFieldText = $GLOBALS['formOptionalFieldText'];
	}
	else {
		$formRequiredFieldText = $GLOBALS['formRequiredFieldText'];
	}
	echo '<div id="div_' . $elementID . '" class="form-group ' . $divClass . '">'
				.'<label for="' . $elementID . '" class="control-label">' . $elementLabel . ' ' . $formRequiredFieldText . '</label>'
				.'<textarea  elementType="textarea" class="form-control" rows="3" id="'.$elementID . '" name="'.$elementID . '" value="'.$elementValue . '" ' . $additionalAttributes . '></textarea>'
				.'<span class="text-danger"></span>'
			.'</div>';
}

function renderFormRadio($elementID, $elementLabel, $additionalAttributes, $elementValues, $elementRadioSelectedText, $divClass) 
{
	$renderFormSelectHtml = '';
	$checkedStatus = "";

	if (strpos($additionalAttributes, "validation=") < 1 || strpos($additionalAttributes, "data-validation-optional=") > 1) {
		$formRequiredFieldText = $GLOBALS['formOptionalFieldText'];
	}
	else {
		$formRequiredFieldText = $GLOBALS['formRequiredFieldText'];
	}

	$renderFormSelectHtml .= '<div id="div_' . $elementID . '" class="form-group ' . $divClass . '">'
						  . '<label for="' . $elementID . '" class="control-label">' . $elementLabel . ' ' . $formRequiredFieldText . '</label> <br>';

				for($loopIndex=0; $loopIndex<count($elementValues); $loopIndex++) {

					if($elementRadioSelectedText==$elementValues[$loopIndex]) {
						$checked = 'checked';
					}
					else {
						$checked = '';
					}
					$renderFormSelectHtml .= '<input type="radio"  elementType="radio" id="'.$elementID . '" name="'.$elementID . '" value="'. $elementValues[$loopIndex] . '" ' . $checked . ' ' . $additionalAttributes . ' /> ' . $elementValues[$loopIndex] . ' ' . '&#160; ';
				}
				
			$renderFormSelectHtml .= '<span class="text-danger"></span>';
			$renderFormSelectHtml .= '</div>';
	 echo $renderFormSelectHtml;
}

function renderFormSelect($elementID, $elementLabel, $additionalAttributes, $elementOptionValues, $elementOptionTexts, $elementOptionSelectedTexts, $optionsSubText, $divClass)
{	
	$renderFormSelectHtml = ''; $optionsSubTextString = ''; $elementOptionDataValue = '';
	$selected = '';

	if (strpos($additionalAttributes, "validation=") < 1 || strpos($additionalAttributes, "data-validation-optional=") > 1) {
		$formRequiredFieldText = $GLOBALS['formOptionalFieldText'];
	}
	else {
		$formRequiredFieldText = $GLOBALS['formRequiredFieldText'];
	}
	$renderFormSelectHtml .= '<div id="div_' . $elementID . '" class="form-group ' . $divClass . '">'
						  	.'<label for="' . $elementID . '" class="control-label">' . $elementLabel . ' ' . $formRequiredFieldText . '</label>'
				 		  	.'<select dropupAuto="false" data-live-search="true" elementType="select" class="form-control selectpicker" rows="5" id="'. $elementID . '" name="'. $elementID . '" ' . $additionalAttributes . '>'
				 		  	.'<option></option>';
				for($loopIndex=0; $loopIndex<count($elementOptionValues); $loopIndex++) {

					if(strpos($elementOptionValues[$loopIndex], "|") != 0) {
						$elementOptionValuesSplitted = explode("|", $elementOptionValues[$loopIndex]);
						$elementOptionDataValue = $elementOptionValuesSplitted[1];
						$elementOptionValueUpdated = $elementOptionValuesSplitted[0];
					}
					else {
						$elementOptionValueUpdated = $elementOptionValues[$loopIndex];
					}

					if(in_array($elementOptionValues[$loopIndex], $elementOptionSelectedTexts)) {
						$selected = 'selected';
					}
					else {
						$selected = '';
					}

					if(count($optionsSubText)>0) {
						$optionsSubTextString = $optionsSubText[$loopIndex];
					}

					$renderFormSelectHtml .= '<option data-value="' . $elementOptionDataValue . '" data-subtext="' . $optionsSubTextString . '" ' . $selected . ' value="' . $elementOptionValueUpdated . '">' . $elementOptionTexts[$loopIndex] . '</option>';
				}
				
	$renderFormSelectHtml .= '</select>'
				.'<span class="text-danger"></span>'
			.'</div>';
	 echo $renderFormSelectHtml;
}

function renderFormCheckbox($elementID, $elementLabel, $additionalAttributes, $isChecked, $divClass) 
{
	$checkedStatus = "";
	if($isChecked == true) { $checkedStatus = "checked";}
	if (strpos($additionalAttributes, "validation=") < 1 || strpos($additionalAttributes, "data-validation-optional=") > 1) {
		$formRequiredFieldText = $GLOBALS['formOptionalFieldText'];
	}
	else {
		$formRequiredFieldText = $GLOBALS['formRequiredFieldText'];
	}
	echo '<div id="div_' . $elementID . '" class="form-group ' . $divClass . '">'
				.'<label for="' . $elementID . '" class="control-label">' . $elementLabel . ' ' . $formRequiredFieldText . '</label> '
				.'<input type="checkbox" elementType="checkbox" id="'.$elementID.'" name="'.$elementID . '" ' . $checkedStatus . ' ' . $additionalAttributes . ' />'
				.'<span class="text-danger"></span>'
			.'</div>';
}

function renderModalFooter($elementID, $elementLabel) 
{
	return '<div class="modal-footer">'
              .'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
              .'<button type="submit" id="'.$elementID.'" class="btn btn-primary">'.$elementLabel.'</button>'
            .'</div>';
}

function renderDeleteModal() 
{
	echo '<div id="delete_modal" class="modal fade">'
			  .'<div class="modal-dialog">'
			    .'<div class="modal-content">'
			      .'<div class="modal-header">'
			          .'<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
			          .'<h4 class="modal-title">Delete</h4>'
			      .'</div>'
			      .'<div class="modal-body">'
			        .'<form method="post" id="frm_delete">'
			            .'<input type="hidden" value="delete" name="action" id="action" />'
			            .'<input type="hidden" value="0" name="delete_id" id="delete_id" />'
			            .'<div class="form-group">'
			              .'<div class="control-label" id="deleteConfirmInfo"></div>'                
			            .'</div>'               
			            .renderModalFooter("btn_delete","Delete","true")
			        .'</form>'
			      .'</div>'
			    .'</div>'
			  .'</div>'
			.'</div>';
}

function renderGridHeaderColumn($headerID, $headerLabel,$showInMobile,$dataFormatter,$dataSortable,$dataType,$dataIdentifier, $isVisibile, $additionalAttributes) 
{	
	$mobileHideClass = "";
	if($showInMobile == false) {
		$mobileHideClass='data-css-class="hidden-xs hidden-sm" data-header-css-class="hidden-xs hidden-sm"';
	}
	echo '<th data-order=desc data-column-id="' . $headerID . '" data-type="' . $dataType . '" data-identifier="' . $dataIdentifier . '" data-formatter="' . $dataFormatter . '" data-sortable="' . $dataSortable . '" ' . $mobileHideClass.' data-visible="' . $isVisibile . '" ' . $additionalAttributes . ' >' . $headerLabel . '</th>';
}




?>