<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Meeting Actions Report</h3>
  	</div>        
    <div class="col-sm-12">

      <div id="filterArea" class="form-row">
        <div class="row">
          <div class="col-md-3">
            <?php 
            $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

              $sqlResult = fetchRecordForDropdown("select id, name, date, time, location, attendees from meetingdetails order by date desc");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                $dataExtra =  $json[$loop]['date'] . "##$$" .  $json[$loop]['time'] . "##$$" .  $json[$loop]['location'] . "##$$" .  $json[$loop]['attendees'];
                array_push($optionValues, $json[$loop]['id'] . "|" . $dataExtra);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;Date: ' . $json[$loop]['date'] . '  &#160;&#160;Location: ' . $json[$loop]['location'] . ' &#160;&#160;Time: ' . $json[$loop]['time']);
              }              

              renderFormSelect("meeting_id","Meeting name",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"");
            ?>
          </div>
          
          <div class="pull-right" style='padding-bottom:10px; padding-top: 5px;'>
            <button type="button" class="btn btn-xs btn-primary" id="btn-pdf" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> PDF</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-xls" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> Excel</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-print" data-row-id="0">
            <span class="glyphicon glyphicon-print"></span> Print</button>
          </div>

          <div class="form-group col-md-4">
          </div>

          <div class="form-group col-md-4">
          </div>
        </div>            
      </div>
      
      <div id="printArea" class="row">

       
      </div>
     

    </div>
  </div>
</div>


<?php include_once("footer.php"); ?>
</div>

<script>
$(document).ready(function() 
{ 

var db_table_name = [] , db_table_columns = [], table_header = [];

db_table_name = ["meetingaction"]; // db table names 
db_table_where_colummn = []; // db table names
db_table_columns = ["id", "action_tobe_taken", "employeename", "action_taken", "task_status"]; // db table column
table_header = ["Sl.No.","Action to be taken", "Action by", "Action taken", "Status"]; // db table column

$("#meeting_id").prop("selectedIndex", 1);
$("#meeting_id").selectpicker('refresh');
loadAjaxCall();

$("#meeting_id").change(function () {
  $("#printArea").html("");
  loadAjaxCall();
});


function loadAjaxCall() {
  var meeting_id = $("#meeting_id option:selected").val();
   if(meeting_id != '') {
      var sqlQuery = "SELECT meetingaction.*, employees.name as employeename, meetingdetails.name as meetingname, meetingdetails.date, meetingdetails.location, meetingdetails.time, meetingdetails.attendees FROM meetingaction left join meetingdetails on meetingdetails.id = meetingaction.meeting_id left join employees on employees.id = meetingaction.task_assigned_to where meetingaction.meeting_id = '" + meeting_id + "'";
      
      
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                  populateTable(response);

              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });

      
   }
}

function populateTable(response) 
{ 
var htmlTableData = '', netIncome = 0, netExpense = 0, meetingData = '';
meetingData = $("#meeting_id option:selected").attr("data-value"); 
meetingDataSplitted = meetingData.split("##$$"); 
        htmlTableData += '<table class="reportHeader"><tbody><tr><td><b>Meeting Actions</b></td></tr></tbody></table>' +
                         '<center><div class="table-responsive"><table id="tableHeaderData" class="table table-bordered table-responsive" style="width:auto"><thead><tr><th>Meeting Name</th><th>Date</th><th>Time</th><th>Location</th><th>Attendees</th></tr></thead>' + 
                          '<tbody><tr><td>' + $("#meeting_id option:selected").text() + '</td>' +
                          '<td>' + meetingDataSplitted[0] + '</td>' +
                          '<td>' + meetingDataSplitted[1] + '</td>' +
                          '<td>' + meetingDataSplitted[2] + '</td>' + 
                          '<td>' + meetingDataSplitted[3] + '</td></tr>' +
                          '</tbody>' +
                        '</table></div></center>' +
                          '<div class="table-responsive"><table id="tableData" class="table table-bordered table-responsive">' + 
                          '<thead>' +
                          '<tr>';
        var tableHeaderLength = table_header.length;
        for(loopColumns=0;loopColumns<tableHeaderLength;loopColumns++) 
        {
          
              htmlTableData += '<th>' + table_header[loopColumns] + '</th>'              
           
        }
        htmlTableData += '</tr>' +
                          '</thead>';

        htmlTableData += '<tbody>' 
                          
        var responseLength = response.length;
        for(loopColumns=0;loopColumns<responseLength;loopColumns++) 
        {          
              htmlTableData += '<tr>'
                  var db_table_columns_Length = db_table_columns.length;
                  for(loopHeaders=0; loopHeaders<db_table_columns_Length; loopHeaders++)  {
                    if(loopHeaders==0) {
                      htmlTableData += '<td>' + parseInt(loopColumns+1) + '</td>';
                    }
                    else {
                      var rowContent = response[loopColumns][db_table_columns[loopHeaders]];
                      if(rowContent != null)
                      {
                        htmlTableData += '<td>' + response[loopColumns][db_table_columns[loopHeaders]] + '</td>';
                      }
                      else {
                        htmlTableData += '<td>&#160;</td>';
                      }
                    }
                    if(db_table_columns[loopHeaders] == "income") {
                      netIncome += parseInt(response[loopColumns][db_table_columns[loopHeaders]]);
                    }
                    if(db_table_columns[loopHeaders] == "expense") {
                      netExpense += parseInt(response[loopColumns][db_table_columns[loopHeaders]]);
                    }
                    
                  }

               htmlTableData += '</tr>'
        }
        htmlTableData += '</tbody></table></div><br><br>';
        console.log(htmlTableData)
        $("#printArea").html(htmlTableData);
}

});

 $( "#btn-xls" ).click(function() {
      $('#printArea').tableExport({type:'excel'}); 
 }); 

 $( "#btn-print" ).click(function() {
      window.print(); 
 });

 $( "#btn-pdf" ).click(function() {
      pdfExport("printArea");
 });

</script>
</body>
</html>

<?php ob_end_flush(); ?>