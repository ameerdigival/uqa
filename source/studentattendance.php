<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>


<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Student Attendance</h3>
  	</div>   

    <!---- Add/Edit Form -->

<div id="edit_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo '<div class="row">';

            $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

              $sqlResult = fetchRecordForDropdown("select id, batch_id, name, start_date, end_date, status from batchdetails where status <> 'Completed' order by name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['batch_id']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;ID: ' .  $json[$loop]['id'] . '  &#160;&#160;Batch Date: ' . $json[$loop]['start_date'] . ' to ' . $json[$loop]['end_date'] . '  &#160;&#160;status: ' . $json[$loop]['status']);
              }              

              renderFormSelect("batch_id","Batch",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-4");  

           
            ?>   
            <div id="div_attendancedate" class="form-group col-md-3">
              <label for="attendancedate" class="control-label">Attendance Date </label>
              <select elementtype="select" class="form-control selectpicker" data-live-search="true" rows="5" id="attendancedate" name="attendancedate" data-validation="required"><option></option>
              </select>
                <span class="text-danger"></span>
              </div> 
              <?php         
            echo '</div>';

            echo '<div id="divAttendance" class="row">'; 
            echo '</div>'; 

            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            
            </div>
          </form>
      </div>
    </div>
  </div>
</div> 
    
    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","","");
              renderGridHeaderColumn("name","Student",true,"","","","","true","");
              renderGridHeaderColumn("batchname","Batch",true,"","","","","true","");
              renderGridHeaderColumn("student_id","Student id",true,"","","","","false","");
              renderGridHeaderColumn("attendance_date","Attendance date",true,"","","","","true","");
              renderGridHeaderColumn("attended","Attended",true,"","","","","true","");

              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>



<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>
<script>
var sqlQuery = '', attendanceRowCount = 0, recordExist = false;

function loadNewAttendanceList(batchid) {
  
   sqlQuery = 'select studentenrolment.student_id as id, studentenrolment.batch_id, students.name from studentenrolment join students on students.id = studentenrolment.student_id where studentenrolment.category = "Classroom" AND studentenrolment.batch_id="' + batchid + '" order by students.name';

  console.log(sqlQuery);
  data = {
  action:"fetch",
  sqlQuery: sqlQuery
  };
  $.ajax({
          type: "POST",  
          url: "response.php",  
          data: data,
          dataType: "json",       
          success: function(response)  
          { 
            console.log(response)
            if(response.status!=-1) 
            {
              var responseLength = response.length;
                if(responseLength > 0) {
                    recordExist = false;
                    showAttendanceList(response, responseLength);
                }
                else {
                  recordExist = true;
                  $('#divAttendance').html("<span class='text-danger'> Students not enrolled for this batch date</span>");
                }
            }
            else 
            {
              $("#divAttendance").html(response.message);
            }
          },
          error: function(req, status, error)  
          {
            $("#divAttendance").html("Error: \n"+status+"\n"+error);
          } 
          });
}
  
function showAttendanceList(response, responseLength) {
 var htmlAtendance = '', chkBoxChecked = "";
 attendanceRowCount = responseLength;
 $("#divAttendance").html('');
 htmlAtendance += '<br><table class="table">';
 htmlAtendance += '<tr><th style="border:0;width : 60px">Emp Id</th><th style="border:0;">Name</th><th style="border:0;">Attended</th></tr>';
  for(loopResponse = 0; loopResponse < responseLength; loopResponse++)
  {
    if(response[loopResponse]["attended"] == "Yes") {
        chkBoxChecked = "checked";
    }
    else {
      chkBoxChecked = "";
    }
    htmlAtendance += '<tr>' +
                     '<td id="data-row-' + loopResponse + '" width=50 style="vertical-align: middle;">' +
                      response[loopResponse]["id"] +
                     '</td>' +
                     '<td width=200 style="vertical-align: middle;">' +
                     '</div>' + response[loopResponse]["name"] +
                     '</td>' +
                      '<td>' +
                      '<label class="toggle-check">' +
                      '<input type="checkbox" class="toggle-check-input" id="chkBoxAttended'+loopResponse+'" ' + chkBoxChecked + ' name="chkBoxAttended'+loopResponse + '"  />' +
                      '<span class="toggle-check-text"></span>' +
                      '</label>' +
                      '</td>' +
                      '<td style="display:none" id="td_attendanceDate' + loopResponse + '">' + response[loopResponse]["attendance_date"] + 
                      '</td>' +
                      '<td style="display:none" id="td_rowid' + loopResponse + '">' + response[loopResponse]["rowid"] + 
                      '</td>' +
                      '</tr>';
  }
  console.log(attendanceRowCount)
  htmlAtendance += '</table><br><br>';
  $("#divAttendance").html(htmlAtendance);
}

function getBatchDates(batchid, attendancedateID, attendancedate, rowid) {
  $('#attendancedate').empty();
  console.log(attendancedate)
sqlQuery = '', attendanceRowCount = 0;
  sqlQuery = 'select id, batch_date from batchportions where batch_id="' + batchid +  '" order by batch_date';
  data = {
  action:"fetch",
  sqlQuery: sqlQuery
  };
  $.ajax({
          type: "POST",  
          url: "response.php",  
          data: data,
          dataType: "json",       
          success: function(response)  
          { 
            if(response.status!=-1) 
            {
              var responseLength = response.length;
                if(responseLength > 0) {
                       $("#attendancedate").empty();
                  for(loopResponse = 0; loopResponse < responseLength; loopResponse++) {
                    if(loopResponse==0) {                      
                      $("#attendancedate").append('<option></option>');
                    }                    
                    $("#attendancedate").append('<option value="'+response[loopResponse]["id"]+'">'+response[loopResponse]["batch_date"]+'</option>');
                  }
                  $('#attendancedate').selectpicker('refresh');
                  if(attendancedateID!='')
                  {
                    $("#attendancedate option[value='" + attendancedateID + "']").prop("selected",true);
                    $('#attendancedate').selectpicker('refresh');
                    getExistingAttendance(batchid, attendancedate, rowid)
                  }
                }
                else {
                  $("#divAttendance").html("No record(s) found");
                }
            }
            else 
            {
              $("#divAttendance").html(response.message);
            }
          },
          error: function(req, status, error)  
          {
            $("#divAttendance").html("Error: \n"+status+"\n"+error);
          } 
          });
}

function getExistingAttendance(batch_id,attendance_date, rowid) {
  sqlQuery = '', attendanceRowCount = 0;
  sqlQuery = 'select studentattendance.id as rowid, studentattendance.student_id as id, studentattendance.batch_id, studentattendance.batch_date_id, studentattendance.attendance_date, studentattendance.attended, students.name from studentattendance left join students on students.id = studentattendance.student_id';
 

  if(rowid != '') {
    sqlQuery += ' where studentattendance.id = "' + rowid + '"';
  }
  else {
     if(batch_id != '' && attendance_date != '') {
    sqlQuery += ' where batch_id = "' + batch_id + '" AND attendance_date="' + attendance_date + '"';
  }
  }
  

  console.log(sqlQuery);
  data = {
  action:"fetch",
  sqlQuery: sqlQuery
  };
  $.ajax({
          type: "POST",  
          url: "response.php",  
          data: data,
          dataType: "json",       
          success: function(response)  
          { 
            if(response.status!=-1) 
            {
              var responseLength = response.length;
                if(responseLength > 0) {
                  recordExist = true;
                  showAttendanceList(response, responseLength);               
                  
                }
                else {
                  recordExist = false;
                  loadNewAttendanceList(batch_id);                  
                }
            }
            else 
            {
              $("#divAttendance").html(response.message);
            }
          },
          error: function(req, status, error)  
          {
            $("#divAttendance").html("Error: \n"+status+"\n"+error);
          } 
          });
}       

  $(document).ready(function() 
  { 
      
    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["studentattendance"]]; // db table names for insert
    db_table_insert_columns = ["student_id","batch_id","batch_date_id","attendance_date","attended"]; // db table column names for insert

    db_table_names_update = [["studentattendance"]]; // db table names for update
    db_table_update_columns = ["attended"]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   


    // load data grid
    loadBootgrid('studentattendance_view','');

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal
            formReset('frm_edit');
            $("#divAttendance").html('');
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data
              
              // ajax call
              sqlQuery = 'select * from studentattendance where id=' + $(this).data("row-id");
              var rowID = $(this).data("row-id");
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                          populateEditFormData(response[0]["batch_id"], response[0]["batch_date_id"], response[0]["attendance_date"], rowID);
                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });
      
            } else 
            {  //alert('No row selected! First select row, then click edit button');
            }
          }); 
    });




function populateEditFormData(batchid, batch_date_id, attendancedate, rowid) {
  // populate the data to the form
  formReset('frm_edit');
  console.log("batchid"+batchid)
  $("#divAttendance").html('');
    if(batchid != '') {
      $("#batch_id option[value='" + batchid + "']").prop("selected",true);   
      $('#batch_id').selectpicker('refresh');
      getBatchDates($('#batch_id').val(),batch_date_id, attendancedate,rowid);
      
      
    }
}

   /************** form interactions *************/


   // date popup


    $('#batch_id').change(function () {
        if($('#batch_id').val() !='') {
          $("#divAttendance").html('');
          getBatchDates($('#batch_id').val(),'','');
        }
    });

    $('#attendancedate').change(function () {
      if($('#attendancedate option:selected').val() != '' && $('#batch_id').val() !='') {
        getExistingAttendance($('#batch_id').val(), $('#attendancedate option:selected').text(),'');
      }
    });
    

$( "#command-add" ).click(function() {
    $("#divAttendance").html('');
  });

   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
        sqlUpdateQueryArray.length=0;
        sqlInsertQueryArray.length=0;
        db_table_update_values.length=0;
        db_table_insert_values.length=0;

        if(attendanceRowCount > 0) 
        {
          var chkBoxAttended = "";
          for (loopAttendance = 0; loopAttendance < attendanceRowCount; loopAttendance++)
          {
            if($("#chkBoxAttended"+loopAttendance).prop("checked") == true) {
              chkBoxAttended = "Yes"
            }
            else {
              chkBoxAttended = "No"
            }
            var td_attendanceDate = $("#td_attendanceDate"+loopAttendance).html();

            if(recordExist == false){            
             tempQuery = 'INSERT INTO ' + db_table_names_insert + '(' + db_table_insert_columns + ') VALUES ("'+$("#data-row-" + loopAttendance).text() + '","' + $("#batch_id option:selected").val()  + '","' + $("#attendancedate option:selected").val()  + '","' + $("#attendancedate option:selected").text() + '","' + chkBoxAttended + '")'; 
             sqlInsertQueryArray.push(tempQuery);
             console.log(tempQuery); 
           }
           if(recordExist == true){   
             tempQuery = 'UPDATE ' + db_table_names_update + ' SET ' + db_table_update_columns + ' = "' + chkBoxAttended + '" where id = "' + $("#td_rowid" + loopAttendance).html() + '"';
             sqlUpdateQueryArray.push(tempQuery);
            console.log(tempQuery);
            } 
          }
          $("#divAttendance").html('');
          //console.log(recordExist);
             if (sqlInsertQueryArray.length > 0 && sqlUpdateQueryArray.length <1) {
              ajaxAction("add");
             }
             else if (sqlUpdateQueryArray.length > 0 && sqlInsertQueryArray.length <1) {
              ajaxAction("edit");
             }
             else if (sqlInsertQueryArray.length > 0 && sqlUpdateQueryArray.length >0) {
              ajaxAction("both");
             }
   
        }

        
        return false; // Will stop the submission of the form
      }
    });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>