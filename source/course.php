<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Course Details</h3>
  	</div>        
    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","","");

              renderGridHeaderColumn("fullname","Full name",true,"","","","","","");
              renderGridHeaderColumn("shortname","Short name",false,"","","","","","");
              renderGridHeaderColumn("coursefees","Course fees",true,"","","","","","");
              renderGridHeaderColumn("summary","Summary",false,"","","","","","");


              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>

<!---- Add/Edit Form -->

<div id="edit_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo "<div class='form-sub-header'>Basic Information</div>";
            echo '<div class="row">';
              renderFormInput("fullname","Full name",'data-validation="alphanumeric" data-validation-allowing="#(),-. "',"","col-md-5");
              renderFormInput("shortname","Short name",'data-validation="alphanumeric" data-validation-allowing="#()-. "',"","col-md-3");
              renderFormInput("coursefees","Course fees",'data-validation="number"',"","col-md-3");
            echo '</div>';



            echo "<br><div class='form-sub-header'>Additional Information</div>";
            echo '<div class="row">';
              renderFormTextarea("summary","Summary",'',"","");
            echo '</div>';


            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>

<script>
  $(document).ready(function() 
  { 
      
    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["courses"]]; // db table names for insert
    db_table_insert_columns = [["fullname","shortname","coursefees","summary"]]; // db table column names for insert

    db_table_names_update = [["courses"]]; // db table names for update
    db_table_update_columns = [["fullname","shortname","coursefees","summary"]]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   


    // load data grid
    loadBootgrid(db_table_names_insert[0].toString());   

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal
            formReset('frm_edit');
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data              
              $('#' + formId+' #id').val($(this).data("row-id")); // in case we're changing the key
              // ajax call
              sqlQuery = 'select * from ' + db_table_names_update + ' where id=' + $(this).data("row-id");
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                          populateEditFormData(response);
                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });
              } else 
              {  //alert('No row selected! First select row, then click edit button');
              }
          }); 
    });




function populateEditFormData(dataArray) {
  // populate the data to the form
  formReset('frm_edit');
  formInteractions(eventElementId);
  var dataArrayLength = dataArray[0].length, formElementValue = '';

  if(dataArray.length > 0) 
  {
    
      loopColumnsArrayLength = db_table_update_columns[0].length;

        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = dataArray[0][formElementId];
          
          console.log("#"+formElementId + " " + $('#' + formElementId).val())
          
            if($("#"+formElementId).attr('elementType') === "text") {
              $('#' + formElementId).val(formElementValue);
            }

            if($("#" + formElementId).attr('elementType') === "radio") {
              formInteractions(eventElementId);
              if(formElementValue!="") {
                $("input[name="+formElementId+"][value='" + formElementValue + "']").prop('checked', true); 
              }
              
            }

            if($("#"+formElementId).attr('elementType') === "select") {
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
              formElementValueSplit = formElementValue.split(",");
              
              if(formElementValueSplit.length>0) {
                for (loop = 0; loop<formElementValueSplit.length; loop++)
                {
                  $("#" + formElementId + " option[value='" + formElementValueSplit[loop] + "']").prop("selected",true);
                }
              }
              else {
                $("#"+formElementId+"[value=" + formElementValue + "]").prop("selected",true);
              }
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
            }

            if($("#" + formElementId).attr("elementType") === "textarea") {
              $('#' + formElementId).val(formElementValue);
            }
        }
  }

}

   /************** form interactions *************/

   $('#div_batch_id').hide();
   $('#div_course_id').hide();

   var eventElementId = "category";
   $('input:radio[id='+eventElementId+']').change(function () {
        formInteractions(eventElementId);
    });


   $('#course_fees_paid, #books_fees_paid, #course_fees, #books_fees, #discount').change(function() {
      dueAmountCalculations();  
   });

   $('#course_fees_paid, #books_fees_paid, #course_fees, #books_fees, #discount').keypress(function() {
      dueAmountCalculations();  
   });

   function dueAmountCalculations() {
      var course_fees_paid = $('#course_fees_paid').val() == "" ? 0 : $('#course_fees_paid').val();
      var course_fees = $('#course_fees').val() == "" ? 0 : $('#course_fees').val();
      var books_fees_paid = $('#books_fees_paid').val() == "" ? 0 : $('#books_fees_paid').val();
      var books_fees = $('#books_fees').val() == "" ? 0 : $('#books_fees').val();
      var discount = $('#discount').val() == "" ? 0 : $('#discount').val();

      $("#dueamount").val( ( parseInt(course_fees) - parseInt(discount) + parseInt(books_fees) ) - ( parseInt(course_fees_paid) + parseInt(books_fees_paid) )  ); 
   }

   function formInteractions(elementId)
   {
        if ($("input[id="+eventElementId+"]:checked").val() == 'Classroom') {
          $('#div_batch_id').show();
          $('#div_batch_id .formOptionalText').hide();
          $('#batch_id').attr("data-validation", "required");
          $('#div_course_id').hide();
          $('#div_course_id .formOptionalText').hide();
          $('#course_id').attr("data-validation", "");
          $('#course_id .selectpicker').val('');
          $('#course_id').val('default').selectpicker("refresh");
        }


        if ($("input[id="+eventElementId+"]:checked").val() == 'Online') {
          $('#div_course_id').show();
          $('#div_course_id .formOptionalText').hide();
          $('#course_id').attr("data-validation", "required");
          $('#div_batch_id').hide();
          $('#div_batch_id .formOptionalText').hide();
          $('#batch_id').attr("data-validation", "");
          $('#batch_id .selectpicker').val('');
          $('#batch_id').val('default').selectpicker("refresh");
        }
   }



     // date popup
     $('#dateofbirth').parent().addClass('date')
    $('#dateofbirth').datetimepicker({
      format: 'YYYY-MM-DD'
     });



   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
      sqlUpdateQueryArray.length=0;
      sqlInsertQueryArray.length=0;
      db_table_update_values.length=0;
      db_table_insert_values.length=0;

      if(db_table_update_columns.length!=0) {
        var tempArray = [], 
            formValue = '', 
            formElementId = '', 
            formElementValue = '', 
            loopColumnsArrayLength =0;


        /************* get edit form values and push to array ***************/

        loopColumnsArrayLength = db_table_update_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
            tempArray.push(formElementValue); 
        }
        db_table_update_values.push(tempArray);



         /************* get add form values and push to array ***************/

        loopColumnsArrayLength = db_table_insert_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_insert_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
          tempArray.push(formElementValue);                    
        }
        db_table_insert_values.push(tempArray);
      }

      create_db_table_sql_update_query($('#' + formId + ' #id').val()); // For update query - db table row unique id as parameter
      create_db_table_sql_insert_query(); // For insert query 
      ajaxAction($('#action').val());  // parameter "both" executes both insert and update sql
      return false; // Will stop the submission of the form
    }
  });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>