<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Understand the Quran Academy</title>

<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css" media="all">
<link href="assets/css/jquery.bootgrid.css" rel="stylesheet" />
<link href="assets/css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap-select.min.css" type="text/css" />
<link rel="stylesheet" href="assets/css/jquery.timepicker.css" type="text/css" />
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="style.css" type="text/css" />


<script type="text/javascript" src="assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.bootgrid.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="assets/js/moment-with-locales.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="assets/js/typeahead.bundle.js"></script>
<script type="text/javascript" src="assets/js/jquery.timepicker.js"></script>
<script type="text/javascript" src="assets/js/jquery-ui.js"></script>
<script type="text/javascript" src="assets/js/uqa.common.js"></script>
<script type="text/javascript" src="assets/js/Chart.bundle.js"></script>
<script type="text/javascript" src="assets/js/utils.js"></script>
<script type="text/javascript" src="assets/js/FileSaver.js"></script>
<script type="text/javascript" src="assets/js/tableExport.js"></script>
<script type="text/javascript" src="assets/js/jquery.base64.js"></script>
<script type="text/javascript" src="assets/js/jspdf.min.js"></script>
<script type="text/javascript" src="assets/js/jspdf.plugin.autotable.min.js"></script>

