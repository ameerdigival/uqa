<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Student Details</h3>
  	</div> 

<!-- Add/Edit Form -->

<div id="edit_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo "<div class='form-sub-header'>Personal Information</div>";
            echo '<div class="row">';              
             $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

              $sqlResult = fetchRecordForDropdown("select id, name, city, phone from enquiry order by name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['id']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;ID: ' .  $json[$loop]['id'] . '  &#160;&#160;Phone: ' . $json[$loop]['phone'] . ' &#160;&#160;City: ' . $json[$loop]['city']);
              }              
              renderFormInput("name","Name",'data-validation="alphanumeric" data-validation-allowing=". "',"","col-md-4");  
              renderFormInput("dateofbirth","Date of birth",' placeholder="YYYY-MM-DD"',"","col-md-4");
              renderFormInput("qualification","Qualification",'',"","col-md-4");
            echo '</div>';
            echo '<div class="row">'; 
              renderFormRadio("gender","Gender",'',['Male', 'Female'],"","col-md-4");
              renderFormRadio("married","Married",'',['Yes', 'No'],"","col-md-4");            
            echo '</div>';

            echo "<br><div class='form-sub-header'>Contact Information</div>";
            echo '<div class="row">';
              renderFormInput("phone","Phone",'data-validation="required"',"","col-md-4");
              renderFormInput("whatsapp","Whatsapp",'',"","col-md-4");
              renderFormInput("email","Email",'data-validation="email" data-validation-error-msg="Enter a valid e-mail" data-validation-optional="true" placeholder="example@domain.com"',"","col-md-4");
            echo '</div>';
            echo "<br><div class='form-sub-header'>Address Information</div>";
            echo '<div class="row">';
              renderFormInput("city","City",'data-validation="required"',"","col-md-3");
              renderFormInput("area","Area",'',"","col-md-3"); 
              renderFormInput("country","Country",'',"","col-md-3");
              renderFormInput("pincode","Post / Zip code",'',"","col-md-3");                           
            echo '</div>';
            echo '<div class="row">';
              renderFormInput("address","Address",'',"","col-md-12");
            echo '</div>';

            echo "<br><div class='form-sub-header'>eLearning card Information</div>";
            echo '<div class="row">';
              renderFormInput("elc_cardnumber","ELC serial number",'',"","col-md-4");
              renderFormInput("enrolment_key","Enrolment key",'',"","col-md-4");
            echo '</div>';

            echo "<br><div class='form-sub-header'>Additional Information</div>";
            echo '<div class="row">';
              renderFormTextarea("comments","Comments",'',"","");
            echo '</div>';


            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","","");

              renderGridHeaderColumn("name","Name",true,"","","","","","");
              renderGridHeaderColumn("dateofbirth","Date of birth",false,"","","","","","");
              renderGridHeaderColumn("gender","Gender",false,"","","","","","");
              renderGridHeaderColumn("married","Married",false,"","","","","false","");
              renderGridHeaderColumn("qualification","Qualification",false,"","","","","false","");
              renderGridHeaderColumn("elc_cardnumber","ELC number",false,"","","","","","");
              renderGridHeaderColumn("enrolment_key","Enrolment key",false,"","","","","false","");
              renderGridHeaderColumn("phone","Phone",false,"","","","","","");
              renderGridHeaderColumn("whatsapp","Whatsapp",false,"","","","","","");
              renderGridHeaderColumn("address","Address",false,"","","","","false","");
              renderGridHeaderColumn("area","Area",false,"","","","","false","");
              renderGridHeaderColumn("city","City",true,"","","","","","");
              renderGridHeaderColumn("country","Country",false,"","","","","false","");
              renderGridHeaderColumn("pincode","Pincode",false,"","","","","false","");
              renderGridHeaderColumn("email","Email",false,"","","","","false","");
              renderGridHeaderColumn("comments","Comments",false,"","","","","false","");


              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>


<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>


<script>

function fetchEnquiryDetails() {

var enquiryDetails = [];

$("#name").autocomplete({
  source: enquiryDetails,
  select: function(event, ui){
    if(ui.item){
      $("#name").val(ui.item.name);
      $("#phone").val(ui.item.phone);
      $("#whatsapp").val(ui.item.whatsapp);
      $("#email").val(ui.item.email);
      $("#city").val(ui.item.city);
      $("#area").val(ui.item.area);
      $("#country").val(ui.item.country);
      $("#pincode").val(ui.item.pincode);
      $("#address").val(ui.item.address);
      // var strLabelSplit = (ui.item.label).split('(');
      // console.log(strLabelSplit[0])
      return ui.item.label;
    }
    else{
      $("#phone").val('');
      $("#whatsapp").val('');
      $("#email").val('');
      $("#city").val('');
      $("#area").val('');
      $("#country").val('');
      $("#pincode").val('');
      $("#address").val('');
    }
  },
  change: function(event, ui){
    if(ui.item){
      $("#name").val(ui.item.name);
      $("#phone").val(ui.item.phone);
      $("#whatsapp").val(ui.item.whatsapp);
      $("#email").val(ui.item.email);
      $("#city").val(ui.item.city);
      $("#area").val(ui.item.area);
      $("#country").val(ui.item.country);
      $("#pincode").val(ui.item.pincode);
      $("#address").val(ui.item.address);
    }
    else{
      $("#phone").val('');
      $("#whatsapp").val('');
      $("#email").val('');
      $("#city").val('');
      $("#area").val('');
      $("#country").val('');
      $("#pincode").val('');
      $("#address").val('');
    }
  }
});

sqlQuery = 'select * from enquiry where enquiry_category = "Course" order by name';
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                         responseLength = response.length;
                          for(loopResponse = 0; loopResponse < responseLength; loopResponse++)
                          {
                            enquiryDetails.push({"label": response[loopResponse]["name"] + ' (Phone: ' + response[loopResponse]["phone"] + ')',"phone": response[loopResponse]["phone"],"name": response[loopResponse]["name"],"whatsapp": response[loopResponse]["whatsapp"],"email": response[loopResponse]["email"],"address": response[loopResponse]["address"],"area": response[loopResponse]["area"],"city": response[loopResponse]["city"],"country": response[loopResponse]["country"],"pincode": response[loopResponse]["pincode"]});
                          }


                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });
}

  $(document).ready(function() 
  { 

    fetchEnquiryDetails();

    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["students"]]; // db table names for insert
    db_table_insert_columns = [["name","dateofbirth","gender","married","qualification","elc_cardnumber","enrolment_key","phone","whatsapp","address","area","city","country","pincode","email","comments"]]; // db table column names for insert

    db_table_names_update = [["students"]]; // db table names for update
    db_table_update_columns = [["name","dateofbirth","gender","married","qualification","elc_cardnumber","enrolment_key","phone","whatsapp","address","area","city","country","pincode","email","comments"]]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   


    // load data grid
    loadBootgrid(db_table_names_insert[0].toString());   

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal
            formReset('frm_edit');
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data              
              $('#' + formId+' #id').val($(this).data("row-id")); // in case we're changing the key
              // ajax call
              sqlQuery = 'select * from ' + db_table_names_update + ' where id=' + $(this).data("row-id");
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                          populateEditFormData(response);
                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });
              } else 
              {  //alert('No row selected! First select row, then click edit button');
              }
          }); 
    });




function populateEditFormData(dataArray) {
  // populate the data to the form
  formReset('frm_edit');
  var dataArrayLength = dataArray[0].length, formElementValue = '';

  if(dataArray.length > 0) 
  {
    
      loopColumnsArrayLength = db_table_update_columns[0].length;

        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = dataArray[0][formElementId];
          

          
            if($("#"+formElementId).attr('elementType') === "text") {
              $('#' + formElementId).val(formElementValue);
            }

            if($("#" + formElementId).attr('elementType') === "radio") {
              
              if(formElementValue!="") {
                $("input[name="+formElementId+"][value='" + formElementValue + "']").prop('checked', true); 
              }
              
            }

            if($("#"+formElementId).attr('elementType') === "select") {
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
              formElementValueSplit = formElementValue.split(",");
              
              if(formElementValueSplit.length>0) {
                for (loop = 0; loop<formElementValueSplit.length; loop++)
                {
                  $("#" + formElementId + " option[value='" + formElementValueSplit[loop] + "']").prop("selected",true);
                }
              }
              else {
                $("#"+formElementId+"[value=" + formElementValue + "]").prop("selected",true);
              }
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
            }

            if($("#" + formElementId).attr("elementType") === "textarea") {
              $('#' + formElementId).val(formElementValue);
            }
        }
  }

}

   /************** form interactions *************/

   $('#div_book_language').hide();
   $('#div_book_names').hide();
   $('#div_job_category').hide();

   // date popup
   $('#dateofbirth').parent().addClass('date')
    $('#dateofbirth').datetimepicker({
    format: 'YYYY-MM-DD'
   });


   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
      sqlUpdateQueryArray.length=0;
      sqlInsertQueryArray.length=0;
      db_table_update_values.length=0;
      db_table_insert_values.length=0;

      if(db_table_update_columns.length!=0) {
        var tempArray = [], 
            formValue = '', 
            formElementId = '', 
            formElementValue = '', 
            loopColumnsArrayLength =0;


        /************* get edit form values and push to array ***************/

        loopColumnsArrayLength = db_table_update_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
            tempArray.push(formElementValue); 
        }
        db_table_update_values.push(tempArray);



         /************* get add form values and push to array ***************/

        loopColumnsArrayLength = db_table_insert_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_insert_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
          tempArray.push(formElementValue);                    
        }
        db_table_insert_values.push(tempArray);
      }

      create_db_table_sql_update_query($('#' + formId + ' #id').val()); // For update query - db table row unique id as parameter
      create_db_table_sql_insert_query(); // For insert query 
      ajaxAction($('#action').val());  // parameter "both" executes both insert and update sql
      return false; // Will stop the submission of the form
    }
  });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>