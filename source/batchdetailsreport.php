<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Batch Details Report</h3>
  	</div>        
    <div class="col-sm-12">

      <div id="filterArea" class="form-row">
        <div class="row">
          <div class="col-md-3">
            <?php 
            $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

              $sqlResult = fetchRecordForDropdown("select batchdetails.*, courses.fullname, courses.shortname, courses.id as courseid, location.name as locationname, employees.name as employeename from batchdetails left join courses on courses.id = batchdetails.course_id left join location on location.id = batchdetails.location_id left join employees on employees.id = batchdetails.trainer_employee_id where batchdetails.status <> 'Completed' order by batchdetails.start_date desc");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['batch_id'] . '|' . $json[$loop]['start_date'] . '##$$' . $json[$loop]['end_date'] . '##$$' . $json[$loop]['fullname'] . '##$$' . $json[$loop]['shortname'] . '##$$' . $json[$loop]['locationname'] . '##$$' . $json[$loop]['start_time'] . '##$$' . $json[$loop]['end_time'] . '##$$' . $json[$loop]['days'] . '##$$' . $json[$loop]['employeename']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;Course: ' .  $json[$loop]['fullname'] . '  &#160;&#160;Fees: ' . $json[$loop]['fees'] . '  &#160;&#160;Batch Date: ' . $json[$loop]['start_date'] . ' to ' . $json[$loop]['end_date'] . '  &#160;&#160;status: ' . $json[$loop]['status']);
              }              

              renderFormSelect("batch_id","Batch name",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,""); 
            ?>
          </div>
          
          <div class="pull-right" style='padding-bottom:10px; padding-top: 5px;'>
            <button type="button" class="btn btn-xs btn-primary" id="btn-pdf" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> PDF</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-xls" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> Excel</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-print" data-row-id="0">
            <span class="glyphicon glyphicon-print"></span> Print</button>
          </div>

          <div class="form-group col-md-4">
          </div>

          <div class="form-group col-md-4">
          </div>
        </div>            
      </div>
      
      <div id="printArea" class="row">

       
      </div>
     

    </div>
  </div>
</div>


<?php include_once("footer.php"); ?>
</div>

<script>
$(document).ready(function() 
{ 

var db_table_name = [] , db_table_columns = [], table_header = [], totalCourseFees = 0, totalBooksFees = 0, expenses = 0 , profit = 0;

db_table_name = ["batchaction"]; // db table names 
db_table_where_colummn = []; // db table names
db_table_columns = ["id", "studentname", "phone", "course_fees", "books_fees", "eLC", "discount", "dueamount", "status"]; // db table column
table_header = ["Sl.No.","Student", "Phone", "Course fees", "Books fees", "eLC", "Discount", "Due", "Status"]; // db table column

$("#batch_id").prop("selectedIndex", 1);
$("#batch_id").selectpicker('refresh');
loadAjaxCall();

$("#batch_id").change(function () {
  $("#printArea").html("");
  loadAjaxCall();
});


function loadAjaxCall() {
  var batch_id = $("#batch_id option:selected").val();
   if(batch_id != '') {
      var sqlQuery = "SELECT students.name as studentname, students.phone, studentenrolment.course_fees, studentenrolment.books_fees, students.elc_cardnumber as eLC, studentenrolment.discount, (studentenrolment.course_fees+studentenrolment.books_fees-studentenrolment.discount)-(SELECT IFNULL(sum(amount_received),0) FROM incomeexpense where incomeexpense.student_enrolment_id = studentenrolment.id) as dueamount, studentenrolment.status FROM studentenrolment left join students on students.id = studentenrolment.student_id where studentenrolment.batch_id = '" + batch_id + "' order by students.name";
      
      
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                  populateTable(response);

              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });

      
   }
}

function populateTable(response) 
{ 
var htmlTableData = '', batchData = '';
batchData = $("#batch_id option:selected").attr("data-value"); 
batchDataSplitted = batchData.split("##$$"); 
        htmlTableData += '<table class="reportHeader"><tbody><tr><td><b>Batch Details</b></td></tr></tbody></table>' +
                         '<center><div class="table-responsive"><table id="tableHeaderData" class="table table-bordered table-responsive" style="width:auto"><thead><tr><th>Batch Name</th><th>Course</th><th>Date</th><th>Time</th><th>Days</th><th>Location</th><th>Trainer</th></tr></thead>' + 
                          '<tbody><tr><td>' + $("#batch_id option:selected").text() + '</td>' +
                          '<td>' + batchDataSplitted[2] + ' (' + batchDataSplitted[3] + ')</td>' +
                          '<td>' + batchDataSplitted[0] + ' to ' + batchDataSplitted[1] + '</td>' +
                          '<td>' + batchDataSplitted[5] + ' - ' + batchDataSplitted[6] + '</td>' +
                          '<td>' + batchDataSplitted[7] + '</td>' + 
                          '<td>' + batchDataSplitted[4] + '</td>' +
                          '<td>' + batchDataSplitted[8] + '</td></tr>' +
                          '</tbody>' +
                        '</table></div></center>' +
                          '<div class="table-responsive"><table id="tableData" class="table table-bordered table-responsive">' + 
                          '<thead>' +
                          '<tr>';
        var tableHeaderLength = table_header.length;
        for(loopColumns=0;loopColumns<tableHeaderLength;loopColumns++) 
        {
          
              htmlTableData += '<th>' + table_header[loopColumns] + '</th>'              
           
        }
        htmlTableData += '</tr>' +
                          '</thead>';

        htmlTableData += '<tbody>' 
                          
        var responseLength = response.length;
        totalCourseFees = 0, totalBooksFees = 0;
        for(loopColumns=0;loopColumns<responseLength;loopColumns++) 
        {          
              htmlTableData += '<tr>'
                  var db_table_columns_Length = db_table_columns.length;
                  for(loopHeaders=0; loopHeaders<db_table_columns_Length; loopHeaders++)  {
                    if(loopHeaders==0) {
                      htmlTableData += '<td>' + parseInt(loopColumns+1) + '</td>';
                    }
                    else {
                      var rowContent = response[loopColumns][db_table_columns[loopHeaders]];
                      if(rowContent != null)
                      {
                        htmlTableData += '<td>' + response[loopColumns][db_table_columns[loopHeaders]] + '</td>';
                      }
                      else {
                        htmlTableData += '<td>&#160;</td>';
                      }
                    }

                    if(db_table_columns[loopHeaders] == "course_fees") {
                      totalCourseFees += parseInt(response[loopColumns][db_table_columns[loopHeaders]]);
                    }

                    if(db_table_columns[loopHeaders] == "books_fees") {
                      totalBooksFees += parseInt(response[loopColumns][db_table_columns[loopHeaders]]);
                    }
                    
                  }

               htmlTableData += '</tr>'
        }
        htmlTableData += '</tbody></table></div>';        
        console.log(htmlTableData);
        $("#printArea").html(htmlTableData);
        loadAjaxCall_incomeexpense();
}

function loadAjaxCall_incomeexpense() {
  var htmlTableData = '';
  var batch_id = $("#batch_id option:selected").val();
   if(batch_id != '') {
      var sqlQuery = 'SELECT amount_paid, description FROM incomeexpense where type = "Expense" and batch_id = "' + batch_id + '" order by amount_paid desc';
      
      
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                  htmlTableData += '<div class="table-responsive"><table id="tableHeaderData" class="table table-bordered table-responsive" style="width:auto">' + 
                          '<thead>' +
                          '<tr><th>Expense description</th><th>Expense</th></tr>';
                          expenses = 0;
                    for(loopColumns=0;loopColumns<response.length;loopColumns++) 
                    {
                      
                          htmlTableData += '<tr><td>' + response[loopColumns]["description"] + '</td><td>' + response[loopColumns]["amount_paid"] + '</td></tr>'  

                          expenses += parseInt(response[loopColumns]["amount_paid"]);        
                       
                    }
                    htmlTableData += '</tbody>' +
                                      '</table></div>';
                    var htmlTableFooterData = '';
                    profit = (totalCourseFees+totalBooksFees)-expenses;
                    htmlTableFooterData = '<b>Total Course Fees: </b>' + totalCourseFees + '&nbsp;&nbsp;&nbsp;&nbsp;   <b>Total Books Fees: </b>' + totalBooksFees + '&nbsp;&nbsp;&nbsp;&nbsp;   <b>Expenses: </b>' + expenses + '&nbsp;&nbsp;&nbsp;&nbsp;   <b>Profit: </b>' + profit + '<br><br>';
                    $("#printArea").append(htmlTableFooterData);
                    $("#printArea").append(htmlTableData);
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });

      
   }
}

});

 $( "#btn-xls" ).click(function() {
      $('#printArea').tableExport({type:'excel'}); 
 }); 

 $( "#btn-print" ).click(function() {
      window.print(); 
 });

 $( "#btn-pdf" ).click(function() {
      pdfExport("printArea");
 });

</script>
</body>
</html>

<?php ob_end_flush(); ?>