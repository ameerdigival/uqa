<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Inventory Details Report</h3>
  	</div>        
    <div class="col-sm-12">

      <div id="filterArea" class="form-row">
        <div class="row">
          <div class="col-md-1">
            <label for="name" class="control-label">Category</label>
            <select id="selectCategory" class="form-control"><option value="">All</option><option value="Books">Books</option><option value="Office">Office</option></select>
          </div>
          <div class="pull-right" style='padding-bottom:10px; padding-top: 5px;'>
            <button type="button" class="btn btn-xs btn-primary" id="btn-pdf" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> PDF</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-xls" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> Excel</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-print" data-row-id="0">
            <span class="glyphicon glyphicon-print"></span> Print</button>
          </div>

          <div class="form-group col-md-4">
          </div>

          <div class="form-group col-md-4">
          </div>
        </div>            
      </div>
      
      <div id="printArea" class="row">

       
      </div>
     

    </div>
  </div>
</div>


<?php include_once("footer.php"); ?>
</div>

<script>
$(document).ready(function() 
{ 

var db_table_name = [] , db_table_columns = [], table_header = [];

db_table_name = ["inventory"]; // db table names 
db_table_where_colummn = []; // db table names
db_table_columns = ["id","category","name","quantity","unit_price","discount","extra_charges","total_amount","comments"]; // db table column
table_header = ["Sl.No.","Category","Name","Quantity","Unit price","Discount","Extra charges","Total amount","Comments"]; // db table column

loadAjaxCall();

$("#selectCategory").change(function () {
  $("#printArea").html("");
  loadAjaxCall();
});

function loadAjaxCall() {
  var category = $("#selectCategory option:selected").val();
      var sqlQuery = "select * from " + db_table_name[0]
        if(category != '') {
          sqlQuery += " where category = '" +  category + "'"
        }
      
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                
                  populateTable(response);
               
                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}

function populateTable(response) 
{ 
  console.log(response);
var htmlTableData = '';                  
          
        htmlTableData += '<table class="reportHeader"><tbody><tr><td><b>Inventory Details</b></td></tr></tbody></table>' + 
                          '<div class="table-responsive"><table id="tableData" class="table table-bordered table-responsive">' + 
                          '<thead>' +
                          '<tr>';
        var tableHeaderLength = table_header.length;
        for(loopColumns=0;loopColumns<tableHeaderLength;loopColumns++) 
        {
          
              htmlTableData += '<th width=100>' + table_header[loopColumns] + '</th>'              
           
        }
        htmlTableData += '</tr>' +
                          '</thead>';

        htmlTableData += '<tbody>' 
                          
        var responseLength = response.length;
        for(loopColumns=0;loopColumns<responseLength;loopColumns++) 
        {          
              htmlTableData += '<tr>'
                  var db_table_columns_Length = db_table_columns.length;
                  for(loopHeaders=0; loopHeaders<db_table_columns_Length; loopHeaders++)  {
                    if(loopHeaders==0) {
                      htmlTableData += '<td>' + parseInt(loopColumns+1) + '</td>';
                    }
                    else {
                      htmlTableData += '<td>' + response[loopColumns][db_table_columns[loopHeaders]] + '</td>';
                    }
                  }

               htmlTableData += '</tr>'
        }
        htmlTableData += '</tbody></table></div><br><br>';
        console.log(htmlTableData)
        $("#printArea").html(htmlTableData);
}

});



 $( "#btn-xls" ).click(function() {
      $('#printArea').tableExport({type:'excel'}); 
 }); 

 $( "#btn-print" ).click(function() {
      window.print(); 
 });

 $( "#btn-pdf" ).click(function() {
      pdfExport("printArea");
 });

</script>
</body>
</html>

<?php ob_end_flush(); ?>