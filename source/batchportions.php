<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>


<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Batch Portions</h3>
  	</div>   

    <!---- Add/Edit Form -->

<div id="edit_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo '<div class="row">';

            $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

              $sqlResult = fetchRecordForDropdown("select id, batch_id, name, start_date, end_date, status from batchdetails order by name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['batch_id']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;ID: ' .  $json[$loop]['id'] . '  &#160;&#160;Batch Date: ' . $json[$loop]['start_date'] . ' to ' . $json[$loop]['end_date'] . '  &#160;&#160;status: ' . $json[$loop]['status']);
              }              

              renderFormSelect("batch_id","Batch",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-4");  

                          echo '</div>';

            echo '<div id="divPortions" class="row">'; 
            echo '</div>';
 

            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            
            </div>
          </form>
      </div>
    </div>
  </div>
</div> 
    
    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","","");

              renderGridHeaderColumn("batchname","Batch",true,"","","","","true","");
              renderGridHeaderColumn("batch_id","Batch id",false,"","","","","false","");
              renderGridHeaderColumn("batch_date","Date",true,"","","","","true","");
              renderGridHeaderColumn("portions_targeted","Portions targeted",false,"","","","","true","");
              renderGridHeaderColumn("portions_completed","Portions completed",false,"","","","","true","");
              renderGridHeaderColumn("comments","Comments",false,"","","","","true","");

              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>



<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>
<script>
var sqlQuery = '', portionsRowCount = 0, recordExist = false;

function verifyBatchDatesForBatch(rowid) {
  
  sqlQuery = 'select batch_date, portions_targeted, portions_completed, comments from batchportions';

  if(rowid != '') {
    sqlQuery += ' where id = "' + rowid + '"';
  }
  else {
    if($("#batch_id option:selected").val() != '') 
    {
    sqlQuery += ' where batch_id = "' + $("#batch_id option:selected").val() + '"';
    }
  }

  console.log(sqlQuery);
  data = {
  action:"fetch",
  sqlQuery: sqlQuery
  };
  $.ajax({
          type: "POST",  
          url: "response.php",  
          data: data,
          dataType: "json",       
          success: function(response)  
          { 
            console.log(response)
            if(response.status!=-1) 
            {
              var responseLength = response.length;
                if(responseLength > 0) {
                    recordExist = true;
                    console.log(recordExist)
                    showPortionsList(response, responseLength)
                }
                else {
                  recordExist = false;
                   $("#divPortions").html("No record(s) found");
                }
            }
            else 
            {
              $("#divPortions").html(response.message);
            }
          },
          error: function(req, status, error)  
          {
            $("#divPortions").html("Error: \n"+status+"\n"+error);
          } 
          });
}
  
function showPortionsList(response, responseLength) {
 var htmlAtendance = '', chkBoxChecked = "";
 portionsRowCount = responseLength;
 htmlAtendance += '<br><table class="table">';
 htmlAtendance += '<tr><th style="border:0;width : 60px">Dates</th><th style="border:0;">Portions Targeted</th><th style="border:0;">Portions Completed</th><th style="border:0;">Comments</th></tr>';
  for(loopResponse = 0; loopResponse < responseLength; loopResponse++)
  {

    htmlAtendance += '<tr>' +
                     '<td id="data-row-' + loopResponse + '" width=50 style="vertical-align: middle;">' +
                      response[loopResponse]["batch_date"] +
                     '</td>' +
                      '<td>' +
                      '<input type="text" class="form-control" id="portions_targeted'+loopResponse + '" name="portions_targeted'+loopResponse + '" value="' + response[loopResponse]["portions_targeted"] + '"/>' +
                      '</td>' +
                      '<td>' +
                      '<input type="text" class="form-control" id="portions_completed'+loopResponse + '" name="portions_completed'+loopResponse + '" value="' + response[loopResponse]["portions_completed"] + '"/>' +
                      '</td>' +
                      '<td>' +
                      '<input type="text" class="form-control" id="comments'+loopResponse + '" name="comments'+loopResponse + '" value="' + response[loopResponse]["comments"] + '"/>' 
                      '</td>' +
                      '</tr>';
  }
  console.log(portionsRowCount)
  htmlAtendance += '</table><br><br>';
  $("#divPortions").html(htmlAtendance);
}


   

  $(document).ready(function() 
  { 
      
    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["batchportions"]]; // db table names for insert
    db_table_insert_columns = []; // db table column names for insert

    db_table_names_update = [["batchportions"]]; // db table names for update
    db_table_update_columns = ["portions_targeted","portions_completed","comments"]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   


    // load data grid
    loadBootgrid('batchportions_view');   

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal
            formReset('frm_edit');
            $("#divPortions").html('');
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data
              
              var ele =$(this).parent();
              var batchDate = ele.siblings(':nth-of-type(3)').text().trim();
              populateEditFormData($(this).data("row-id"))
      
            } else 
            {  //alert('No row selected! First select row, then click edit button');
            }
          }); 
    });




function populateEditFormData(rowid) {
  // populate the data to the form
  formReset('frm_edit');
  $("#divPortions").html('');
    if(rowid !='') {      
      verifyBatchDatesForBatch(rowid);
      fetchBatchIdandDate(rowid);
    }
}


function fetchBatchIdandDate(id) {
  
  sqlQuery = 'select batch_id, batch_date from batchportions where id = "' + id + '"';

  console.log(sqlQuery);
  data = {
  action:"fetch",
  sqlQuery: sqlQuery
  };
  $.ajax({
          type: "POST",  
          url: "response.php",  
          data: data,
          dataType: "json",       
          success: function(response)  
          { 
            if(response.status!=-1) 
            {
              var responseLength = response.length;
                if(responseLength > 0) {
                    $('#batch_id option[value="'+response[0]["batch_id"]+'"]').prop('selected', true);
                    $('.selectpicker').selectpicker('refresh');
                }
                else {
                }
            }
            else 
            {
              $("#divAttendance").html(response.message);
            }
          },
          error: function(req, status, error)  
          {
            $("#divAttendance").html("Error: \n"+status+"\n"+error);
          } 
          });
}

   /************** form interactions *************/


   // date popup

   $('#batchDate').parent().addClass('date')
    $('#batchDate').datetimepicker({
    format: 'YYYY-MM-DD'
   });

    $('#batch_id').change(function () {
      console.log( $('#batch_id option:selected').val())
      verifyBatchDatesForBatch('');
    });
    

$( "#command-add" ).click(function() {
    $("#divPortions").html('');
  });

   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
        sqlUpdateQueryArray.length=0;
        sqlInsertQueryArray.length=0;
        db_table_update_values.length=0;
        db_table_insert_values.length=0;

        if(portionsRowCount > 0) 
        {
          var chkBoxAttended = "";
          for (loopPortions = 0; loopPortions < portionsRowCount; loopPortions++)
          {      

             tempQuery = 'UPDATE ' + db_table_names_update + ' SET portions_targeted = "' + $("#portions_targeted" + loopPortions).val() + '", portions_completed = "' +
              $("#portions_completed" + loopPortions).val() + '", comments = "' +
              $("#comments" + loopPortions).val() +
              '" where batch_id = "' + $("#batch_id").val() + '" AND batch_date = "' + $("#data-row-" + loopPortions).html() + '"';
             sqlUpdateQueryArray.push(tempQuery);
            console.log(tempQuery); 
          }
          $("#divPortions").html('');

              ajaxAction('edit');
        }

        
        return false; // Will stop the submission of the form
      }
    });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>