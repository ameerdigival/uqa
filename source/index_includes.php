<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?> 
</head>
<?php	
	$error = false;
	$usernameError = "";
	$passError = "";
	$username ="";
	if( isset($_POST['btn-login']) ) {	
		
		// prevent sql injections/ clear user invalid inputs
		$username = trim($_POST['username']);
		$username = strip_tags($username);
		$username = htmlspecialchars($username);
		
		$pass = trim($_POST['pass']);
		$pass = strip_tags($pass);
		$pass = htmlspecialchars($pass);
		// prevent sql injections / clear user invalid inputs
		
		if(empty($username)){
			$error = true;
			$usernameError = "Enter username.";
		} 
		
		if(empty($pass)){
			$error = true;
			$passError = "Enter password.";
		}
		
		// if there's no error, continue to login
		if (!$error) {
			
			//$pass = hash('sha256', $pass); // password hashing using SHA256
			$sql = "SELECT id, username, password, is_admin, active FROM users WHERE username='$username'";
			$res=mysqli_query($connString, $sql) or die("error to read user data");
			$row = mysqli_fetch_assoc($res);
			$count = mysqli_num_rows($res); // if uname/pass correct it returns must be 1 row

			if( $count == 1 && $row['password']==$pass ) {
				$_SESSION['user'] = $row['id'];		
				$_SESSION['is_admin'] = $row['is_admin'];	
				$_SESSION['active'] = $row['active'];	
				echo($row['active']);
				if($row['is_admin']=="No" && $row['active']=="Yes") {
					header("Location: dashboard.php");
				}
				if($row['is_admin']=="Yes" && $row['active']=="Yes") {
					header("Location: dashboard.php");
				}
				if($row['active']!="Yes") {
					$errMSG = "User is not yet activated... Contact Admin";
				}
			} else {
				$errMSG = "Incorrect Credentials, Try again...";
			}
				
		}
		
	}
?>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
                <a class="navbar-brand" href="#"><img class="navbar-logo-img" src="assets/images/understand-quran-logo.png" /></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Login</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			  <span class="glyphicon glyphicon-user"></span>&nbsp; Welcome Guest &nbsp;<span class="caret"></span></a>
             
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav> 

    	<div id="wrapper">
<div class="container">

	<div id="login-form">
    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" autocomplete="off">
    
    	<div class="col-md-12">
        
        	<div class="form-group">
            	<h2 class="">User Login</h2>
            </div>
        
        	<div class="form-group">
            	<hr />
            </div>
            
            <?php
			if ( isset($errMSG) ) {
				
				?>
				<div class="form-group">
            	<div class="alert alert-danger">
				<span class="glyphicon glyphicon-info-sign"></span> <?php echo $errMSG; ?>
                </div>
            	</div>
                <?php
			}
			?>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
            	<input type="username" name="username" class="form-control" placeholder="UserName" value="<?php echo $username; ?>" maxlength="60" />
                </div>
                <span class="text-danger"><?php echo $usernameError; ?></span>
            </div>
            
            <div class="form-group">
            	<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
            	<input type="password" name="pass" class="form-control" placeholder="Password" maxlength="15" />
                </div>
                <span class="text-danger"><?php echo $passError; ?></span>
            </div>
            
            <div class="form-group">
            	<hr />
            </div>
            
            <div class="form-group">
            	<button type="submit" class="btn btn-block btn-primary" name="btn-login">Submit</button>
            </div>
            
            <div class="form-group">
            	<hr />
            </div>



        
        </div>
   
    </form>


    </div>	

</div>
<?php include_once("footer.php"); ?>

</div>
</body>
</html>
<?php ob_end_flush(); ?>