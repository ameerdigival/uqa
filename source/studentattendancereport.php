<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
  <div class="container">    
    <div class="page-header">
      <h3>Student Attendance Report</h3>
    </div>        
    <div class="col-sm-12">

      <div id="filterArea" class="form-row"> 
        <div class="row">
          <div class="col-md-3">
            <?php 
            $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

              $sqlResult = fetchRecordForDropdown("select batchdetails.*, courses.fullname, courses.shortname, courses.id as courseid, location.name as locationname, employees.name as employeename from batchdetails left join courses on courses.id = batchdetails.course_id left join location on location.id = batchdetails.location_id left join employees on employees.id = batchdetails.trainer_employee_id where batchdetails.status <> 'Completed' order by batchdetails.start_date desc");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['batch_id'] . '|' . $json[$loop]['start_date'] . '##$$' . $json[$loop]['end_date'] . '##$$' . $json[$loop]['fullname'] . '##$$' . $json[$loop]['shortname'] . '##$$' . $json[$loop]['locationname'] . '##$$' . $json[$loop]['start_time'] . '##$$' . $json[$loop]['end_time'] . '##$$' . $json[$loop]['days'] . '##$$' . $json[$loop]['employeename']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;Course: ' .  $json[$loop]['fullname'] . '  &#160;&#160;Fees: ' . $json[$loop]['fees'] . '  &#160;&#160;Batch Date: ' . $json[$loop]['start_date'] . ' to ' . $json[$loop]['end_date'] . '  &#160;&#160;status: ' . $json[$loop]['status']);
              }              

              renderFormSelect("batch_id","Batch name",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,""); 
            ?>
          </div>          
          <div class="pull-right" style='padding-bottom:10px; padding-top: 5px;'>
            <button type="button" class="btn btn-xs btn-primary" id="btn-pdf" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> PDF</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-xls" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> Excel</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-print" data-row-id="0">
            <span class="glyphicon glyphicon-print"></span> Print</button>
          </div>

          <div class="form-group col-md-4">
            <span class="text-danger"></span>
          </div>

          <div class="row">
          <div class="form-group col-md-4">
          </div>
        </div>            
      </div>
      
      <div id="printArea" class="row">

       
      </div>
     

    </div>
  </div>
</div>


<?php include_once("footer.php"); ?>
</div>

<script>
$(document).ready(function() 
{ 


var db_table_name = [] , db_table_columns = [], table_header = [];

db_table_name = ["batchdetails"]; // db table names 
db_table_where_colummn = []; // db table names
db_table_columns = []; // db table column
table_header = []; // db table column


$("#batch_id").prop("selectedIndex", 1);
$("#batch_id").selectpicker('refresh');
loadBatchDates();

$("#batch_id").change(function () {
  $("#printArea").html("");
  loadBatchDates();
});

function loadBatchDates() {
  var batch_id = $("#batch_id option:selected").val();

      var sqlQuery = "SELECT batch_date, portions_targeted, portions_completed from batchportions where batch_id = '" + batch_id + "'";
      console.log(sqlQuery);
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                
                  loadAjaxCall(response, batch_id);                
                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}


var employee_id = [], attendance_date = [], attended = [], filteredDate = [], portionsTargeted = [], portionsCompleted = [];
function loadAjaxCall(batchDates, batch_id) {
filteredDate = [], table_header = [], portionsTargeted = [], portionsCompleted = [];
  for(loopBatchDates=0;loopBatchDates<batchDates.length;loopBatchDates++) 
    {
      var day = 1000*60*60*24;
      batchDate = new Date(batchDates[loopBatchDates]["batch_date"]);

      var batchDateObject = new Date(batchDate);

      filteredDate.push(batchDateObject.getFullYear() + '-' + (batchDateObject.getMonth()+1) + '-' + batchDateObject.getDate());
      portionsTargeted.push(batchDates[loopBatchDates]["portions_targeted"]);
      portionsCompleted.push(batchDates[loopBatchDates]["portions_completed"]);
      table_header.push(batchDateObject.getDate() + " " + monthShortNames[(batchDateObject.getMonth())]  + " " + batchDateObject.getFullYear() + ' <br><span style="color:#777">' + weekDayFullNames[batchDateObject.getDay()] + '</span>');
    }
   

  var batch_id = $("#batch_id").val();


      var sqlQuery = "SELECT * from studentattendance where batch_id ='" + batch_id + "'"

      console.log(sqlQuery)
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {                  

                  loadStudents(response, batch_id);                                
                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}


function loadStudents(attendanceData, batch_id) {
      var sqlQuery = "SELECT studentenrolment.student_id as studentid, name, (studentenrolment.course_fees+studentenrolment.books_fees-studentenrolment.discount)-(SELECT IFNULL(sum(amount_received),0) FROM incomeexpense where incomeexpense.student_enrolment_id = studentenrolment.id) as dueamount from studentenrolment left join students on studentenrolment.student_id = students.id where batch_id ='" + batch_id + "' order by students.name"
      console.log(sqlQuery);
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                
                  populateTable(response, attendanceData, batch_id);                
                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}

function populateTable(response, attendanceData, batch_id) 
{ 

var htmlTableData = '', dates = '', headingText = '';

  $(".text-danger").html("");

batchData = $("#batch_id option:selected").attr("data-value"); 
batchDataSplitted = batchData.split("##$$"); 

        htmlTableData = '<table class="reportHeader"><tbody><tr><td><b>Student Attendance - ' + $("#batch_id option:selected").text() + '</b></td></tr></tbody></table>' +
        '<center><div class="table-responsive"><table id="tableHeaderData" class="table table-bordered table-responsive" style="width:auto"><thead><tr><th>Batch Name</th><th>Course</th><th>Date</th><th>Time</th><th>Days</th><th>Location</th><th>Trainer</th></tr></thead>' + 
                          '<tbody><tr><td>' + $("#batch_id option:selected").text() + '</td>' +
                          '<td>' + batchDataSplitted[2] + ' (' + batchDataSplitted[3] + ')</td>' +
                          '<td>' + batchDataSplitted[0] + ' to ' + batchDataSplitted[1] + '</td>' +
                          '<td>' + batchDataSplitted[5] + ' - ' + batchDataSplitted[6] + '</td>' +
                          '<td>' + batchDataSplitted[7] + '</td>' + 
                          '<td>' + batchDataSplitted[4] + '</td>' +
                          '<td>' + batchDataSplitted[8] + '</td></tr>' +
                          '</tbody>' +
                        '</table></div></center>' +
                          '<div class="table-responsive"><table id="tableData" class="table table-bordered">' + 
                          '<thead>' +
                          '<tr>' +
                          '<th style="width:40px !important">Sl.No.</th>' + 
                          '<th style="width:150px !important">Student</th>' +
                          '<th style="width:40px !important">Fees Due</th>';
        var tableHeaderLength = table_header.length;
        for(loopColumns=0;loopColumns<tableHeaderLength;loopColumns++) 
        {
          
              htmlTableData += '<th style="width:50px !important">' + table_header[loopColumns] + '</th>'           
           
        }
        htmlTableData += '</tr>' +
                          '</thead>';

        htmlTableData += '<tbody>';
            
        var responseLength = response.length, attendanceDetails = '';
        for(loopColumns=0;loopColumns<responseLength;loopColumns++) 
        {          
              htmlTableData += '<tr>' +
                                '<td>' + (loopColumns+1) + '</td>' +
                                '<td>'+response[loopColumns]["name"]+'</td>' +
                                '<td>'+response[loopColumns]["dueamount"]+'</td>'

                        var filteredDateLength = filteredDate.length;
                        for(loopFilteredDate=0;loopFilteredDate<filteredDateLength;loopFilteredDate++) 
                        {
                              attendanceStatus = '';
                               for(loopAttendance=0;loopAttendance<attendanceData.length;loopAttendance++) 
                                {
                                   
                                      if(attendanceData[loopAttendance]["attendance_date"].replace(/-0/g, "-") == filteredDate[loopFilteredDate] && response[loopColumns]["id"]==attendanceData[loopAttendance]["studentid"]) {
                                         attendanceDetails = attendanceData[loopAttendance]["attended"];
                                      }
                                                                      
                                }
                                if(attendanceDetails == 'Yes') {
                                  attendanceDetails = "P";
                                }
                                if(attendanceDetails == 'No') {
                                  attendanceDetails = "A";
                                }
                                htmlTableData += '<td>' + attendanceDetails + '</td>'
                                attendanceDetails = '';
                           
                        }


               htmlTableData += '</tr>'
        }

        htmlTableData += '<div class="table-responsive"><table id="tableData" class="table table-bordered">' + 
                          '<thead>' +
                          '<tr>' +
                          '<th style="width:40px !important"></th>';
        var tableHeaderLength = table_header.length;
        for(loopColumns=0;loopColumns<tableHeaderLength;loopColumns++) 
        {
          
              htmlTableData += '<th style="width:50px !important">' + table_header[loopColumns] + '</th>'           
           
        }
        htmlTableData += '</tr>' +
                          '</thead>';

        htmlTableData += '<tbody>';

              var filteredDateLength = filteredDate.length;
                        for(loopFilteredDate=0;loopFilteredDate<filteredDateLength;loopFilteredDate++) 
                        {
                          htmlTableData += '<td>&#160;</td>'
                        }
              htmlTableData += '</tr>'

              htmlTableData += '<tr>' +
                                '<td><b>Portions<br>Targeted</b></td>'
                                
                        for(loopPortions=0; loopPortions < portionsTargeted.length;loopPortions++) 
                                {
                                   
                                     htmlTableData += '<td>' + portionsTargeted[loopPortions] + '</td>'
                                                                      
                                } 

               htmlTableData += '</tr>'

               htmlTableData += '<tr>' +
                                '<td><b>Portions<br>Completed</b></td>'
                                
                        for(loopPortions=0; loopPortions < portionsCompleted.length;loopPortions++) 
                                {
                                   
                                     htmlTableData += '<td>' + portionsCompleted[loopPortions] + '</td>'
                                                                      
                                } 

               htmlTableData += '</tr>'


        htmlTableData += '</tbody></table></div><br><br>';
        htmlTableData += '<b>Trainer:</b> ' + batchDataSplitted[8] + '<br><b>Signature:</b><br><br>'
        //console.log(htmlTableData)
        $("#printArea").html(htmlTableData);
        htmlTableData = '';
}

});



 $( "#btn-xls" ).click(function() {
      $('#printArea').tableExport({type:'excel'}); 
 }); 

 $( "#btn-print" ).click(function() {
      window.print(); 
 });

 $( "#btn-pdf" ).click(function() {
      pdfExport("printArea");
 });

</script>
</body>
</html>

<?php ob_end_flush(); ?>