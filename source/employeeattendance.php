<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>


<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Employee Attendance</h3>
  	</div>   

    <!---- Add/Edit Form -->

<div id="edit_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo '<div class="row">';
            renderFormInput("attendancedate","Attendance Date",'data-validation="required" placeholder="YYYY-MM-DD"',"","col-md-3");              
            echo '</div>';

            echo '<div id="divAttendance" class="row">'; 
            echo '</div>';
 

            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            
            </div>
          </form>
      </div>
    </div>
  </div>
</div> 
    
    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","","");

              renderGridHeaderColumn("name","Employee",true,"","","","","true","");
              renderGridHeaderColumn("employee_id","Employee id",true,"","","","","false","");
              renderGridHeaderColumn("attendance_date","Attendance date",true,"","","","","true","");
              renderGridHeaderColumn("attended","Attendance",true,"","","","","true","");

              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>



<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>
<script>
var sqlQuery = '', attendanceRowCount = 0, recordExist = false;

function verifyAttendanceForDate(attendanceId) {
  
  sqlQuery = 'select employeeattendance.employee_id as id, employees.name, employeeattendance.attended from employeeattendance join employees on employees.id = employeeattendance.employee_id where employeeattendance.attendance_date = "' + $("#attendancedate").val() + '"';

  if (attendanceId != '') {
    sqlQuery += ' AND employeeattendance.id="' + attendanceId + '"';
  }

  console.log(sqlQuery);
  data = {
  action:"fetch",
  sqlQuery: sqlQuery
  };
  $.ajax({
          type: "POST",  
          url: "response.php",  
          data: data,
          dataType: "json",       
          success: function(response)  
          { 
            if(response.status!=-1) 
            {
              var responseLength = response.length;
                if(responseLength > 0) {
                    recordExist = true;
                    showAttendanceList(response, responseLength)
                }
                else {
                  recordExist = false;
                  getStudentDataforAttendance()
                }
            }
            else 
            {
              $("#divAttendance").html(response.message);
            }
          },
          error: function(req, status, error)  
          {
            $("#divAttendance").html("Error: \n"+status+"\n"+error);
          } 
          });
}
  
function showAttendanceList(response, responseLength) {
 var htmlAtendance = '', selectedOption = "";
 attendanceRowCount = responseLength;
 var selectAttendedArray = ["Present","Absent","Halfday Compensatory","Compensatory Leave","Halfday Leave","Casual Leave"]
 htmlAtendance += '<br><table class="table">';
 htmlAtendance += '<tr><th style="border:0;width : 60px">Emp Id</th><th style="border:0;">Name</th><th style="border:0;">Attendance</th></tr>';
  for(loopResponse = 0; loopResponse < responseLength; loopResponse++)
  {
    if(response[loopResponse]["attended"] == "Yes") {
        chkBoxChecked = "checked";
    }
    else {
      chkBoxChecked = "";
    }
    htmlAtendance += '<tr>' +
                     '<td id="data-row-' + loopResponse + '" width=50 style="vertical-align: middle;">' +
                      response[loopResponse]["id"] +
                     '</td>' +
                     '<td width=200 style="vertical-align: middle;">' +
                     '</div>' + response[loopResponse]["name"] +
                     '</td>' +
                      '<td>' +
                      '<select class="form-control" rows="5" id="Attended' + loopResponse + '" name="Attended' + loopResponse + 
                      '" data-validation="required">'
                      for(loopSelectAttendedArray = 0; loopSelectAttendedArray < selectAttendedArray.length; loopSelectAttendedArray++) {
                        if(selectAttendedArray[loopSelectAttendedArray] == response[loopResponse]["attended"]) {
                            selectedOption = "selected";
                        }
                        else {
                            selectedOption = "";
                        }
                        htmlAtendance += '<option '+ selectedOption + ' value="' + selectAttendedArray[loopSelectAttendedArray] + '">' + selectAttendedArray[loopSelectAttendedArray] + '</option>'
                      }
                      htmlAtendance += '</select>' + 
                                        '</td>' +
                                        '</tr>';
  }
  console.log(attendanceRowCount)
  htmlAtendance += '</table><br><br>';
  $("#divAttendance").html(htmlAtendance);
}


function getStudentDataforAttendance() {
  sqlQuery = '', attendanceRowCount = 0;
  sqlQuery = 'select id, name from employees where job_status = "Employed"';
  data = {
  action:"fetch",
  sqlQuery: sqlQuery
  };
  $.ajax({
          type: "POST",  
          url: "response.php",  
          data: data,
          dataType: "json",       
          success: function(response)  
          { 
            if(response.status!=-1) 
            {
              var responseLength = response.length;
                if(responseLength > 0) {
                  $("#divAttendance").html("Processing...");
                  showAttendanceList(response, responseLength)
                }
                else {
                  $("#divAttendance").html("No record(s) found");
                }
            }
            else 
            {
              $("#divAttendance").html(response.message);
            }
          },
          error: function(req, status, error)  
          {
            $("#divAttendance").html("Error: \n"+status+"\n"+error);
          } 
          });
}       

  $(document).ready(function() 
  { 
      
    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["employeeattendance"]]; // db table names for insert
    db_table_insert_columns = ["employee_id","attendance_date","attended"]; // db table column names for insert

    db_table_names_update = [["employeeattendance"]]; // db table names for update
    db_table_update_columns = ["attended"]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   


    // load data grid
    loadBootgrid('employeeattendance_view');   

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal
            formReset('frm_edit');
            $("#divAttendance").html('');
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data
              
              var ele =$(this).parent();
              var attendanceDate = ele.siblings(':nth-of-type(3)').text().trim();
              populateEditFormData($(this).data("row-id"), attendanceDate)
      
            } else 
            {  //alert('No row selected! First select row, then click edit button');
            }
          }); 
    });




function populateEditFormData(attendanceId, attendanceDate) {
  // populate the data to the form
  formReset('frm_edit');
  $("#divAttendance").html('');
    if(attendanceDate != '' && attendanceId) {
      $("#attendancedate").val(attendanceDate);
      verifyAttendanceForDate(attendanceId);
    }
}

   /************** form interactions *************/


   // date popup

   $('#attendancedate').parent().addClass('date')
    $('#attendancedate').datetimepicker({
    format: 'YYYY-MM-DD'
   });

    $('#attendancedate').blur(function () {
      if($('#attendancedate').val()!='') {
      verifyAttendanceForDate('');
    }
    });
    

$( "#command-add" ).click(function() {
    $("#divAttendance").html('');
  });

   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
        sqlUpdateQueryArray.length=0;
        sqlInsertQueryArray.length=0;
        db_table_update_values.length=0;
        db_table_insert_values.length=0;

        if(attendanceRowCount > 0) 
        {
          var chkBoxAttended = "";
          for (loopAttendance = 0; loopAttendance < attendanceRowCount; loopAttendance++)
          {
            console.log($("#Attendance" + loopAttendance + " option:selected").val())
             tempQuery = 'INSERT INTO ' + db_table_names_insert + '(' + db_table_insert_columns + ') VALUES ("'+$("#data-row-" + loopAttendance).text() + '","' + $("#attendancedate").val()  + '","' + $("#Attended" + loopAttendance + " option:selected").text() + '")'; 
             sqlInsertQueryArray.push(tempQuery);
             console.log(tempQuery); 

             tempQuery = 'UPDATE ' + db_table_names_update + ' SET ' + db_table_update_columns + ' = "' + $("#Attended" + loopAttendance + " option:selected").text() + '" where employee_id = "' + $("#data-row-" + loopAttendance).text() + '" AND attendance_date = "' + $("#attendancedate").val() + '"';
             sqlUpdateQueryArray.push(tempQuery);
            console.log(tempQuery); 
          }
          $("#divAttendance").html('');
          console.log(recordExist);
            if(recordExist == true) {
              ajaxAction('edit');
            }
            else {
             ajaxAction('add');
            }
        }

        
        return false; // Will stop the submission of the form
      }
    });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>