<?php
	ob_start();
	session_start();
	 include_once("connection.php");
  
  $db = new dbObj();
  $connString =  $db->getConnstring();
	
	// if session is not set this will redirect to login page
  if ( isset($_SESSION['user'])=="") {
    header("Location: index.php");
    exit;
  }
	if ( isset($_SESSION['user'])!="" && isset($_SESSION['isAdmin'])!="No") {
    header("Location: users.php");
    exit;
  }
  if ( isset($_SESSION['user'])!="" && isset($_SESSION['isAdmin'])!="Yes" && isset($_SESSION['Active'])!="off") {
    header("Location: mytransactions.php");
    exit;
  }
	// select loggedin users detail
	$res=mysqli_query($connString, "SELECT * FROM users WHERE UserId=".$_SESSION['user']);
	$userRow=mysqli_fetch_array($res);

  $username = ""; $usernameError = "";
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Welcome - <?php echo $userRow['FullName']; ?></title>
<script type="text/javascript"  src="assets/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"  src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript"  src="assets/js/jquery.bootgrid.js"></script>
<script type="text/javascript"  src="assets/js/moment-with-locales.js"></script>
<script type="text/javascript"  src="assets/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="assets/js/tableExport.js"></script>
<script type="text/javascript" src="assets/js/jquery.base64.js"></script>

<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css" media="all">
<link href="assets/css/jquery.bootgrid.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.css" type="text/css" />
<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">COMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Transactions</a></li>
            <li><a href="myprofile.php">Profile</a></li>      
          </ul>
          <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <span class="glyphicon glyphicon-user"></span>&nbsp; <?php echo $userRow['FullName'];   
?>&nbsp;<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav> 

	<div id="wrapper">

	<div class="container">
    
    	<div class="page-header">
    	<h3>Transactions</h3>
    	</div>
        
      <div class="">        
        <div class="col-sm-12">
    <b>Savings:</b> <?php echo $userRow['Savings'];?>  &#160;&#160;
    <b>Loan:</b> <?php echo $userRow['Loan'];?>

    <table id="User_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
      <thead>
        <tr>
          <th data-column-id="TransID" data-type="numeric" data-identifier="true">TransID</th>
          <th data-column-id="UserID">UserID</th> 
          <th data-column-id="FullName">Name</th>   
          <th data-column-id="Amount">Amount</th>  
          <th data-column-id="PayType">PayType</th>   
          <th data-column-id="SchemeID">Scheme</th>  
          <th data-column-id="Subscription">Subscription</th> 
          <th data-column-id="PayDate">PayDate</th>   
          <th data-column-id="Comments">Comments</th>   
        </tr>
      </thead>
    </table>
    </div>
      </div>
    </div>
  

 <div id="pay_model" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Payment</h4>
            </div>
            <div class="modal-body">
                
        </div>
    </div>
</div>   
    </div>
    
</body>
</html>
<script type="text/javascript">
$( document ).ready(function() {

  var grid = $("#User_grid").bootgrid({
    ajax: true,
    rowSelect: true,
    post: function ()
    {
      /* To accumulate custom parameter with the request object */
      return {
        id: "b0df282a-0d67-40e5-8558-c9e93b7befed"
      };
    },
    
    url: "response.php?action=transact",
    data: { 'action': 'transact' } 
});    
                    
});

</script>
<?php ob_end_flush(); ?>