--
-- Database structure `uqa_tamil`
--

DROP DATABASE IF EXISTS `uqa_tamil`;

CREATE DATABASE `uqa_tamil`;

USE `uqa_tamil`;

-- Table structure for table `users`
--


CREATE TABLE `users` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `fullname` varchar(60) NOT NULL,
 `phone` varchar(60),
 `email` varchar(60),
 `username` varchar(60),
 `password` varchar(60),
 `active` varchar(3) NOT NULL DEFAULT 'No',
 `is_admin` varchar(3) NOT NULL DEFAULT 'No',
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
  INDEX `username` (`username`)
);



--
-- Row insert for default admin user for table `users`
--

INSERT INTO `users`(`fullname`, `phone`, `email`, `username`, `passWord`, `active`, `is_admin`) VALUES ("ADMIN","", "","admin","admin","Yes","Yes");

--
-- Table structure for table `courses`
--


CREATE TABLE `courses` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `fullname` varchar(200) NOT NULL,
 `shortname` varchar(60),
 `coursefees` decimal(60),
 `summary` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
);



--
-- Table structure for table `location`
--


CREATE TABLE `location` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(200) NOT NULL,
 `address` varchar(200),
 `area` varchar(60),
 `city` varchar(60),
 `country` varchar(60),
 `pincode` varchar(60),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
);


--
-- Table structure for table `enquiry`
--

CREATE TABLE `enquiry` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `name` varchar(60) NOT NULL,
 `phone` varchar(60),
 `whatsapp` varchar(60),
 `address` varchar(200),
 `area` varchar(60),
 `city` varchar(60),
 `country` varchar(60),
 `pincode` varchar(60),
 `email` varchar(60),
 `enquiry_category` varchar(60),
 `enquiry_date` varchar(60),
 `book_language` varchar(60),
 `book_names` varchar(200),
 `job_category` varchar(60),
 `comments` varchar(600),
 `status` varchar(60),
 `followup_comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `name` (`name`)
);

--
-- Table structure for table `enquiry`
--

CREATE TABLE `school` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `name` varchar(60) NOT NULL,
 `phone` varchar(60),
 `whatsapp` varchar(60),
 `address` varchar(200),
 `area` varchar(60),
 `city` varchar(60),
 `country` varchar(60),
 `pincode` varchar(60),
 `contact_name` varchar(60),
 `email` varchar(60),
 `comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `name` (`name`)
);

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `name` varchar(60) NOT NULL,
 `dateofbirth` date DEFAULT NULL,
 `gender` varchar(60),
 `married` varchar(60),
 `qualification` varchar(60),
 `elc_cardnumber` varchar(60),
 `enrolment_key` varchar(60),
 `phone` varchar(60),
 `whatsapp` varchar(60),
 `address` varchar(200),
 `area` varchar(60),
 `city` varchar(60),
 `country` varchar(60),
 `pincode` varchar(60) DEFAULT 'off',
 `email` varchar(60) DEFAULT 'No',
 `comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `id` (`id`)
 );

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `name` varchar(60) NOT NULL,
 `dateofbirth` date DEFAULT NULL,
 `gender` varchar(60),
 `married` varchar(60),
 `qualification` varchar(60),
 `experience` varchar(600),
 `job_category` varchar(60),
 `job_type` varchar(60),
 `job_status` varchar(60),
 `joined_date` date DEFAULT NULL,
 `resigned_date` date DEFAULT NULL,
 `salary` decimal(60),
 `phone` varchar(60),
 `address` varchar(200),
 `area` varchar(60),
 `city` varchar(60),
 `country` varchar(60),
 `pincode` varchar(60) DEFAULT 'off',
 `email` varchar(60) DEFAULT 'No',
 `comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `id` (`id`)
 );

--
-- Table structure for table `location`
--


CREATE TABLE `studentenrolment` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `student_id` varchar(60) NOT NULL,
 `batch_id` varchar(60),
 `course_id` varchar(60),
 `category` varchar(60),
 `enrolment_date` date DEFAULT NULL,
 `books_id` varchar(600),
 `course_fees` decimal(60),
 `books_fees` decimal(60),
 `discount` decimal(60),
 `status` varchar(60),
 `comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `batch_id` (`batch_id`)
);


--
-- Table structure for table `location`
--


CREATE TABLE `batchdetails` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `batch_id` varchar(100),
 `name` varchar(200) NOT NULL,
 `course_id` varchar(60),
 `fees` decimal(60),
 `location_id` varchar(600),
 `days` varchar(60),
 `start_date` date DEFAULT NULL,
 `end_date` date DEFAULT NULL,
 `start_time` varchar(60),
 `end_time` varchar(60),
 `trainer_employee_id` varchar(60),
 `status` varchar(60),
 `comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `batch_id` (`batch_id`)
);

--
-- Table structure for table `batchportions`
--

CREATE TABLE `batchportions` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `batch_id` varchar(200) NOT NULL,
 `batch_date` date DEFAULT NULL,
 `portions_targeted` varchar(600),
 `portions_completed` varchar(600),
 `comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `batch_id` (`batch_id`),
 INDEX `batch_date` (`batch_date`)
);


--
-- Table structure for table `batchportions`
--

CREATE TABLE `employeeattendance` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `employee_id` varchar(60) NOT NULL,
 `attendance_date` date DEFAULT NULL,
 `attended` varchar(100),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `employee_id` (`employee_id`),
 INDEX `attendance_date` (`attendance_date`)
);


--
-- Table structure for table `batchportions`
--

CREATE TABLE `studentattendance` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `student_id` varchar(60) NOT NULL,
 `batch_id` varchar(600),
 `batch_date_id` varchar(600),
 `attendance_date` date DEFAULT NULL,
 `attended` varchar(100),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `batch_id` (`batch_id`),
 INDEX `attendance_date` (`attendance_date`)
);


--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `category` varchar(60) NOT NULL,
 `name` varchar(200),
 `quantity` int(60),
 `unit_price` decimal(60),
 `discount` decimal(60),
 `extra_charges` decimal(60),
 `total_amount` decimal(60),
 `comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `category` (`category`)
);


--
-- Table structure for table `meetingdetails`
--

CREATE TABLE `meetingdetails` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `name` varchar(200),
 `date` date DEFAULT NULL,
 `location` varchar(200),
 `time` varchar(60),
 `attendees` varchar(600),
 `agenda` varchar(600),
 `comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `name` (`name`)
);

--
-- Table structure for table `meetingaction`
--

CREATE TABLE `meetingaction` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `meeting_id` varchar(200),
 `task_name` varchar(200),
 `task_start_date` date DEFAULT NULL,
 `task_end_date` date DEFAULT NULL, 
 `task_assigned_to` varchar(200),
 `action_tobe_taken` varchar(600),
 `action_taken` varchar(600),
 `task_status` varchar(100),
 `comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `meeting_id` (`meeting_id`)
);

--
-- Table structure for table `incomeexpense`
--

CREATE TABLE `incomeexpense` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `type` varchar(200),
 `category` varchar(200),
 `student_enrolment_id` varchar(200),
 `school_id` varchar(200),
 `batch_id` varchar(200),
 `description` varchar(500),
 `total_bill_amount` decimal(60),
 `amount_received` decimal(60),
 `amount_paid` decimal(60),
 `balance` decimal(60),
 `invoice_number` varchar(200),
 `invoice_date` date DEFAULT NULL,
 `payment_mode` varchar(200),
 `payment_comments` varchar(200),
 `payment_date` date DEFAULT NULL,
 `status` varchar(60),
 `comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `category` (`category`)
);

--
-- Table structure for table `incomeexpense`
--

CREATE TABLE `incomeexpensepayment` (
 `id` int(10) NOT NULL AUTO_INCREMENT,
 `incomeexpense_id` varchar(200),
 `amount_received` decimal(60),
 `amount_paid` decimal(60),
 `payment_date` date DEFAULT NULL,
 `payment_mode` varchar(60),
 `payment_comments` varchar(600),
 `modified` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 INDEX `id` (`id`)
);

--
-- Trigger structure for table `incomeexpense`
--

DELIMITER $$

CREATE TRIGGER after_insert_incomeexpense AFTER INSERT ON incomeexpense FOR EACH ROW
BEGIN
  INSERT INTO incomeexpensepayment (incomeexpense_id, payment_date, amount_received, amount_paid, payment_mode, payment_comments) values (new.id, new.payment_date, new.amount_received, new.amount_paid, new.payment_mode, new.payment_comments);
END$$

DELIMITER ;


DELIMITER $$
CREATE TRIGGER `after_delete_incomeexpense`
AFTER DELETE ON `incomeexpense`
FOR EACH ROW
BEGIN
  DELETE FROM incomeexpensepayment WHERE incomeexpense_id = OLD.id; 
END $$
DELIMITER ;

--
-- Views structure for student attendance
--

create view studentenrolment_students_join_view as select studentenrolment.student_id as id, studentenrolment.batch_id, students.name from studentenrolment join students on students.id = studentenrolment.student_id where studentenrolment.category = 'Classroom';

create view studentenrolment_view as SELECT distinct (studentenrolment.course_fees+studentenrolment.books_fees-studentenrolment.discount)-(SELECT IFNULL(sum(amount_received),0) FROM incomeexpense where incomeexpense.student_enrolment_id = studentenrolment.id) as due_amount, studentenrolment.*, students.name, courses.fullname as coursename, inventory.name as bookname, batchdetails.name as batchname FROM studentenrolment left join students on students.id = studentenrolment.student_id left join courses on courses.id = studentenrolment.course_id left join inventory on studentenrolment.books_id = inventory.id left join batchdetails on batchdetails.batch_id = studentenrolment.batch_id; 

create view studentattendance_students_join_view as select studentattendance.id as rowid, studentattendance.student_id as id, studentattendance.batch_id, studentattendance.attendance_date, studentattendance.attended, students.name from studentattendance join students on students.id = studentattendance.student_id;

create view studentattendance_batchportions_join_view as select DISTINCT studentattendance.id as rowid, studentattendance.student_id as id, batchportions.batch_id, studentattendance.attendance_date, studentattendance.attended, batchportions.batch_date from batchportions left join studentattendance on batchportions.batch_date = studentattendance.attendance_date;

create view studentattendance_view as SELECT studentattendance.*, students.name, batchdetails.name as batchname FROM studentattendance left join students on studentattendance.student_id = students.id left join batchdetails on batchdetails.batch_id = studentattendance.batch_id;
	
create view employeeattendance_view as SELECT employeeattendance.*, employees.name FROM employeeattendance join employees on employeeattendance.employee_id = employees.id;

create view batchdetails_view as SELECT batchdetails.*, location.name as locationname, courses.fullname as coursename, employees.name as employeename FROM batchdetails LEFT JOIN courses on courses.id = batchdetails.course_id LEFT JOIN location on location.id = batchdetails.location_id LEFT JOIN employees on employees.id = batchdetails.trainer_employee_id;

create view batchportions_view as SELECT batchportions.*, batchdetails.name as batchname FROM batchportions LEFT JOIN batchdetails on batchdetails.batch_id = batchportions.batch_id;

create view meetingaction_view as SELECT meetingaction.*, meetingdetails.name as meetingname, employees.name as employeename FROM meetingaction LEFT JOIN meetingdetails on meetingdetails.id = meetingaction.meeting_id LEFT JOIN employees on employees.id = meetingaction.task_assigned_to;

create view incomeexpense_view as select distinct incomeexpense.*, batchdetails.name as batchname, school.name as schoolname from incomeexpense left join batchdetails on batchdetails.batch_id = incomeexpense.batch_id LEFT JOIN school on school.id = incomeexpense.school_id;