<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>
<?php
	// select loggedin users detail
	$res=mysqli_query($connString, "SELECT * FROM users WHERE id=".$_SESSION['user']);
	$userRow=mysqli_fetch_array($res);

  $username = ""; $usernameError = "";
?>


	<div id="wrapper">

	<div class="container">
    
    	<div class="page-header">
    	<h3>Profile</h3>
    	</div>
        
        <div class="col-sm-12">
    <form method="post" id="frm_edit">
       <input type="hidden" value="save" name="action" id="action">
        <input type="hidden" value="<?php echo $userRow['id']; ?>"" name="id" id="id" >
                  <div class="form-group">
                    <label for="fullname" class="control-label">Name</label>
                    <input type="text" class="form-control" id="fullname" name="fullname" data-validation="alphanumeric" data-validation-allowing=". " value="<?php echo $userRow['fullname']; ?>"/>
                    <span class='text-danger'></span>
                  </div>
                  <div class="form-group">
                    <label for="phone" class="control-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" data-validation="alphanumeric" data-validation-allowing=")(+-. " value="<?php echo $userRow['phone']; ?>"/>
                    <span class='text-danger'></span>
                  </div>
                  <div class="form-group">
                    <label for="email" class="control-label">Email</label>
                    <input type="text" class="form-control" id="email" name="email" data-validation="email" data-validation-error-msg="Enter a valid e-mail" data-validation-optional="true" placeholder="example@domain.com" value="<?php echo $userRow['email']; ?>"/>
                    <span class='text-danger'></span>
                  </div>
                  <div class="form-group">
                    <label for="username" class="control-label">Username</label>
                    <input type="text" disabled class="form-control" id="username" name="username" value="<?php echo $userRow['username']; ?>"/>
                    <span class='text-danger'></span>
                  </div>
                  <div class="form-group">
                    <label for="password" class="control-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password" value="<?php echo $userRow['password']; ?>"/>
                    <span class='text-danger'></span> 
                  </div>              
                
            </div>
            <div class="modal-footer" style="border:0">
                <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
            </div>
      </form>
   
   
       </div>
       
  
<div id="edit_modal"></div>
 
</div>   
<?php include_once("footer.php"); ?>
    </div>
    <script>
$( document ).ready(function() {

var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["users"]]; // db table names for insert
    db_table_insert_columns = [["fullname","phone","email","username","password","active","is_admin"]]; // db table column names for insert

    db_table_names_update = [["users"]]; // db table names for update
    db_table_update_columns = [["fullname","phone","email","password"]]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert  
   
/************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
     sqlUpdateQueryArray.length=0;
      sqlInsertQueryArray.length=0;
      db_table_update_values.length=0;
      db_table_insert_values.length=0;

      if(db_table_update_columns.length!=0) {
        var tempArray = [], 
            formValue = '', 
            formElementId = '', 
            formElementValue = '', 
            loopColumnsArrayLength =0;


        /************* get edit form values and push to array ***************/

        loopColumnsArrayLength = db_table_update_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
            tempArray.push(formElementValue); 
        }
        db_table_update_values.push(tempArray);



         /************* get add form values and push to array ***************/

        loopColumnsArrayLength = db_table_insert_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_insert_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
          tempArray.push(formElementValue);                    
        }
        db_table_insert_values.push(tempArray);
      }

      create_db_table_sql_update_query($('#' + formId + ' #id').val()); // For update query - db table row unique id as parameter
      create_db_table_sql_insert_query(); // For insert query 
        ajaxAction('edit'); 
        return false; // Will stop the submission of the form
      }
    });

});


    </script>
</body>
</html>

<?php ob_end_flush(); ?>