<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Student Enrolment</h3>
  	</div>        
    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","",""); 


              renderGridHeaderColumn("name","Student",true,"","","","","","");
              renderGridHeaderColumn("student_id","Student id",true,"","","","","false","");
              renderGridHeaderColumn("category","Category",false,"","","","","","");
              renderGridHeaderColumn("enrolment_date","Enrolment date",false,"","","","","","");
              renderGridHeaderColumn("batchname","Batch",false,"","","","","","");
              renderGridHeaderColumn("batch_id","Batch id",false,"","","","","false","");
              renderGridHeaderColumn("coursename","Course",true,"","","","","","");
              renderGridHeaderColumn("course_id","Course id",false,"","","","","false","");
              renderGridHeaderColumn("bookname","Books",false,"","","","","","");              
              renderGridHeaderColumn("books_id","Books id",false,"","","","","false","");
              renderGridHeaderColumn("course_fees","Course fees",false,"","","","","false","");
              renderGridHeaderColumn("books_fees","Books fees",false,"","","","","false","");
              renderGridHeaderColumn("discount","Discount",false,"","","","","false","");
              renderGridHeaderColumn("due_amount","Due",false,"","","","","","");
              renderGridHeaderColumn("status","Status",true,"","","","","","");
              renderGridHeaderColumn("comments","Comments",false,"","","","","false","");


              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>

<!---- Add/Edit Form -->

<div id="edit_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo "<div class='form-sub-header'>Basic Information</div>";
            echo '<div class="row">';

              $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

              $sqlResult = fetchRecordForDropdown("select id, name, dateofbirth, city, phone from students order by name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['id']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;ID: ' .  $json[$loop]['id'] . '  &#160;&#160;DOB: ' . $json[$loop]['dateofbirth'] . '  &#160;&#160;Phone: ' . $json[$loop]['phone'] . ' &#160;&#160;City: ' . $json[$loop]['city']);
              }              

              renderFormSelect("student_id","Student",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-6");
              renderFormInput("enrolment_date","Enrolment date",'data-validation="required" placeholder="YYYY-MM-DD"',"","col-md-3");
              echo '</div>';

            echo '<div class="row">';

              renderFormRadio("category","Enrolment category",'data-validation="required"',['Classroom', 'Online'],"","col-md-4");
               
            echo '</div>';

            echo '<div class="row">';   

              $optionTexts=array(); $optionValues=array(); $optionSubTexts=array();

              $sqlResult = fetchRecordForDropdown("select batchdetails.*, courses.fullname, courses.id as courseid from batchdetails left join courses on courses.id = batchdetails.course_id where batchdetails.status <> 'Completed' order by batchdetails.name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['batch_id'] . '|' . $json[$loop]['fees'] . '#' . $json[$loop]['courseid']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;Course: ' .  $json[$loop]['fullname'] . '  &#160;&#160;Fees: ' . $json[$loop]['fees'] . '  &#160;&#160;Batch Date: ' . $json[$loop]['start_date'] . ' to ' . $json[$loop]['end_date'] . '  &#160;&#160;status: ' . $json[$loop]['status']);
              }              

              renderFormSelect("batch_id","Batch",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-4");         
              

              $optionTexts=[]; $optionValues=[]; $optionSubTexts=[];
              $sqlResult = fetchRecordForDropdown("select id, fullname, shortname, coursefees from courses order by fullname");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['id'] . '|' . $json[$loop]['coursefees']);
                array_push($optionTexts, $json[$loop]['fullname'] . ' (' . $json[$loop]['shortname'] . ')');
                array_push($optionSubTexts, '<br>&#160;&#160;fees: ' .  $json[$loop]['coursefees']);
              } 

              renderFormSelect("course_id","Course",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-4");

              $optionTexts=[]; $optionValues=[]; $optionSubTexts=[];
              $sqlResult = fetchRecordForDropdown("select id, name, unit_price from inventory where category = 'Books' order by name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['id'] . '|' . $json[$loop]['unit_price']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;&#160;unit price: ' .  $json[$loop]['unit_price']);
              } 

              renderFormSelect("books_id","Book Name(s)",'',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-6");

            echo '</div>';

            echo '<div class="row">';
              renderFormRadio("status","Status",'data-validation="required"',['Inprogress', 'Completed', 'Certified'],"Inprogress","col-md-6"); 
            echo '</div>';




            echo "<br><div class='form-sub-header'>Fees Payment Information</div>";
            echo '<div class="row">';              
              renderFormInput("course_fees","Course fees",'data-validation="number"',"","col-md-4");                       
              renderFormInput("books_fees","Books fees",'data-validation="number"',"","col-md-4");  
            echo '</div>';

            echo '<div class="row">';
              renderFormInput("discount","Discount amount",'data-validation="number"',"","col-md-4");    
              renderFormInput("total_fees","Total fees",'disabled data-validation="number"',"","col-md-4");    
            echo '</div>';

            echo "<br><div class='form-sub-header'>Additional Information</div>";
            echo '<div class="row">';
              renderFormTextarea("comments","Comments",'',"","");
            echo '</div>';


            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>

<script>
  $(document).ready(function() 
  { 
      
    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["studentenrolment"]]; // db table names for insert
    db_table_insert_columns = [["student_id","batch_id","course_id","category","enrolment_date","books_id","course_fees","books_fees","discount","status","comments"]]; // db table column names for insert

    db_table_names_update = [["studentenrolment"]]; // db table names for update
    db_table_update_columns = [["student_id","batch_id","course_id","category","enrolment_date","books_id","course_fees","books_fees","discount","status","comments"]]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   


    // load data grid
    loadBootgrid('studentenrolment_view');   

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal
            formReset('frm_edit');
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data              
              $('#' + formId+' #id').val($(this).data("row-id")); // in case we're changing the key
              // ajax call
              sqlQuery = 'select * from ' + db_table_names_update + ' where id=' + $(this).data("row-id");
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                          populateEditFormData(response);
                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });
              } else 
              {  //alert('No row selected! First select row, then click edit button');
              }
          }); 
    });


var recordExist = false;
function verifyDuplicateEnrolment() {
  
  sqlQuery = 'select id from studentenrolment where student_id = "' + $('#student_id').val() + '" and batch_id="' + $('#batch_id').val() + '"';

  console.log(sqlQuery);
  data = {
  action:"fetch",
  sqlQuery: sqlQuery
  };
  $.ajax({
          type: "POST",  
          url: "response.php",  
          data: data,
          dataType: "json",       
          success: function(response)  
          { 
            console.log(response)
            if(response.status!=-1) 
            {
              var responseLength = response.length;
                if(responseLength > 0) {
                    recordExist = true;
                    $('#div_student_id .text-danger').html("Already enrolled for this batch");
                }
                else {
                  recordExist = false;
                  $('#div_student_id .text-danger').html("");
                }
            }
            else 
            {
              alertm(response.message);
            }
          },
          error: function(req, status, error)  
          {
            alertm("Error: \n"+status+"\n"+error);
          } 
          });
}

function populateEditFormData(dataArray) {
  // populate the data to the form
  formReset('frm_edit');
  formInteractions(eventElementId);
  var dataArrayLength = dataArray[0].length, formElementValue = '';

  if(dataArray.length > 0) 
  {
    
      loopColumnsArrayLength = db_table_update_columns[0].length;

        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = dataArray[0][formElementId];
          
          //console.log("#"+formElementId + " " + $('#' + formElementId).val())
          
            if($("#"+formElementId).attr('elementType') === "text") {
              $('#' + formElementId).val(formElementValue);
            }

            if($("#" + formElementId).attr('elementType') === "radio") {
              formInteractions(eventElementId);
              if(formElementValue!="") {
                $("input[name="+formElementId+"][value='" + formElementValue + "']").prop('checked', true); 
              }
              
            }

            if($("#"+formElementId).attr('elementType') === "select") {
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
                if(formElementValue != null || formElementValue != undefined)
                {
                    formElementValueSplit = formElementValue.split(",");
                    
                    if(formElementValueSplit.length>0) {
                      for (loop = 0; loop<formElementValueSplit.length; loop++)
                      {
                        $("#" + formElementId + " option[value='" + formElementValueSplit[loop] + "']").prop("selected",true);
                      }
                }
              }
              else {
                $("#"+formElementId+"[value=" + formElementValue + "]").prop("selected",true);
              }
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
            }

            if($("#" + formElementId).attr("elementType") === "textarea") {
              $('#' + formElementId).val(formElementValue);
            }
        }
  }

}

   /************** form interactions *************/

// date popup
    $('#enrolment_date').parent().addClass('date')
      $('#enrolment_date').datetimepicker({
      format: 'YYYY-MM-DD'
    });

   $('#div_batch_id').hide();

   var eventElementId = "category";
   $('input:radio[id='+eventElementId+']').change(function () {
        formInteractions(eventElementId);
    });


   $('#course_fees, #books_fees, #discount').change(function() {
      totalAmountCalculations();  
   });

   $(window).on('shown.bs.modal', function (e) {
    if($("#action").val() == "edit") {
      totalAmountCalculations(); 
    }
  });

   $('#frm_edit').click(function() {
      totalAmountCalculations();  
   });

   $('#batch_id').change(function() {
      var courseDetails = $('#batch_id option:selected').attr("data-value");
    
      if(courseDetails != '') {

      var courseId = '', courseFees = '';

      var courseDetailsSplit = courseDetails.split("#");
      courseId = courseDetailsSplit[1];
      courseFees = courseDetailsSplit[0];

        console.log(courseId)

          $('#course_fees').val(courseFees); 
          $("#course_id  option[value='" + courseId + "']").prop("selected",true);
          $('#course_id').selectpicker('refresh');
      }

      if( $("#action").val() =='add') {
        if($('#batch_id').val() != '') {
          verifyDuplicateEnrolment();
        }
      }
   });

   $('#course_id').change(function() {
      $('#course_fees').val($('#course_id option:selected').attr("data-value"));  
   });

   $('#student_id').change(function() {
      if( $("#action").val() =='add') {
        if($('#batch_id').val() != '') {
         verifyDuplicateEnrolment();
       }
      }
   });

   


   function totalAmountCalculations() {
      
      var course_fees = parseInt($('#course_fees').val() == "" ? 0 : $('#course_fees').val());
      var books_fees = parseInt($('#books_fees').val() == "" ? 0 : $('#books_fees').val());
      var discount = parseInt($('#discount').val() == "" ? 0 : $('#discount').val());

      
      if(discount > parseInt(course_fees+books_fees)) {
        $('#div_course_fees_paid .text-danger').html('Discount should not be more than course fees + books fees');
      }
      else {
        $('#div_course_fees_paid .text-danger').html('');
      }

      $("#total_fees").val(parseInt(course_fees) - parseInt(discount) + parseInt(books_fees)); 
   }

   function formInteractions()
   {
        if ($("input[id="+eventElementId+"]:checked").val() == 'Classroom') {
          $('#div_batch_id').show();
          $('#div_batch_id .formOptionalText').hide();
          $('#batch_id').attr("data-validation", "required");
        }


        if ($("input[id="+eventElementId+"]:checked").val() == 'Online') {
          $('#div_course_id').show();
          $('#div_course_id .formOptionalText').hide();
          $('#div_batch_id').hide();
          $('#div_batch_id .formOptionalText').hide();
          $('#batch_id').attr("data-validation", "");
          $('#batch_id .selectpicker').val('');
          //$('#course_id').val('default').selectpicker("refresh");
          $('#batch_id').val('default').selectpicker("refresh");
        }
   }



     // date popup
     $('#dateofbirth').parent().addClass('date')
    $('#dateofbirth').datetimepicker({
      format: 'YYYY-MM-DD'
     });



   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },   
    onSuccess : function($form) {
      sqlUpdateQueryArray.length=0;
      sqlInsertQueryArray.length=0;
      db_table_update_values.length=0;
      db_table_insert_values.length=0;

      if(db_table_update_columns.length!=0) {
        var tempArray = [], 
            formValue = '', 
            formElementId = '', 
            formElementValue = '', 
            loopColumnsArrayLength =0;


        /************* get edit form values and push to array ***************/

        loopColumnsArrayLength = db_table_update_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              //console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
            tempArray.push(formElementValue); 
        }
        db_table_update_values.push(tempArray);



         /************* get add form values and push to array ***************/

        loopColumnsArrayLength = db_table_insert_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_insert_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
              //console.log(formElementId + " : " + formElementValue)
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
          tempArray.push(formElementValue);                    
        }
        db_table_insert_values.push(tempArray);
      }

      create_db_table_sql_update_query($('#' + formId + ' #id').val()); // For update query - db table row unique id as parameter
      create_db_table_sql_insert_query(); // For insert query 
       if(recordExist == true &&  $("#action").val() =='add') {
          alertm("Already enrolled for this batch");
        }
        else {
          ajaxAction($('#action').val()); 
         }
       // parameter "both" executes both insert and update sql
      return false; // Will stop the submission of the form
    }
  });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>