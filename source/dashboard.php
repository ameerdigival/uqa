<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper" class="dashboardWrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Dashboard</h3> 
  	</div>        
    <div class="col-sm-12">

      <div class="form-row">
        <div class="row">
          <div id="subHeader"></div>
           
           <div class="pull-right" style='padding-bottom:10px;'>

            <div style="float:left;padding-right: 7px">
            <label for="selectYear" class="control-label">Year </label>
           <select id="selectYear" class="form-control"><option value="">---</option></select>
          </div>
          <div style="float:left;padding-right: 7px">
            <label for="selectMonth" class="control-label">Month </label>
            <select id="selectMonth" class="form-control"><option value="">---</option></select>
          </div>

          <div style="float:left;padding-right: 7px">
            <label for="fromDate" class="control-label">From date </label>
            <input elementtype="text" class="form-control" id="fromDate" name="fromDate" value="" placeholder="YYYY-MM-DD" type="text">
          </div>          
         <div style="float:left;padding-right: 7px">
          <label for="toDate" class="control-label">To date </label>
            <input elementtype="text" class="form-control" id="toDate" name="toDate" value="" placeholder="YYYY-MM-DD" type="text">
          </div>

          </div>
          </div>

          <div class="row">
          <div class="form-group col-md-4">
          </div>

          <div class="form-group col-md-4">
          </div>
        </div>            
      </div>

      <div class="row">
         <div class="form-group col-box">
            <div class="dashboard-box box-color-1">
              <div id="openEnquiry" class="dashboard-box-count">0</div>
              <div class="dashboard-box-text">Open <br>Enquiry</div>
            </div>
          </div>

          <div class="form-group col-box">
            <div class="dashboard-box box-color-2">
              <div id="closedEnquiry" class="dashboard-box-count">0</div>
              <div class="dashboard-box-text">Closed <br>Enquiry</div>
            </div>
          </div>

          <div class="form-group col-box">
            <div class="dashboard-box box-color-3">
              <div id="batchesCompleted" class="dashboard-box-count">0</div>
              <div class="dashboard-box-text">Batches <br>Completed</div>
            </div>
          </div>

          <div class="form-group col-box">
            <div class="dashboard-box box-color-4">
              <div id="batchesInprogress" class="dashboard-box-count">0</div>
              <div class="dashboard-box-text">Batches <br>Inprogress</div>
            </div>
          </div>

          <div class="form-group col-box">
            <div class="dashboard-box box-color-5">
              <div id="batchesToStart" class="dashboard-box-count">0</div>
              <div class="dashboard-box-text">Batches <br>to Start</div>
            </div>
          </div>          

          <div class="form-group col-box">
            <div class="dashboard-box box-color-6">
              <div id="studentsEnrolled" class="dashboard-box-count">0</div>
              <div class="dashboard-box-text">Students <br>Enrolled</div>
            </div>
          </div>

          <div class="form-group col-box">
            <div class="dashboard-box box-color-7">
              <div id="coursesEnrolled" class="dashboard-box-count">0</div>
              <div class="dashboard-box-text">Students <br>Completed</div>
            </div>
          </div>

          <div class="form-group col-box">
            <div class="dashboard-box box-color-8">
              <div id="studentsCertified" class="dashboard-box-count">0</div>
              <div class="dashboard-box-text">Students <br>Certified</div>
            </div>
          </div>
      </div>

      <br />
      <div class="form-row">
        <div class="row">
          <div class="form-group col-md-3" id="divBarChart">            
          </div>

          <div class="form-group col-md-3" id="divPieChart">            
          </div>

          <div class="form-group col-md-3" id="divHorizontalBarChart">
          </div>

           <div class="form-group col-md-3" id="divDoughnutChart">
          </div>
        </div>            
      </div>
      <br />
      <div class="form-row">
        <div class="row">
          <div class="form-group col-md-3">
            <div class="stats-bar stats-header-batch">Batch Course Statistics</div>
              <div class="stats-bar stats-subheader-batch"><div id="batchCourseStatsEnrolled" class="statsCount"></div> students enrolled</div>
              <div class="stats-bar stats-subheader-batch"><div id="batchCourseStatsRevenue" class="statsCount"></div> revenue generated</div>
          </div>

          <div class="form-group col-md-3">
            <div class="stats-bar stats-header-school">School Course Statistics</div>
              <div class="stats-bar stats-subheader-school"><div id="schoolCourseStatsEnrolled" class="statsCount"></div> schools</div>
              <div class="stats-bar stats-subheader-school"><div id="schoolCourseStatsRevenue" class="statsCount"></div> revenue generated</div>
          </div>

          <div class="form-group col-md-3">
            <div class="stats-bar stats-header-course">Online Course Statistics</div>
              <div class="stats-bar stats-subheader-course"><div id="onlineCourseStatsEnrolled" class="statsCount"></div>students enrolled</div>
              <div class="stats-bar stats-subheader-course"><div id="onlineCourseStatsRevenue" class="statsCount"></div> revenue generated</div>
          </div>

          <div class="form-group col-md-3">
            <div class="stats-bar stats-header-accounts">Student Statistics</div>
              <div class="stats-bar stats-subheader-accounts"><div id="totalStudentsEnrolled" class="statsCount"></div> students enrolled</div>
              <div class="stats-bar stats-subheader-accounts"><div id="totalStudentDues" class="statsCount"></div> students due</div>
          </div>

        </div>            
      </div>

    </div>
  </div>
</div>


<?php include_once("footer.php"); ?>
</div>

<script>
$(document).ready(function() 
{ 
// Year data populate
var max = new Date().getFullYear(),
    min = max - 20,
    select = document.getElementById('selectYear');

for (var i = max; i>=min; i--){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}

for (var i = 0; i<monthShortNames.length; i++){
    select = document.getElementById('selectMonth');
    var opt = document.createElement('option');
    opt.value = i+1;
    opt.innerHTML = monthShortNames[i];
    select.appendChild(opt);
}

var currentDate = new Date();
var currentMonth = currentDate.getMonth();
// console.log(currentMonth);
// $("#selectMonth option[value="+(currentMonth+1)+"]").prop("selected","true");


// date popup
var current = new Date();     // get current date    
var weekstart = current.getDate() - current.getDay() +1;    
var weekend = weekstart + 6;       // end day is the first day + 6 
var startOfWeek = new Date(current.setDate(weekstart));  
var endOfWeek = new Date(current.setDate(weekend));
var startOfWeekSplitted = startOfWeek.toISOString().slice(0,10);  
var endOfWeekSplitted = endOfWeek.toISOString().slice(0,10);
// console.log(startOfWeekSplitted,endOfWeekSplitted);

var todayDate = new Date();
var previousDate = new Date(todayDate);
previousDate.setDate(todayDate.getDate()-6);
previousDate.toLocaleDateString();

   $('#fromDate').parent().addClass('date')
    $('#fromDate').datetimepicker({
    format: 'YYYY-MM-DD',
    date: moment()
    // date: startOfWeekSplitted
   }); 

    $('#toDate').parent().addClass('date')
    $('#toDate').datetimepicker({
    format: 'YYYY-MM-DD',
    date: moment()
    // date: endOfWeekSplitted
   }); 

var totalCourseRevenueCount = 0, totalCourseExpenseCount = 0, totalExpenseCount = 0, totalIncomeCount = 0, totalExpenseDue = 0, totalIncomeDue = 0, totalStudentsDue = 0, verticalChartData = [], verticalChartLabels = [], horizontalChartData = [], horizontalChartLabels = [];

var month = $("#selectMonth option:selected").val();
var year = $("#selectYear option:selected").val();

var fromDate = $("#fromDate").val();
var toDate = $("#toDate").val();

formInteractions(month, year, fromDate, toDate);

$("#selectMonth, #selectYear").change(function () {
  var month = $("#selectMonth option:selected").val();
  var year = $("#selectYear option:selected").val();
  $('#fromDate').val('');
  $('#toDate').val('');
  formInteractions(month, year, '', '');
});


$("#fromDate, #toDate").blur(function () {
  var fromDate = $("#fromDate").val();
var toDate = $("#toDate").val();
  $("#printArea").html("");
  if(fromDate != '' && toDate != '' && fromDate != undefined && toDate != undefined) {
    formInteractions(month, year, fromDate, toDate);
  }
});

function formInteractions(month, year, fromDate, toDate) {

  var displaySubHeader = '';
  var monthname = '';
  
  if(month != '') {
    monthname = monthFullNames[month-1];
    displaySubHeader = monthname + ' ' + year;
  }
  else {
    displaySubHeader = year;
  }

  if(fromDate != '' && toDate != '' && fromDate != undefined && toDate != undefined) {

    var yy_fromDate = new Date(fromDate);
     yy_fromDate_html = yy_fromDate.getDate() + " " + monthShortNames[(yy_fromDate.getMonth())]  + " " + yy_fromDate.getFullYear();

     var yy_toDate = new Date(toDate);
     yy_toDate_html = yy_toDate.getDate() + " " + monthShortNames[(yy_toDate.getMonth())]  + " " + yy_toDate.getFullYear();

      if(fromDate == toDate) {
        displaySubHeader = yy_fromDate_html;
      }
      else
      {
        displaySubHeader = yy_fromDate_html + " - " + yy_toDate_html;
      }
      
  }

  $("#subHeader").html(displaySubHeader+"<div id=cashInHand></div>");
  // console.log(month, year);
  loadCashInHand(month, year, fromDate, toDate)
  loadEnquiryDetailsCount(month, year, fromDate, toDate);
  loadBatchDetailsCount(month, year, fromDate, toDate);
  loadEnrolmentDetailsCount(month, year, fromDate, toDate);
  loadSchoolDetailsCount(month, year, fromDate, toDate);
  loadIncomeExpenseDetailsCount(month, year, fromDate, toDate);
  loadTopCourseEnrolmentCount(month, year, fromDate, toDate);
  loadTopCourseRevenuCount(month, year, fromDate, toDate);
}

function loadCashInHand(month, year, fromDate, toDate) {
var toDate = $("#toDate").val();
      var sqlQuery = "SELECT sum(IF(amount_received <> 0, amount_received,0))-sum(IF(amount_paid <> 0, amount_paid, 0)) as profitloss, payment_mode FROM incomeexpensepayment where 1"

        if(toDate != '' && toDate != undefined) {
            sqlQuery += " and (payment_date) <= '" + toDate + "'"
        }
         else {
              if(month != '') {
                    sqlQuery += ' and month(payment_date) <= "' +  month + '"';
                  }
              if(year != '') {
                      sqlQuery += ' and year(payment_date) <= "' +  year + '"';
                    }
        }

        sqlQuery += " GROUP by payment_mode order by payment_mode asc"

      console.log(sqlQuery)
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                var html_cashInHand = '', cashInHandAmount = 0, cashInBankAmount = 0;

                for(loopColumns=0;loopColumns<response.length;loopColumns++) 
                { 
                  if(response[loopColumns]["payment_mode"] == "Cash") {                    
                    cashInHandAmount = response[loopColumns]["profitloss"];
                  }
                  if(response[loopColumns]["payment_mode"] == "Bank") {
                    cashInBankAmount = response[loopColumns]["profitloss"];
                  }                  
                }
                if(cashInHandAmount == null) {
                  cashInHandAmount = 0;
                }
                if(cashInBankAmount == null) {
                  cashInBankAmount = 0;
                }
                html_cashInHand = '<b>Cash in Hand:</b> ' + cashInHandAmount + '&#160;&#160;&#160;&#160; <b>Bank:</b> ' + cashInBankAmount
                $("#cashInHand").html(html_cashInHand);
                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}

function loadEnquiryDetailsCount(month, year, fromDate, toDate) {
  var sqlQuery='SELECT status FROM enquiry where 1 <> 0';
  
  if(fromDate != '' && toDate != '') {
          sqlQuery += ' and enquiry_date between "' +  fromDate + '" AND "' + toDate + '"';
        }
  else {
    if(month != '') {
          sqlQuery += ' and month(enquiry_date) = "' +  month + '"';
        }
  if(year != '') {
          sqlQuery += ' and year(enquiry_date) = "' +  year + '"';
        }
  }
  console.log(sqlQuery)
  var enquiryOpenCount = 0, enquiryClosedCount = 0;
  data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                var responseLength = response.length;
                for(loopRecord=0; loopRecord < responseLength; loopRecord++) {
                  if(response[loopRecord]["status"] == 'Open') {
                    enquiryOpenCount++;
                  }
                  if(response[loopRecord]["status"] == 'Closed') {
                    enquiryClosedCount++;
                  }
                }
                $("#openEnquiry").html(enquiryOpenCount);
                $("#closedEnquiry").html(enquiryClosedCount);
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
  
}

function loadBatchDetailsCount(month, year, fromDate, toDate) {
  var sqlQuery='SELECT status FROM batchdetails where 1 <> 0';
  if(fromDate != '' && toDate != '') {
          sqlQuery += ' and start_date between "' +  fromDate + '" AND "' + toDate + '"';
        }
  else {
    if(month != '') {
          sqlQuery += ' and month(start_date) = "' +  month + '"';
        }
  if(year != '') {
          sqlQuery += ' and year(start_date) = "' +  year + '"';
        }
  }
  console.log(sqlQuery)
  var batchesCompletedCount = 0, batchesInprogressCount = 0, batchesNotStartedCount = 0;
  data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                var responseLength = response.length;
                for(loopRecord=0; loopRecord < responseLength; loopRecord++) {
                  if(response[loopRecord]["status"] == 'Completed') {
                    batchesCompletedCount++;
                  }
                  if(response[loopRecord]["status"] == 'Inprogress') {
                    batchesInprogressCount++;
                  }
                  if(response[loopRecord]["status"] == 'Not started') {
                    batchesNotStartedCount++;
                  }
                }
                $("#batchesCompleted").html(batchesCompletedCount);
                $("#batchesInprogress").html(batchesInprogressCount);
                $("#batchesToStart").html(batchesNotStartedCount);
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
  
}

function loadSchoolDetailsCount(month, year, fromDate, toDate) {
  var sqlQuery='SELECT * FROM school';
  data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                var responseLength = response.length;

                $("#schoolCourseStatsEnrolled").html(responseLength);
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
  
}


function loadEnrolmentDetailsCount(month, year, fromDate, toDate) {
  var sqlQuery='SELECT studentenrolment.*, (studentenrolment.course_fees+studentenrolment.books_fees-studentenrolment.discount)-(SELECT IFNULL(sum(amount_received),0) FROM incomeexpense where incomeexpense.student_enrolment_id = studentenrolment.id) as dueamount FROM studentenrolment where 1';
  if(fromDate != '' && toDate != '') {
          sqlQuery += ' and enrolment_date between "' +  fromDate + '" AND "' + toDate + '"';
        }
  else {
    if(month != '') {
          sqlQuery += ' and month(enrolment_date) = "' +  month + '"';
        }
  if(year != '') {
          sqlQuery += ' and year(enrolment_date) = "' +  year + '"';
        }
  }
        console.log(sqlQuery);
  var coursesEnrolledCount = 0, studentsCompletedCount = 0, studentsEnrolledCount = 0, studentsCertifiedCount = 0, batchCourseStatsEnrolledCount = 0,  batchCourseStatsRevenueCount = 0, onlineCourseStatsRevenueCount = 0, totalStudentDuesCount = 0, courses = [], batches = [];
  data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              courses = [];
              batches = [];
              studentsCertifiedCount =0, studentsCompletedCount = 0, batchCourseStatsEnrolledCount = 0, batchCourseStatsRevenueCount=0, totalCourseRevenueCount=0, totalStudentsDue = 0;
              if(response.status!=-1) 
              {
                var responseLength = response.length;
                for(loopRecord=0; loopRecord < responseLength; loopRecord++) {
                  if(response[loopRecord]["status"] == 'Certified') {
                    studentsCertifiedCount++;
                  }

                  if(response[loopRecord]["status"] == 'Completed') {
                    studentsCompletedCount++;
                  }

                  if(response[loopRecord]["category"] == 'Online') {
                    batchCourseStatsEnrolledCount++;
                    onlineCourseStatsRevenueCount += parseInt(response[loopRecord]["course_fees"])+parseInt(response[loopRecord]["books_fees"])-parseInt(response[loopRecord]["discount"]);
                    courses.push(response[loopRecord]["course_id"]);
                  }         

                  if(response[loopRecord]["category"] == 'Classroom') {
                    batchCourseStatsRevenueCount += parseInt(response[loopRecord]["course_fees"])+parseInt(response[loopRecord]["books_fees"])-parseInt(response[loopRecord]["discount"]);
                    batches.push(response[loopRecord]["batch_id"]);
                  }

                  totalCourseRevenueCount += parseInt(response[loopRecord]["course_fees"])+parseInt(response[loopRecord]["books_fees"])-parseInt(response[loopRecord]["discount"]); 

                   // console.log("totalCourseRevenueCount = "+totalCourseRevenueCount)

                  totalStudentsDue += parseInt(response[loopRecord]["dueamount"]);

                  if( parseInt(response[loopRecord]["dueamount"]) != 0) {
                    totalStudentDuesCount++;
                  }
                  
                  
                }
                //$("#coursesEnrolled").html(arrUnique(courses).length);
                $("#studentsCompleted").html(studentsCompletedCount);
                $("#studentsEnrolled").html(response.length);
                $("#studentsCertified").html(studentsCertifiedCount);
                $("#batchCourseStatsEnrolled").html(batches.length);
                $("#batchCourseStatsRevenue").html(batchCourseStatsRevenueCount);
                $("#onlineCourseStatsEnrolled").html(batchCourseStatsEnrolledCount);
                $("#onlineCourseStatsRevenue").html(onlineCourseStatsRevenueCount);
                $("#totalStudentsEnrolled").html(parseInt(batches.length)+parseInt(batchCourseStatsEnrolledCount));
                $("#totalStudentDues").html(totalStudentDuesCount);
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
  
}


function loadIncomeExpenseDetailsCount(month, year, fromDate, toDate) {
  var sqlQuery='SELECT incomeexpense.* FROM incomeexpensepayment left join incomeexpense on incomeexpense.id = incomeexpensepayment.incomeexpense_id where 1';

  if(fromDate != '' && toDate != '') {
          sqlQuery += ' and incomeexpensepayment.payment_date between "' +  fromDate + '" AND "' + toDate + '"';
        }
  else {
    if(month != '') {
          sqlQuery += ' and month(incomeexpensepayment.payment_date) = "' +  month + '"';
        }
  if(year != '') {
          sqlQuery += ' and year(incomeexpensepayment.payment_date) = "' +  year + '"';
        }
  }
  console.log(sqlQuery);
  var schoolCourseStatsRevenueCount = 0 , totalCourseExpenseCount = 0;
  totalExpenseDue=0, totalIncomeDue = 0;
  data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                var responseLength = response.length;
                for(loopRecord=0; loopRecord < responseLength; loopRecord++) {
       

                  if(response[loopRecord]["type"] == 'Income' && response[loopRecord]["category"] == 'School') {
                    schoolCourseStatsRevenueCount += parseInt(response[loopRecord]["amount_received"]);
                  }

                  if(response[loopRecord]["type"] == 'Expense') {
                    totalExpenseCount += parseInt(response[loopRecord]["amount_paid"]);
                    totalExpenseDue += parseInt(response[loopRecord]["balance"]);

                    if(response[loopRecord]["category"] == 'School' || response[loopRecord]["category"] == 'Student' || response[loopRecord]["category"] == 'Batch') {
                      totalCourseExpenseCount += parseInt(response[loopRecord]["amount_paid"]);
                    }
                  }

                  if(response[loopRecord]["type"] == 'Income') {
                    totalIncomeCount += parseInt(response[loopRecord]["amount_received"]);
                    totalIncomeDue += parseInt(response[loopRecord]["balance"]);

                    if(response[loopRecord]["category"] == 'School' || response[loopRecord]["category"] == 'Student' || response[loopRecord]["category"] == 'Batch') {
                      //totalCourseRevenueCount += parseInt(response[loopRecord]["amount_received"]);
                    }

                  }


                }
                $("#schoolCourseStatsRevenue").html(schoolCourseStatsRevenueCount);
                // console.log("total"+totalCourseRevenueCount);
                loadPieChart([totalCourseExpenseCount,schoolCourseStatsRevenueCount+totalCourseRevenueCount], ['Expense', 'Revenue'],'Course Revenue and Expense');
                loadDoughnutChart([totalStudentsDue, totalIncomeDue, totalExpenseDue], ['Students Due', 'Income Due', 'Expense Due'],'Overall due');
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
  
}

function loadTopCourseEnrolmentCount(month, year, fromDate, toDate) {
  
  verticalChartData = [];
  verticalChartLabels = [];

  var sqlQuery='SELECT count(studentenrolment.course_id) as coursecount, studentenrolment.course_id, courses.shortname FROM studentenrolment left join courses on courses.id = studentenrolment.course_id where 1';

  if(fromDate != '' && toDate != '') {
          sqlQuery += ' and studentenrolment.enrolment_date between "' +  fromDate + '" AND "' + toDate + '"';
        }
  else {
    if(month != '') {
          sqlQuery += ' and month(studentenrolment.enrolment_date) = "' +  month + '"';
        }
  if(year != '') {
          sqlQuery += ' and year(studentenrolment.enrolment_date) = "' +  year + '"';
        }
  }
  console.log(sqlQuery);
  sqlQuery += ' GROUP BY studentenrolment.course_id LIMIT 5';
     
  data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                var responseLength = response.length;
                verticalChartData = [], verticalChartLabels = [];

                for(loopRecord=0; loopRecord < responseLength; loopRecord++) {
                  verticalChartData.push(response[loopRecord]["coursecount"]);
                  verticalChartLabels.push(response[loopRecord]["shortname"]);
                }
                loadBarChart(verticalChartData, verticalChartLabels, 'Top 5 courses enrolment');
              }
              else 
              {
                alert("ERROR: " + response.message);
                loadBarChart(verticalChartData, verticalChartLabels, 'Top 5 courses enrolment');
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });  
}


function loadTopCourseRevenuCount(month, year, fromDate, toDate) {
  
  horizontalChartData = [];
  verticalChartLabels = [];

  var sqlQuery='SELECT sum(course_fees) as course_fees_paid, sum(books_fees) as books_fees_paid, sum(discount) as discount, courses.shortname FROM studentenrolment left join courses on courses.id = studentenrolment.course_id where 1';

  if(fromDate != '' && toDate != '') {
          sqlQuery += ' and studentenrolment.enrolment_date between "' +  fromDate + '" AND "' + toDate + '"';
        }
  else {
    if(month != '') {
          sqlQuery += ' and month(studentenrolment.enrolment_date) = "' +  month + '"';
        }
  if(year != '') {
          sqlQuery += ' and year(studentenrolment.enrolment_date) = "' +  year + '"';
        }
  }
  console.log(sqlQuery);

   sqlQuery += ' GROUP BY studentenrolment.course_id LIMIT 5';

  data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                var responseLength = response.length;
                horizontalChartData = [], horizontalChartLabels = [];

                for(loopRecord=0; loopRecord < responseLength; loopRecord++) {
                  horizontalChartData.push( ( parseInt(response[loopRecord]["course_fees_paid"]) + parseInt(response[loopRecord]["books_fees_paid"]) ) - parseInt(response[loopRecord]["discount"]));
                  horizontalChartLabels.push(response[loopRecord]["shortname"]);
                }
                loadHorizontalBarChart(horizontalChartData, horizontalChartLabels,'Top 5 revenue courses');

              }
              else 
              {
                alert("ERROR: " + response.message);
                loadHorizontalBarChart(horizontalChartData, horizontalChartLabels,'Top 5 revenue courses');
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
  
}

// Horizontal chart configuration
function loadHorizontalBarChart(horizontalChartData, horizontalChartLabels, horizontalChartText) {

    var color = Chart.helpers.color;
    var barChartData = {
      labels: horizontalChartLabels,
      datasets: [{
        label: 'Revenue',
        backgroundColor: '#FFAE0A',
        borderColor: '#FFAE0A',
        borderWidth: 1,
        data: horizontalChartData
      }]
    };

$('#divHorizontalBarChart').empty();
    
$('#divHorizontalBarChart').html('<canvas id="horizontalBarChart" height="50px"></canvas>');

var ctx = document.getElementById('horizontalBarChart').getContext('2d');
window.myBar = new Chart(ctx, {
  type: 'horizontalBar',
  data: barChartData,
  options: {
    responsive: true,
    legend: {
      position: 'top',
    },
    title: {
      display: true,
      text: horizontalChartText
    },
    scales: {
        xAxes: [{
            ticks: {
                beginAtZero: true,
                fixedStepSize: 200
            }
        }]
    }
  }
});
}

// Vertical chart configuration
function loadBarChart(verticalChartData, verticalChartLabels, verticalChartText) {

    var color = Chart.helpers.color;
    var barChartData = {
      labels: verticalChartLabels,
      datasets: [{
        label: 'Enrolment',
        backgroundColor: '#49A5E9',
        borderColor: '#49A5E9',
        borderWidth: 1,
        data: verticalChartData
      }]

    };

$('#divBarChart').empty();
    
$('#divBarChart').html('<canvas id="barChart" height="50px"></canvas>');

var ctx = document.getElementById('barChart').getContext('2d');
window.myBar = new Chart(ctx, {
  type: 'bar',
  data: barChartData,
  options: {
    responsive: true,
    legend: {
      position: 'top',
    },
    title: {
      display: true,
      text: 'Top 5 courses enrolment'
    },
    scales: {
        yAxes: [{
          scaleLabel: { display: false, labelString: ["Enrolment"] },
            ticks: {
                beginAtZero: true,
                fixedStepSize: 1,
            }
        }],
        xAxes: [{
          scaleLabel: { display: false, labelString: ["Enrolment"] }
        }]
    }
  }
});
}

// pie chart configuration
function loadPieChart(pieChartData, pieChartLabels, pieChartText) {
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: pieChartData,
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.green
          ],
          label: pieChartLabels[0] + ' / ' + pieChartLabels[1]
        }],
        labels: pieChartLabels
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: pieChartText
        }
      }
    };   
  $('#divPieChart').empty();
      
  $('#divPieChart').html('<canvas id="pieChart" height="50px"></canvas>');

  var ctx = document.getElementById('pieChart').getContext('2d');
  window.myPie = new Chart(ctx, config);
}

// doughnut chart configuration
function loadDoughnutChart(doughnutChartData, doughnutChartLabels, doughnutChartText) {
    var config = {
      type: 'doughnut',
      data: {
        datasets: [{
          data: doughnutChartData,
          backgroundColor: [
            window.chartColors.blue,
            window.chartColors.purple,
            window.chartColors.yellow,
          ],
          label: doughnutChartLabels
        }],
        labels: doughnutChartLabels
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: doughnutChartText
        }
      }
    };   
  $('#divDoughnutChart').empty();
      
  $('#divDoughnutChart').html('<canvas id="doughnutChart" height="50px"></canvas>');

  var ctx = document.getElementById('doughnutChart').getContext('2d');
  window.myPie = new Chart(ctx, config);
}
});
</script>
</body>
</html>

<?php ob_end_flush(); ?>