<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="dashboard.php"><img class="navbar-logo-img" src="assets/images/understand-quran-logo.png" /></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">      
      <ul class="nav navbar-nav">
        <li><a href="dashboard.php">Dashboard</a></li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="batchdetails.php">Batch Details</a></li>
            <li><a href="batchportions.php">Batch Portions</a></li>
            <li class="divider"></li> 
            <li><a href="course.php">Course Details</a></li>
            <li><a href="school.php">School Details</a></li>
            <li class="divider"></li>        
            <li><a href="enquiry.php">Enquiry Details</a></li>
            <li class="divider"></li>            
            <li><a href="employees.php">Employee Details</a></li>
            <li><a href="employeeattendance.php">Employee Attendance</a></li>
            <li class="divider"></li>
            <li><a href="meetingdetails.php">Meeting Details</a></li>
            <li><a href="meetingaction.php">Meeting Action</a></li>
            <li class="divider"></li>
            <li><a href="location.php">Location Details</a></li>
            <?php
            if ($_SESSION['is_admin']=="Yes") {
            ?>
            <li class="divider"></li>
            <li><a href="users.php">Site Users</a></li>
            <?php
            }
            ?>
            
          </ul>
        </li>        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Student
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="students.php">Student Details</a></li>
            <li><a href="enrolment.php">Student Enrolment</a></li>
            <li><a href="studentattendance.php">Student Attendance</a></li>
          </ul>
        </li>   
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Accounts
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="inventory.php">Inventory Details</a></li>
            <li><a href="incomeexpense.php">Income / Expense Details</a></li>
          </ul>
        </li>          
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Reports
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="enquiryreport.php">Enquiry Details</a></li>
            <li><a href="batchdetailsreport.php">Batch Details</a></li>
            <li><a href="batchesyearwisereport.php">Batches Yearwise</a></li>
            <li><a href="trainersschedulereport.php">Trainers Schedule</a></li>
            <li><a href="studentattendancereport.php">Student Attendance</a></li>
            <li><a href="employeeattendancereport.php">Employee Attendance</a></li>
            <li><a href="meetingactionsreport.php">Meeting Actions</a></li>
            <li><a href="schoolpaymentreport.php">School Payment</a></li>
            <li><a href="incomeexpensereport.php">Income / Expense</a></li>
            <li><a href="cashstatementreport.php">Cash Statement</a></li>
            <li><a href="inventrydetailsreport.php">Inventory Details</a></li>
          </ul>
        </li>        
      </ul>
      <ul class="nav navbar-nav navbar-right">        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <span class="glyphicon glyphicon-user"></span>&nbsp; <?php echo $userRow['fullname']; ?>&nbsp;<span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a href="myprofile.php"><span class="glyphicon glyphicon-user"></span>&nbsp;My Profile</a></li>
            <li><a href="logout.php?logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Sign Out</a></li>
          </ul>
        </li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>