<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Cash Statement Report</h3>
  	</div>        
    <div class="col-sm-12">

      <div id="filterArea" class="form-row">
        <div class="row">
          <div class="col-md-0">
            <label for="fromDate" class="control-label">From date</label>
            <input elementtype="text" class="form-control" id="fromDate" name="fromDate" value="" placeholder="YYYY-MM-DD" type="text">
          </div>
          <div class="col-md-0">
            <label for="toDate" class="control-label">To date</label>
            <input elementtype="text" class="form-control" id="toDate" name="toDate" value="" placeholder="YYYY-MM-DD" type="text">
          </div>          
          <div class="pull-right" style='padding-bottom:10px; padding-top: 5px'>
            <button type="button" class="btn btn-xs btn-primary" id="btn-pdf" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> PDF</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-xls" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> Excel</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-print" data-row-id="0">
            <span class="glyphicon glyphicon-print"></span> Print</button>
          </div>

          <div class="form-group col-md-4">
            <span class="text-danger"></span>
          </div>

          <div class="row">
          <div class="form-group col-md-4">
          </div>
        </div>            
      </div>
      
      <div id="printArea" class="row">

       
      </div>
     

    </div>
  </div>
</div>


<?php include_once("footer.php"); ?>
</div>

<script>
$(document).ready(function() 
{ 


var db_table_name = [] , db_table_columns = [], table_header = [];

db_table_name = ["incomeexpense"]; // db table names 
db_table_where_colummn = []; // db table names
db_table_columns = ["id","description","payment_date","amount_received","amount_paid","amount_received","amount_paid","payment_comments"]; // db table column
table_header = ["Sl.No.","Description","Date","Cash credit","Cash debit","Bank credit","Bank debit","Comments"]; // db table column

$('#fromDate, #toDate').val('');

// date popup

// date popup
var current = new Date();     // get current date    
var weekstart = current.getDate() - current.getDay() +1;    
var weekend = weekstart + 6;       // end day is the first day + 6 
var startOfWeek = new Date(current.setDate(weekstart));  
var endOfWeek = new Date(current.setDate(weekend));
var startOfWeekSplitted = startOfWeek.toISOString().slice(0,10);  
var endOfWeekSplitted = endOfWeek.toISOString().slice(0,10);
console.log(startOfWeekSplitted,endOfWeekSplitted);

var todayDate = new Date();
var previousDate = new Date(todayDate);
previousDate.setDate(todayDate.getDate()-6);
previousDate.toLocaleDateString();

   $('#fromDate').parent().addClass('date')
    $('#fromDate').datetimepicker({
    format: 'YYYY-MM-DD',
    date: startOfWeekSplitted
   }); 

    $('#toDate').parent().addClass('date')
    $('#toDate').datetimepicker({
    format: 'YYYY-MM-DD',
    date: endOfWeekSplitted
   }); 

loadAjaxCall();



$("#fromDate, #toDate").blur(function () {
  $("#printArea").html("");
  loadAjaxCall();
});

function loadAjaxCall() {
  var status = $("#selectStatus option:selected").val();
  var acctype = $("#selectType option:selected").val();

  var fromDate = $("#fromDate").val();
  var toDate = $("#toDate").val();



      var sqlQuery = "SELECT incomeexpense.*, incomeexpensepayment.* from incomeexpensepayment left join incomeexpense on incomeexpense.id = incomeexpensepayment.incomeexpense_id where incomeexpensepayment.id <> 0"

        if(fromDate != '' && toDate != '' && fromDate != undefined && toDate != undefined) {
            sqlQuery += " and incomeexpensepayment.payment_date BETWEEN '" + fromDate + "' AND '" + toDate + "'"
        }

        sqlQuery += " order by incomeexpensepayment.payment_date desc"

      console.log(sqlQuery)
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {                
                  
                  populateTable(response, status, acctype, fromDate, toDate);               
                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}


function loadCashInHand() {
var toDate = $("#toDate").val();
      var sqlQuery = "SELECT sum(IF(amount_received <> 0, amount_received,0))-sum(IF(amount_paid <> 0, amount_paid, 0)) as profitloss, payment_mode FROM incomeexpensepayment where 1"

        if(toDate != '' && toDate != undefined) {
            sqlQuery += " and (payment_date) <= '" + toDate + "'"
        }

        sqlQuery += " GROUP by payment_mode order by payment_mode asc"

      console.log(sqlQuery)
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                var html_cashInHand = '', cashInHandAmount = 0, cashInBankAmount = 0;

                for(loopColumns=0;loopColumns<response.length;loopColumns++) 
                { 
                  if(response[loopColumns]["payment_mode"] == "Cash") {                    
                    cashInHandAmount = response[loopColumns]["profitloss"];
                  }
                  if(response[loopColumns]["payment_mode"] == "Bank") {
                    cashInBankAmount = response[loopColumns]["profitloss"];
                  }                  
                }
                if(cashInHandAmount == null) {
                  cashInHandAmount = 0;
                }
                if(cashInBankAmount == null) {
                  cashInBankAmount = 0;
                }
                html_cashInHand = '<b>Cash in Hand:</b> ' + cashInHandAmount
                html_cashInBank = '<b>Bank:</b> ' + cashInBankAmount
                $("#cashInHand").html(html_cashInHand + ' &#160;&#160;&#160;' + html_cashInBank);                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}

function populateTable(response, status, acctype, fromDate, toDate) 
{ 

var htmlTableData = '', dates = '', headingText = 'Cash Statement';
  

  $(".text-danger").html("");
  if(fromDate != '' && toDate != '' && fromDate != undefined && toDate != undefined) {
        if(moment(toDate).isAfter(fromDate) == false && fromDate != toDate)
          {
            $(".text-danger").html("To date should be greater than or equal to From date");            
          }
          else {
            var fromDateFormatted = formatDate(fromDate);
            var toDateFormatted = formatDate(toDate);

            $(".text-danger").html("");
            console.log(fromDate, toDate)
            if(fromDate == toDate) {
              dates = '<br>' + fromDateFormatted;
            }
            else
            { 
              dates = '<br>' + fromDateFormatted + ' - ' + toDateFormatted;
            }
          }

 
  }
  else {
    dates = '';
  }


        htmlTableData += '<table class="reportHeader"><tbody><tr><td><b>' + headingText + ' ' + dates + '</b></td></tr></tbody></table>' + 
                          '<div id=cashInHand></div><div class="table-responsive"><table id="tableData" class="table table-bordered table-responsive">' + 
                          '<thead>' 
        var tableHeaderLength = table_header.length;
        for(loopColumns=0;loopColumns<tableHeaderLength;loopColumns++) 
        {
                htmlTableData += '<th width=100>' + table_header[loopColumns] + '</th>';
        }
        htmlTableData += '</tr>' +
                          '</thead>';

        htmlTableData += '<tbody>' 
                          
        var responseLength = response.length;
        for(loopColumns=0;loopColumns<responseLength;loopColumns++) 
        {          
              htmlTableData += '<tr>'
                  var db_table_columns_Length = db_table_columns.length;
                  for(loopHeaders=0; loopHeaders<db_table_columns_Length; loopHeaders++)  {
                    if(loopHeaders==0) {
                      htmlTableData += '<td>' + parseInt(loopColumns+1) + '</td>';
                    }
                    else {
                      console.log(response[loopColumns]["payment_mode"])
                         // if(response[loopColumns]["payment_mode"] == "Cash" && loopHeaders==5) {
                         //    loopHeaders=loopHeaders+2;
                         // }
                         if(response[loopColumns]["payment_mode"] == "Bank" && (loopHeaders==3 || loopHeaders==4)) {
                            htmlTableData += '<td>0</td>';
                         }
                         else {                         
                             if(response[loopColumns]["payment_mode"] == "Cash" && (loopHeaders==5 || loopHeaders==6)) {
                                htmlTableData += '<td>0</td>';
                             }
                             else {
                             console.log(loopHeaders)
                             // if(db_table_columns[loopHeaders] == "payment_date") {
                             //  htmlTableData += '<td>' + formatDate(response[loopColumns][db_table_columns[loopHeaders]]) + '</td>';
                             // }
                             // else {
                             //  htmlTableData += '<td>' + response[loopColumns][db_table_columns[loopHeaders]] + '</td>';
                             // }
                                var rowContent = response[loopColumns][db_table_columns[loopHeaders]];
                                if(rowContent != null)
                                {
                                  htmlTableData += '<td>' + response[loopColumns][db_table_columns[loopHeaders]] + '</td>';
                                }
                                else {
                                  htmlTableData += '<td>0</td>';
                                }
                             
                             }
                        }
                      
                    }
                  }

               htmlTableData += '</tr>'
        }
        htmlTableData += '</tbody></table></div><br><br>';
        //console.log(htmlTableData)
        $("#printArea").html(htmlTableData);
        loadCashInHand();
}

});



 $( "#btn-xls" ).click(function() {
      $('#printArea').tableExport({type:'excel'}); 
 }); 

 $( "#btn-print" ).click(function() {
      window.print(); 
 });

 $( "#btn-pdf" ).click(function() {
      pdfExport("printArea");
 });

</script>
</body>
</html>

<?php ob_end_flush(); ?>