<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
  <div class="container">    
    <div class="page-header">
      <h3>Trainers Schedule Report</h3>
    </div>        
    <div class="col-sm-12">

      <div id="filterArea" class="form-row">
        <div class="row">
          <div class="col-md-0">
            <label for="fromDate" class="control-label">From date</label>
            <input elementtype="text" class="form-control" id="fromDate" name="fromDate" value="" placeholder="YYYY-MM-DD" type="text">
          </div>
          <div class="col-md-0">
            <label for="toDate" class="control-label">To date</label>
            <input elementtype="text" class="form-control" id="toDate" name="toDate" value="" placeholder="YYYY-MM-DD" type="text">
          </div>          
          <div class="pull-right" style='padding-bottom:10px; padding-top: 5px;'>
            <button type="button" class="btn btn-xs btn-primary" id="btn-pdf" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> PDF</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-xls" data-row-id="0">
            <span class="glyphicon glyphicon-download"></span> Excel</button>
            <button type="button" class="btn btn-xs btn-primary" id="btn-print" data-row-id="0">
            <span class="glyphicon glyphicon-print"></span> Print</button>
          </div>

          <div class="form-group col-md-4">
            <span class="text-danger"></span>
          </div>

          <div class="row">
          <div class="form-group col-md-4">
          </div>
        </div>            
      </div>
      
      <div id="printArea" class="row">

       
      </div>
     

    </div>
  </div>
</div>


<?php include_once("footer.php"); ?>
</div>

<script>
$(document).ready(function() 
{ 


var db_table_name = [] , db_table_columns = [], table_header = [];

db_table_name = ["batchdetails"]; // db table names 
db_table_where_colummn = []; // db table names
db_table_columns = []; // db table column
table_header = []; // db table column


$('#fromDate, #toDate').val('');

// date popup
var current = new Date();     // get current date    
var weekstart = current.getDate() - current.getDay() +1;    
var weekend = weekstart + 6;       // end day is the first day + 6 
var startOfWeek = new Date(current.setDate(weekstart));  
var endOfWeek = new Date(current.setDate(weekend));
var startOfWeekSplitted = startOfWeek.toISOString().slice(0,10);  
var endOfWeekSplitted = endOfWeek.toISOString().slice(0,10);
console.log(startOfWeekSplitted,endOfWeekSplitted);

var todayDate = new Date();
var previousDate = new Date(todayDate);
previousDate.setDate(todayDate.getDate()-6);
previousDate.toLocaleDateString();

   $('#fromDate').parent().addClass('date')
    $('#fromDate').datetimepicker({
    format: 'YYYY-MM-DD',
    date: startOfWeekSplitted
   }); 

    $('#toDate').parent().addClass('date')
    $('#toDate').datetimepicker({
    format: 'YYYY-MM-DD',
    date: endOfWeekSplitted
   }); 


loadAjaxCall();


$("#fromDate, #toDate").blur(function () {
  $("#printArea").html("");
  loadAjaxCall();
});


var employee_id = [], attendance_date = [], attended = [], filteredDate = [];
function loadAjaxCall() {

  var fromDate = $("#fromDate").val();
  var toDate = $("#toDate").val();



      var sqlQuery = "SELECT trainer_employee_id, employees.name as employeename, batchdetails.name as batchname, start_time, end_time, batch_date FROM batchportions left join batchdetails on batchdetails.batch_id = batchportions.batch_id left join employees on employees.id = batchdetails.trainer_employee_id"

        if(fromDate != '' && toDate != '' && fromDate != undefined && toDate != undefined) {
            sqlQuery += " where batchportions.batch_date BETWEEN '" + fromDate + "' AND '" + toDate + "'"
        }

      console.log(sqlQuery)
      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {                  

                  loadEmployees(response, fromDate, toDate);                                
                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}


function loadEmployees(batchData, fromDate, toDate) {
      var sqlQuery = "SELECT id, name from employees where job_category ='Trainer' and job_status = 'Employed' order by name"

      data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                
                  populateTable(response, batchData, fromDate, toDate);                
                  
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
}

function populateTable(response, batchData, fromDate, toDate) 
{ 

var htmlTableData = '', dates = '', headingText = '';

  $(".text-danger").html("");
  if(fromDate != '' && toDate != '' && fromDate != undefined && toDate != undefined) {

        if(moment(toDate).isAfter(fromDate) == false && fromDate != toDate)
          {
            $(".text-danger").html("<br><br>To date should be greater than or equal to From date");
            $("#printArea").html('');
          }
          else {

            var fromDateObject = new Date(fromDate);
            var fromDateFormatted = fromDateObject.getDate() + ' ' + monthShortNames[fromDateObject.getMonth()] + ' ' + fromDateObject.getFullYear();

            var toDateObject = new Date(toDate);
            var toDateFormatted = toDateObject.getDate() + ' ' + monthShortNames[toDateObject.getMonth()] + ' ' + toDateObject.getFullYear();

            $(".text-danger").html("");
            console.log(fromDate, toDate)
            if(fromDate == toDate) {
              dates = '<br>' + fromDateFormatted;
            }
            else
            { 
              dates = '<br>' + fromDateFormatted + ' - ' + toDateFormatted;
            }

              var day = 1000*60*60*24;
              date1 = new Date(fromDate);
              date2 = new Date(toDate);

                var diff = (date2.getTime()- date1.getTime())/day;
                filteredDate = [];
                table_header = [];
                for(var i=0;i<=diff; i++)
                {
                   var xx = date1.getTime()+day*i;
                   var yy = new Date(xx);
                   var strYear = '';
                   strYear = yy.getFullYear();
                   filteredDate.push(strYear + '-' + (yy.getMonth()+1) + '-' + yy.getDate());
                   table_header.push(yy.getDate() + " " + monthShortNames[(yy.getMonth())]  + " " + strYear + ' <br><span style="color:#777">' + weekDayFullNames[yy.getDay()] + '</span>');
                }
        }    
  }
  else {
    dates = '';
  }


        htmlTableData = '<table class="reportHeader"><tbody><tr><td><b>Trainers Schedule ' + dates + '</b></td></tr></tbody></table>' + 
                          '<div class="table-responsive"><table id="tableData" class="table table-bordered table-responsive">' + 
                          '<thead>' +
                          '<tr>' +
                          '<th>Sl.No.</th>' + 
                          '<th>Trainer</th>';
        var tableHeaderLength = table_header.length;
        for(loopColumns=0;loopColumns<tableHeaderLength;loopColumns++) 
        {
          
              htmlTableData += '<th>' + table_header[loopColumns] + '</th>'           
           
        }
        htmlTableData += '</tr>' +
                          '</thead>';

        htmlTableData += '<tbody>';
            
        var responseLength = response.length, batchDetails = '';
        console.log(batchData)
        for(loopColumns=0;loopColumns<responseLength;loopColumns++) 
        {          
              htmlTableData += '<tr>' +
                                '<td>' + (loopColumns+1) + '</td>' +
                                '<td>'+response[loopColumns]["name"]+'</td>'

                        var filteredDateLength = filteredDate.length;
                        for(loopFilteredDate=0;loopFilteredDate<filteredDateLength;loopFilteredDate++) 
                        {
                              attendanceStatus = '';

                               for(loopBatch=0;loopBatch<batchData.length;loopBatch++) 
                                {
                                   
                                      if(batchData[loopBatch]["batch_date"].replace(/-0/g, "-") == filteredDate[loopFilteredDate] && response[loopColumns]["id"]==batchData[loopBatch]["trainer_employee_id"]) {
                                         batchDetails += batchData[loopBatch]["batchname"] + ' <br><span style="color:#777"> ' + batchData[loopBatch]["start_time"] + ' - ' + batchData[loopBatch]["end_time"] + '</span> <br>';
                                      }
                                                                      
                                }
                                htmlTableData += '<td>' + batchDetails + '</td>'
                                batchDetails = '';
                           
                        }


               htmlTableData += '</tr>'
        }

        htmlTableData += '</tbody></table></div><br><br>';
        // console.log(htmlTableData)
        $("#printArea").html(htmlTableData);
        htmlTableData = '';
}

});



 $( "#btn-xls" ).click(function() {
      $('#printArea').tableExport({type:'excel'}); 
 }); 

 $( "#btn-print" ).click(function() {
      window.print(); 
 });

 $( "#btn-pdf" ).click(function() {
      pdfExport("printArea");
 });

</script>
</body>
</html>

<?php ob_end_flush(); ?>