<!DOCTYPE html>
<html>
<head>
<?php include_once("header.php"); ?>
</head>
<body>
<?php include_once("navigation.php"); ?>

<div id="wrapper">
	<div class="container">    
  	<div class="page-header">
      <h3>Batch Details</h3>
  	</div>        
    <div class="col-sm-12">
        <div style="padding-bottom:25px;">
          <div class="pull-right"><button type="button" class="btn btn-xs btn-primary" id="command-add" data-row-id="0">
            <span class="glyphicon glyphicon-plus"></span> Add Details</button>
          </div>
        </div>
        <table id="data_grid" class="table table-condensed table-hover table-striped" width="60%" cellspacing="0" data-toggle="bootgrid">
          <thead>
            <tr>            
              <?php 
              // Data grid header rendering function call 
              renderGridHeaderColumn("id","ID",false,"","","numeric","true","","");

              
              renderGridHeaderColumn("name","Batch",true,"","","","","","");
              renderGridHeaderColumn("coursename","Course",false,"","","","","","");
              renderGridHeaderColumn("course_id","Course id",false,"","","","","false","");
              renderGridHeaderColumn("fees","Fees",false,"","","","","false","");
              renderGridHeaderColumn("locationname","Location",false,"","","","","false","");
              renderGridHeaderColumn("location_id","Location id",false,"","","","","false","");
              renderGridHeaderColumn("days","Days",false,"","","","","","");
              renderGridHeaderColumn("start_date","Start date",false,"","","","","","");
              renderGridHeaderColumn("end_date","End date",false,"","","","","","");
              renderGridHeaderColumn("start_time","Start time",false,"","","","","","");
              renderGridHeaderColumn("end_time","End time",false,"","","","","","");
              renderGridHeaderColumn("employeename","Trainer",false,"","","","","","");
              renderGridHeaderColumn("trainer_employee_id","Trainer id",false,"","","","","false","");
              renderGridHeaderColumn("status","Status",true,"","","","","","");
              renderGridHeaderColumn("comments","Comments",false,"","","","","false","");


              renderGridHeaderColumn("modified","Modified",false,"","","","","false","");
              renderGridHeaderColumn("created","Created",false,"","","","","false","");
              if($_SESSION['is_admin'] == "Yes") {
                renderGridHeaderColumn("commands","Commands",true,"commands","false","","","","");
              }
              ?>
            </tr>
          </thead>
        </table>
    </div>
  </div>
</div>

<!---- Add/Edit Form -->

<div id="edit_modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
          <form method="post" id="frm_edit">
            <input type="hidden" value="edit" name="action" id="action">
            <input type="hidden" value="0" name="id" id="id">
            <div class="form-row">
            <?php 
            // Form elements rendering function call  
            echo "<div class='form-sub-header'>Basic Information</div>";
            echo '<div class="row">';
              renderFormInput("name","Batch name",'data-validation="alphanumeric" data-validation-allowing="#(),-. "',"","col-md-5");

              echo '<div style="display:none;" id="batch_id_hidden"></div>';
              
              $optionTexts=[]; $optionValues=[]; $optionSubTexts=[];
              $sqlResult = fetchRecordForDropdown("select id, fullname, shortname, coursefees from courses order by fullname");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['id'] . '|' . $json[$loop]['coursefees']);
                array_push($optionTexts, $json[$loop]['fullname'] . ' (' . $json[$loop]['shortname'] . ')');
                array_push($optionSubTexts, '<br>&#160;&#160;fees: ' .  $json[$loop]['coursefees']);
              } 

              renderFormSelect("course_id","Course",'data-validation="required"',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-5");

              renderFormInput("fees","Batch fees",'data-validation="number"',"","col-md-2");
              echo '</div>';

            echo '<div class="row">';
              renderFormRadio("status","Status",'data-validation="required"',['Not started', 'Inprogress', 'Completed'],"","col-md-6");
            echo '</div>';

            echo "<br><div class='form-sub-header'>Schedule Information</div>";
            echo '<div class="row">';
            $optionTexts=[]; $optionValues=[]; $optionSubTexts=[];
              $sqlResult = fetchRecordForDropdown("select id, name, city from location order by name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {
                array_push($optionValues, $json[$loop]['id']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160;' .  $json[$loop]['city']);
              } 
              renderFormSelect("location_id","Location",'',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-4");


            $optionTexts=[]; $optionValues=[]; $optionSubTexts=[];
              $sqlResult = fetchRecordForDropdown("select id, name, job_category, job_type from employees where job_status = 'Employed' and job_category = 'Trainer' order by name");              
              $json = json_decode($sqlResult, true);
              $sqlRecordCount = count($json);

              for($loop=0; $loop < $sqlRecordCount; $loop++) {  
                array_push($optionValues, $json[$loop]['id']);
                array_push($optionTexts, $json[$loop]['name']);
                array_push($optionSubTexts, '<br>&#160; ID: ' .  $json[$loop]['id'] . ' &#160; Job category: ' . $json[$loop]['job_category'] . ' &#160; Job type: ' . $json[$loop]['job_type']);
              } 
              renderFormSelect("trainer_employee_id","Trainer",'',$optionValues,$optionTexts,[''],$optionSubTexts,"col-md-4");

            echo '</div>';

            echo '<div class="row">';

              renderFormInput("start_date","Start date",'data-validation="required" placeholder="YYYY-MM-DD"',"","col-md-2");
              renderFormInput("end_date","End date",'data-validation="required" placeholder="YYYY-MM-DD"',"","col-md-3");
              renderFormInput("start_time","Start time",'data-validation="required"',"","col-md-2");
              renderFormInput("end_time","End time",'data-validation="required"',"","col-md-2");
            echo '</div>';

            echo '<div class="row">';
             renderFormSelect("days","Batch days",'data-validation="required" multiple',$dayShortNames,$dayFullNames,[''],[],"col-md-8");
             echo '</div>';

             echo '<div class="row">';
             echo "<div id='datesList' class='datesList'></div>";
             echo '</div>';

            echo "<br><div class='form-sub-header'>Additional Information</div>";
            echo '<div class="row">';
              renderFormTextarea("comments","Comments",'',"","");
            echo '</div>';


            // Modal footer render function call              
            echo renderModalFooter("btn_save","Save","true");            
            ?>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>

<!-- Delete form -->

<?php 
// Delete Modal render function call  
  renderDeleteModal(); 
?>

<?php include_once("footer.php"); ?>
</div>

<script>
  $(document).ready(function() 
  { 




    var formId= "frm_edit"; // form add / update id

    // Database table for insert values - all in arrays for multi tables and its columns, if any
    db_table_names_insert = [["batchdetails"]]; // db table names for insert
    db_table_insert_columns = [["batch_id","name","course_id","fees","location_id","days","start_date","end_date","start_time","end_time","trainer_employee_id","status","comments"]]; // db table column names for insert

    db_table_names_update = [["batchdetails"]]; // db table names for update
    db_table_update_columns = [["name","course_id","fees","location_id","days","start_date","end_date","start_time","end_time","trainer_employee_id","status","comments"]]; // db table column names for update

    sqlUpdateQueryArray = new Array();  // query array for update
    sqlInsertQueryArray = new Array();  // query array for insert   
    sqlDeleteQueryArray = new Array();  // query array for insert  
    sqlUpdateQueryArray = [], sqlInsertQueryArray = [], sqlDeleteQueryArray = [];


    // load data grid
    loadBootgrid('batchdetails_view');   

    var data_grid = $("#data_grid").bootgrid().on("loaded.rs.jquery.bootgrid", function()
    {  

    $('th[data-column-id="comments"]').attr("data-visible",false); //$("#data_grid-header input.dropdown-item-checkbox"));

          /* Executes after data is loaded and rendered */
          data_grid.find(".command-edit").on("click", function(e)
          {            
            // show edit modal

            formReset('frm_edit');            
            $('#edit_modal').modal('show');            
            $('.modal-title').html('Edit - '+$('.page-header h3').text());
            $('#action').val('edit');

            if($(this).data("row-id") >0) 
            {              
              // collect the data              
              $('#' + formId+' #id').val($(this).data("row-id")); // in case we're changing the key
              // ajax call
              sqlQuery = 'select * from ' + db_table_names_update + ' where id=' + $(this).data("row-id");
              data = {
                      action:"fetch",
                      sqlQuery: sqlQuery
                     };
              $.ajax({
                      type: "POST",  
                      url: "response.php",  
                      data: data,
                      dataType: "json",       
                      success: function(response)  
                      { 
                        if(response.status!=-1) 
                        {
                          populateEditFormData(response);
                        }
                        else 
                        {
                          alert("ERROR: " + response.message);
                        }
                      },
                      error: function(req, status, error)  
                      {
                        alert("Error: \n"+status+"\n"+error);
                      } 
                      });
              } else 
              {  //alert('No row selected! First select row, then click edit button');
              }
          }); 
    });

var recordExist = false;
function verifyDuplicateBatchName() {
  
  sqlQuery = 'select id from batchdetails where name = "' + $('#name').val() + '"';

  console.log(sqlQuery);
  data = {
  action:"fetch",
  sqlQuery: sqlQuery
  };
  $.ajax({
          type: "POST",  
          url: "response.php",  
          data: data,
          dataType: "json",       
          success: function(response)  
          { 
            console.log(response)
            if(response.status!=-1) 
            {
              var responseLength = response.length;
                if(responseLength > 0) {
                    recordExist = true;
                    $('#div_name .text-danger').html("Batch name already exists");
                }
                else {
                  recordExist = false;
                  $('#div_name .text-danger').html("");
                }
            }
            else 
            {
              alertm(response.message);
            }
          },
          error: function(req, status, error)  
          {
            alertm("Error: \n"+status+"\n"+error);
          } 
          });
}

function populateEditFormData(dataArray) {
  // populate the data to the form
  formReset('frm_edit');
  formInteractions(eventElementId);
  var dataArrayLength = dataArray[0].length, formElementValue = '';

  if(dataArray.length > 0) 
  {
    $("#batch_id_hidden").html(dataArray[0]["batch_id"]);
      loopColumnsArrayLength = db_table_update_columns[0].length;

        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = dataArray[0][formElementId];
                    
            if($("#"+formElementId).attr('elementType') === "text") {
              $('#' + formElementId).val(formElementValue);
            }

            if($("#" + formElementId).attr('elementType') === "radio") {
              formInteractions(eventElementId);
              if(formElementValue!="") {
                $("input[name="+formElementId+"][value='" + formElementValue + "']").prop('checked', true); 
              }              
            }

            if($("#"+formElementId).attr('elementType') === "select") {
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
              if(formElementValue != null || formElementValue != undefined)
                {
                    formElementValueSplit = formElementValue.split(",");
                    
                    if(formElementValueSplit.length>0) {
                      for (loop = 0; loop<formElementValueSplit.length; loop++)
                      {
                        $("#" + formElementId + " option[value='" + formElementValueSplit[loop] + "']").prop("selected",true);
                      }
                }
              }
              else {
                $("#"+formElementId+"[value=" + formElementValue + "]").prop("selected",true);
              }
              $('#' + formElementId + '.selectpicker').selectpicker('refresh');
            }

            if($("#" + formElementId).attr("elementType") === "textarea") {
              $('#' + formElementId).val(formElementValue);
            }
        }
  }

}

   /************** form interactions *************/
    $('#course_id').selectpicker("refresh");
    console.log($('#course_id option:selected').val())

     // date popup
    $('#start_date, #end_date').parent().addClass('date')
      $('#start_date, #end_date').datetimepicker({
      format: 'YYYY-MM-DD'
    });

    $('#start_time, #end_time').addClass("timepicker");
    $('#start_time.timepicker, #end_time.timepicker').timepicker();


   var eventElementId = "days";
   $('#'+eventElementId).blur(function () {
        formInteractions(eventElementId);
    });
   $('#frm_edit').click(function () {
        formInteractions(eventElementId);
    });

   $(window).on('shown.bs.modal', function() { 
        $('#div_name .text-danger').html("");
        existingBatchDates = [];
        initialLoad = 0;
    });

   

   $('#start_date, #end_date').blur(function () {
    var startDateValue = $('#start_date').val();
    var endDateValue = $('#end_date').val();
    console.log(startDateValue + ''+endDateValue)
    formInteractions();
    if(startDateValue != "" && endDateValue != "" && endDateValue != startDateValue) {      

      if(moment(endDateValue).isAfter(startDateValue) == false)
      {
        $("#div_end_date .text-danger").html("End date should be greater than or equal to start date");
        $('#datesList').html('');
      }
      else {
        $("#div_end_date .text-danger").html("");
      }    
    }      
    });

   $('#name').blur(function () {
    if($('#name').val() != '') {
      verifyDuplicateBatchName();
     }
    });
   

   $('#course_id').change(function() {
      $('#fees').val($('#course_id option:selected').attr("data-value"));  
   });

   var batchDates = [], existingBatchDates = [], initialLoad = 0;
   function formInteractions(elementId)
   {


    var batchDaysSelected = $("#div_days button").attr("title");

    // loop for every day
    var startDateValue = $('#start_date').val();
    var endDateValue = $('#end_date').val();
    var daysValue = $('#days').val();
    var start = new Date(startDateValue); //yyyy-mm-dd
    var end = new Date(endDateValue); //yyyy-mm-dd
    var batchDatesListHtml = '', batchDaysCount = 0;
    var day = '', month = '', year = '', batchDate = '';

    if(startDateValue != "" && endDateValue != "" && daysValue != "" && daysValue != null) 
    {

      

      $('#datesList').html("");
      $('#datesList').show();
       batchDates = [];

        while(start <= end)
        {
          var mm = ((start.getMonth()+1)>=10)?(start.getMonth()+1):'0'+(start.getMonth()+1);
          var dd = ((start.getDate())>=10)? (start.getDate()) : '0' + (start.getDate());
          var yyyy = start.getFullYear();
          var batchDate = yyyy+"/"+mm+"/"+dd; 
          var batchDateObject = new Date(batchDate);
          
          if( batchDaysSelected.indexOf(weekDayFullNames[start.getDay()]) != -1 ) {
            ++batchDaysCount;
            var day = batchDateObject.getDate();
            var monthIndex = batchDateObject.getMonth();
            var year = batchDateObject.getFullYear();
              batchDatesListHtml += '<div class="datesListInner">';
            
            batchDatesListHtml += day + ' ' + monthShortNames[monthIndex] + ' ' + year + ' <span class="batchDatesDays">' + weekDayShortNames[start.getDay()] + '</span>';
              batchDatesListHtml += '</div>';

            batchDates.push(batchDate);
          }

          start = new Date(start.setDate(start.getDate() + 1)); //date increase by 1
        }

        if(initialLoad == 0) {
          existingBatchDates = batchDates;
          console.log(batchDates)
          initialLoad = 1;
        }
        console.log("existing: " + existingBatchDates)
        console.log("new: " + batchDates)

        if(batchDatesListHtml != '') {
          $('#datesList').html('<label class="control-label">List of batch dates (' + batchDaysCount + ')  </label><br>' + batchDatesListHtml);
           $('#div_days .text-danger').html('');
            if($("#trainer_employee_id option:selected").val() != "") {
              checkTrainerAvailability(batchDates, $("#trainer_employee_id option:selected").val(), $("#batch_id_hidden").html());
            }           
        }
        else {
          $('#div_days .text-danger').html('No batch dates available for the batch days selected');
        }
      }   
   else {
      $('#datesList').html("");
      $('#datesList').hide();
   }
 }

 function checkTrainerAvailability(batchDates, trainer_employee_id, batch_id) {
   var batchDatesList = '';
   for(loopbatchDates = 0; loopbatchDates < batchDates.length; loopbatchDates++) {
     batchDatesList += "'" + batchDates[loopbatchDates] + "'";

     if (loopbatchDates<batchDates.length-1 && batchDates.length!=1) {
      batchDatesList += ",";
     }
   }

  var sqlQuery = "SELECT batchportions.batch_id, batchportions.batch_date, batchdetails.batch_id, batchdetails.trainer_employee_id FROM batchportions left join batchdetails on batchdetails.batch_id = batchportions.batch_id where batchdetails.trainer_employee_id = '" + trainer_employee_id + "' AND batchdetails.batch_id <> '" + batch_id + "' AND batchportions.batch_date IN (" + batchDatesList + ")";
  console.log(sqlQuery);
  data = {
            action:"fetch",
            sqlQuery: sqlQuery
           };
    $.ajax({
            type: "POST",  
            url: "response.php",  
            data: data,
            dataType: "json",       
            success: function(response)  
            { 
              if(response.status!=-1) 
              {
                console.log(response.length )
                if(response.length > 0) {
                  $('#div_days .text-danger').html('<br>Trainer already scheduled for the date(s) below:<br>');
                   for(loopResponse=0;loopResponse<response.length;loopResponse++) 
                    {
                      $('#div_days .text-danger').append(response[loopResponse]["batch_date"]);
                      if (loopResponse<response.length-1 && response.length!=1) {
                        $('#div_days .text-danger').append(', ');
                       }
                    }                  
                }
                else {
                  $('#div_days .text-danger').html('');
                }
              }
              else 
              {
                alert("ERROR: " + response.message);
              }
            },
            error: function(req, status, error)  
            {
              alert("Error: \n"+status+"\n"+error);
            } 
            });
  
 }

  $( "#command-add" ).click(function() {
    $('#datesList').html("");
    $('#datesList').hide();
    $("#batch_id_hidden").html("");
  });



   /************* Form validation   ***************/
  $.validate({
    form: "#frm_edit",
    validateOnBlur : _validateOnBlur, // disable validation when input looses focus
    errorMessagePosition : _errorMessagePosition, // Instead of 'inline' which is default, you can set to 'top'
    scrollToTopOnError : _scrollToTopOnError, // Set this property to true on longer forms 
    onError : function($form) {
        alertm(_validateErrorMessage);
    },  
    onSuccess : function($form) {
      sqlUpdateQueryArray.length=0;
      sqlInsertQueryArray.length=0;
      sqlDeleteQueryArray.length=0;
      db_table_update_values.length=0;
      db_table_insert_values.length=0;

      console.log($("#batch_id").val())
      if($("#batch_id").val() != 'undefined') {

      }
      $("<input id='batch_id' style='color:#fff;display:none' value='' />").insertAfter("#div_status");
      var formattedBatchId = ($("#name").val()).replace(/\s/g, "_")
      $("#batch_id").val(formattedBatchId);

      if(db_table_update_columns.length!=0) {
        var tempArray = [], 
            formValue = '', 
            formElementId = '', 
            formElementValue = '', 
            loopColumnsArrayLength =0;


        /************* get edit form values and push to array ***************/

        loopColumnsArrayLength = db_table_update_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_update_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
            tempArray.push(formElementValue); 
        }
        db_table_update_values.push(tempArray);



         /************* get add form values and push to array ***************/

        loopColumnsArrayLength = db_table_insert_columns[0].length;
        tempArray = [];
        for(loopColumns=0;loopColumns<loopColumnsArrayLength;loopColumns++) 
        {
          formElementId = db_table_insert_columns[0][loopColumns];
          formElementValue = $('#' + formId + ' #' + formElementId).val();
            if($("#" + formElementId).attr('type') === "radio") {
              
              formElementValue = $("input[id="+formElementId+"]:checked").val();
            }

            if(formElementValue === undefined || formElementValue === null) {
                 formElementValue = '';            
            }
          tempArray.push(formElementValue);                    
        }
        db_table_insert_values.push(tempArray);
      }

        var dateValue_1 = $('#start_date').val();
        var dateValue_2 = $('#end_date').val();
        var dateComparison = compare_dates(dateValue_1, dateValue_2);
        if(dateComparison === "d1 > d2") {
          alert("End date should be greater than start date");
        }
        else {         
        //console.log("sqlInsertQuery "+sqlInsertQueryArray); 

        create_db_table_sql_update_query($('#' + formId + ' #id').val()); // For update query - db table row unique id as parameter
        create_db_table_sql_insert_query(); // For insert query 

        if($('#action').val() == 'add') 
        {
          if(batchDates != '' || batchDates != 'undefined')
          {             
            var batchDatesLength = batchDates.length;
            var sqlQueryText = "";
            for (loopBatchDates=0; loopBatchDates < batchDatesLength; loopBatchDates++) 
            {
                    
              sqlQueryText = 'INSERT INTO batchportions (batch_id,batch_date,portions_targeted,portions_completed,comments) VALUES("' + $("#batch_id").val() + '", "' + batchDates[loopBatchDates] + '","","","")';
              sqlInsertQueryArray.push(sqlQueryText);
                
                //console.log(sqlQueryText); 

            }
            
             console.log("sqlInsertQuery "+sqlInsertQueryArray); 
          }
         }

         if($('#action').val() == 'edit') 
        {
          sqlInsertQueryArray.length = 0;
          if(batchDates != '' || batchDates != 'undefined')
          {             
            var batchDatesLength = batchDates.length;
            var sqlQueryText = "";


            for (loopBatchDates=0; loopBatchDates < batchDatesLength; loopBatchDates++) 
            {
              console.log(batchDates[loopBatchDates] + ' ' + existingBatchDates);
              if(jQuery.inArray(batchDates[loopBatchDates], existingBatchDates) == -1)  {
                sqlQueryText = 'INSERT INTO batchportions (batch_id,batch_date,portions_targeted,portions_completed,comments) VALUES("' + $("#batch_id").val() + '", "' + batchDates[loopBatchDates] + '","","","")';
                sqlInsertQueryArray.push(sqlQueryText);
              }
              else {
                sqlQueryText = '';
              }
            }

            var existingBatchDatesLength = existingBatchDates.length;
            for (loopBatchDates=0; loopBatchDates < existingBatchDatesLength; loopBatchDates++) 
            {
              if(jQuery.inArray(existingBatchDates[loopBatchDates], batchDates) == -1)  { 
                sqlQueryText = 'DELETE from batchportions where batch_id="' + $("#batch_id_hidden").html() + '" and batch_date="' + existingBatchDates[loopBatchDates] + '"';
                sqlUpdateQueryArray.push(sqlQueryText);

                sqlQueryText = 'DELETE from studentattendance where batch_id="' + $("#batch_id_hidden").html() + '" and attendance_date="' + existingBatchDates[loopBatchDates] + '"';
                sqlUpdateQueryArray.push(sqlQueryText);
              }
               else {
                sqlQueryText = '';
              }
            }

            if ($("#action").val() =='edit') {
                sqlQueryText = 'UPDATE studentenrolment SET status ="' + $("#status:checked").val() + '" where batch_id="' + $("#batch_id_hidden").html() + '" AND status <> "Certified"';
                sqlUpdateQueryArray.push(sqlQueryText);
            }
            
             console.log("sqlInsertQuery "+sqlInsertQueryArray);
             console.log("sqlUpdateQuery "+sqlUpdateQueryArray);
          }
         }

         
         if(recordExist == true && $("#action").val() =='add') {
          alertm("Batch name already exists")
         }
         else {
            if(batchDates.length > 0) {
                if ($("#action").val() =='edit') {
                  ajaxAction('both');
                }
                if ($("#action").val() =='add') {
                  ajaxAction('add');
                }
                initialLoad = 0;                 
            }
         }     

          // parameter "both" executes both insert and update sql
        } 

      return false; // Will stop the submission of the form
    }
  });

});



</script>
</body>
</html>

<?php ob_end_flush(); ?>